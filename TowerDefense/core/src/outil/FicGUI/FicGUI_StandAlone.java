package outil.FicGUI;

public class FicGUI_StandAlone {

	public static void main (String[] args){
		Modele modele = new Modele();
		MenuInterface inter = new MenuInterface();
		@SuppressWarnings("unused")
		MenuControleur ctrl = new MenuControleur(inter, modele);
		inter.setVisible(true);
	}

}
