package outil.FicGUI;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class MenuInterface extends JFrame {
	
	private JPanel pannelPrincipal;
	
	public JButton btnMonstre;
	public JButton btnPatternTour;
	public JButton btnPatternTir;
	public JButton btnTour;
	public JButton btnPiege;
	public JButton btnEffetPiege;
	
	public MenuInterface()
	{
		
		this.setSize(200,200);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("FicGUI");
		
		pannelPrincipal = new JPanel(new GridLayout(6, 1));
		
		btnEffetPiege = new JButton("EffetPiege");
		btnMonstre = new JButton("Monstre");
		btnPatternTir = new JButton("PatternTir");
		btnPatternTour = new JButton("PatternTour");
		btnPiege = new JButton("Pi�ge");
		btnTour = new JButton("Tour");
		
		pannelPrincipal.add(btnMonstre);
		pannelPrincipal.add(btnPatternTour);
		pannelPrincipal.add(btnPatternTir);
		pannelPrincipal.add(btnTour);
		pannelPrincipal.add(btnPiege);
		pannelPrincipal.add(btnEffetPiege);
		this.add(pannelPrincipal);
	}
}
