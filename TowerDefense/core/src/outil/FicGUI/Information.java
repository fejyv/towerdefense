package outil.FicGUI;

import java.util.ArrayList;

import outil.typeFichier.FichierInfo;

public class Information {
	private FichierInfo fichier;
	private ArrayList<ArrayList<String>> donnees;
	private ArrayList<String> labels; 
	
	private InformationInterface inter;
	@SuppressWarnings("unused")
	private InformationControleur ctrl;
	
	
	public Information(FichierInfo fichier, ArrayList<String> labels)
	{
		if(fichier.getNbColonne() != labels.size()) throw new RuntimeException("Nombre de labels incohérent avec le nombre de colonnes");
		donnees = new ArrayList<ArrayList<String>>();
		this.fichier = fichier;
		this.labels = labels;
		extraireDonnees();
		inter = new InformationInterface(labels);
		ctrl = new InformationControleur(inter, this);
	}
	
	public String getNom()
	{
		return fichier.getNom();
	}

	@SuppressWarnings("unchecked")
	private void extraireDonnees()
	{
		ArrayList<String> tmp = new ArrayList<String>();
		for(int i = 0; i < fichier.getNbLigne(); i++)
		{
			for(int j = 0; j < fichier.getNbColonne(); j++)
			{
				tmp.add(fichier.getInfo(i, j));
			}
			donnees.add((ArrayList<String>) tmp.clone());
			tmp.clear();
		}
	}
	
	public void print()
	{
		for(int i = 0; i < donnees.size(); i++)
		{
			for(int j = 0; j < donnees.get(i).size(); j++)
			{
				System.out.println(labels.get(j) + " : " + donnees.get(i).get(j));
			}
		}
	}
	
	public ArrayList<String> getObj(int index)
	{
		return donnees.get(index);
	}
	
	public void display()
	{
		inter.setVisible(true);
	}
	
	public void hide()
	{
		inter.setVisible(false);
	}
	
	public int getNbObj()
	{
		return donnees.size();
	}
	
	public void save()
	{
		fichier.save(donnees);
	}
}
