package outil.FicGUI;

import java.util.ArrayList;
import java.util.Collections;

import outil.typeFichier.FichierInfo;

public class Modele {
	private final String pathInfoMonstre = "../core/assets/properties/infoMonstre.propData";
	private final String pathInfoPatternTour = "../core/assets/properties/infoPatternTour.propData";
	private final String pathInfoPatternTir = "../core/assets/properties/infoPatternTir.propData";
	private final String pathInfoTour = "../core/assets/properties/infoTour.propData";
	private final String pathInfoEffetPiege = "../core/assets/properties/infoEffetPiege.propData";
	private final String pathInfoPiege = "../core/assets/properties/infoPiege.propData";
	
	private final ArrayList<String> paramMonstre = new ArrayList<String>();
	private final ArrayList<String> paramPatternTour = new ArrayList<String>();
	private final ArrayList<String> paramPatternTir = new ArrayList<String>();
	private final ArrayList<String> paramInfoTour = new ArrayList<String>();
	private final ArrayList<String> paramEffetPiege = new ArrayList<String>();
	private final ArrayList<String> paramPiege = new ArrayList<String>();

	private FichierInfo infoMonstre;
	private FichierInfo infoPatternTour;
	private FichierInfo infoPatternTir;
	private FichierInfo infoTour;
	private FichierInfo infoEffetPiege;
	private FichierInfo infoPiege;
	
	public Information monstre;
	public Information patternTour;
	public Information patternTir;
	public Information tour;
	public Information effetPiege;
	public Information piege;

	
	public Modele()
	{
		infoMonstre = new FichierInfo(pathInfoMonstre, "#Chargement info Monstre", true);
		infoPatternTour = new FichierInfo(pathInfoPatternTour, "#Chargement info PatternTour", false);
		infoPatternTir = new FichierInfo(pathInfoPatternTir, "#Chargement info PatternTir", false);
		infoTour = new FichierInfo(pathInfoTour, "#Chargement info Tour", false);
		infoEffetPiege = new FichierInfo(pathInfoEffetPiege, "#Chargement info EffetPiege", false);
		infoPiege = new FichierInfo(pathInfoPiege, "#Chargement info Piege", false);
		
		Collections.addAll(paramMonstre, "Num�ro", "Nom", "Vie", "D�gats", "Argent", "Vitesse", "Type", "Score", "Prix");
		Collections.addAll(paramPatternTour, "Num�ro", "Forme", "Port�e", "Prix");
		Collections.addAll(paramPatternTir, "Num�ro", "Type", "D�gats", "Cadence", "Prix");
		Collections.addAll(paramInfoTour, "Num�ro", "PatternTour", "PatternTir", "Prix");
		Collections.addAll(paramEffetPiege, "Num�ro", "D�gats", "Slow");
		Collections.addAll(paramPiege, "Num�ro", "Num�ro effet", "Cadence", "Prix", "Nombre d'image");
		
		monstre = new Information(infoMonstre, paramMonstre);
		patternTour = new Information(infoPatternTour, paramPatternTour);
		patternTir = new Information(infoPatternTir, paramPatternTir);
		tour = new Information(infoTour, paramInfoTour);
		effetPiege = new Information(infoEffetPiege, paramEffetPiege);
		piege = new Information(infoPiege, paramPiege);
		
	}
}
