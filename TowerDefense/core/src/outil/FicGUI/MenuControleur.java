package outil.FicGUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MenuControleur implements ActionListener{
	
	private MenuInterface inter;
	private Modele model;


	public MenuControleur(final MenuInterface inter, final Modele model)
	{
		this.inter = inter;
		this.model = model;
		
		inter.btnEffetPiege.addActionListener(this);
		inter.btnMonstre.addActionListener(this);
		inter.btnPatternTir.addActionListener(this);
		inter.btnPatternTour.addActionListener(this);
		inter.btnPiege.addActionListener(this);
		inter.btnTour.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent a) {
		Object sender = a.getSource();
		
		if(sender == inter.btnEffetPiege)
		{
			model.effetPiege.display();
		}
		else if(sender == inter.btnMonstre)
		{
			model.monstre.display();
		}
		else if(sender == inter.btnPatternTir)
		{
			model.patternTir.display();
		}
		else if(sender == inter.btnPatternTour)
		{
			model.patternTour.display();
		}
		else if(sender == inter.btnPiege)
		{
			model.piege.display();
		}
		else
		{
			model.tour.display();
		}
	}

}
