package outil.FicGUI;




import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;




public class InformationControleur implements ActionListener {
	
	private InformationInterface vue;
	private Information modele;
	
	private int index = 0;
	
	public InformationControleur(final InformationInterface vue, final Information modele)
	{
		this.vue = vue;
		this.modele = modele;
		
		this.vue.enregistrer.addActionListener(this);
		this.vue.retour.addActionListener(this);
		this.vue.suivant.addActionListener(this);
		this.vue.precedent.addActionListener(this);
		
		this.vue.setTitle(modele.getNom());
		
		afficherMonstreIndexCourant();
	}
	

	public void actionPerformed(ActionEvent e) {
		
		Object source = e.getSource();
		
		if(source == vue.enregistrer)
		{
			sauverMonstreCourant();
			modele.save();
			vue.dispose();
		}
		else if(source == vue.retour)
		{
			afficherMonstreIndexCourant();
		}
		else if(source == vue.suivant)
		{
			sauverMonstreCourant();
			if(index < modele.getNbObj()-1)
			{
				index++;
			}
			afficherMonstreIndexCourant();
		}
		else if(source == vue.precedent)
		{
			sauverMonstreCourant();
			if(index > 0)
			{
				index--;
			}
			afficherMonstreIndexCourant();
		}
	}
	
	private void afficherMonstreIndexCourant()
	{
		ArrayList<String> unObj = modele.getObj(index);
		for(int i = 0; i < vue.textFields.size(); i++)
		{
			vue.textFields.get(i).setText(unObj.get(i));
		}

	}
	
	private void sauverMonstreCourant()
	{	
		ArrayList<String> unObj = modele.getObj(index);
		for(int i = 0; i < vue.textFields.size(); i++)
		{
			unObj.set(i, vue.textFields.get(i).getText());
		}
	}
	
}
