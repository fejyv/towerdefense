package outil.FicGUI;


import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


@SuppressWarnings("serial")
public class InformationInterface extends JFrame {
	
	private JPanel pannelPrincipal;
	private JPanel pannelValeurs;
	private JPanel pannelGestionBoutons;
	public JButton precedent;
	public JButton suivant;
	public JButton enregistrer;
	public JButton retour;
	
	@SuppressWarnings("unused")
	private ArrayList<String> parametres;
	private ArrayList<JLabel> labels;
	public ArrayList<JTextField> textFields;
	
	
	public InformationInterface(ArrayList<String> attributs)
	{
		//Frame
		this.setSize(300,attributs.size()*20+50);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.parametres = attributs;
		this.labels = new ArrayList<JLabel>();
		this.textFields = new ArrayList<JTextField>();
		//Pannels
		pannelPrincipal = new JPanel(new BorderLayout());
		pannelValeurs = new JPanel(new GridLayout(attributs.size(), 2));
		pannelGestionBoutons = new JPanel();
		
		//Boutons
		precedent = new JButton("<");
		suivant = new JButton(">");
		enregistrer = new JButton("Save & quit");
		retour = new JButton("Cancel");
		
		//Labels
		for(String param : attributs)
		{
			labels.add(new JLabel(param + " :"));
			textFields.add(new JTextField());
		}
		
		//Imbrication des pannels
		pannelPrincipal.add(pannelValeurs, BorderLayout.CENTER);
		pannelPrincipal.add(pannelGestionBoutons, BorderLayout.SOUTH);
		
		//Setup du pannelValeurs
		for(int i = 0; i < attributs.size(); i++)
		{
			pannelValeurs.add(labels.get(i));
			pannelValeurs.add(textFields.get(i));
		}
		
		pannelGestionBoutons.add(precedent);
		pannelGestionBoutons.add(suivant);
		pannelGestionBoutons.add(enregistrer);
		pannelGestionBoutons.add(retour);
		
		this.setContentPane(pannelPrincipal);

	}
}
