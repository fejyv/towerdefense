package outil;

/**
 * 
 * @author Florian
 *
 */
public enum Direction {
	Gauche,
	Droite,
	Haut,
	Bas;	
}
