
package outil;

import java.util.ArrayList;

import com.badlogic.gdx.math.Vector2;

import jeu.AJeu;
import model.Carte;
import model.batiment.NomTypeBatiment;
import model.caseMod.ACase;

/**
 * 
 * @author Florian
 *
 */
public class Dijkstra {
	
	/**
	 * Calcul le chemin le plus court en partant de la zone de pop � la zone d'arriv�
	 * V�rification qu'il reste au moins un chemin pour les monstres qui apparaissent
	 * @param carte
	 * @return
	 */
	public static Boolean calculDijkstra(AJeu game){
		
		Carte carte = game.getCarte();
		
		Vector2 tailleMap = new Vector2(carte.getCase().get(0).size(), carte.getCase().size());
		
		
		ArrayList<ArrayList<Integer>> tabValeurs = new ArrayList<ArrayList<Integer>>();
		
		for(int i = 0 ; i < tailleMap.y ; i++){
			ArrayList<Integer> tabValeursTemp = new ArrayList<Integer>();
			for(int j = 0 ; j < tailleMap.x ; j++){
				tabValeursTemp.add(Integer.MAX_VALUE);
			}
			tabValeurs.add(tabValeursTemp);
		}
		
		
		boolean caseFinTrouver = false;
		int cpt = 0;
		ACase caseActu = carte.getCaseDeb(); 
		
		ArrayList<ACase> caseATester = new ArrayList<ACase>();
		caseATester.add(caseActu);
		
		
		int posX, posY, posTestX, posTestY;
		
		tabValeurs.get((int)caseActu.getPosition().y).set((int)caseActu.getPosition().x, caseActu.getValeurPassage());
		
		while(cpt < caseATester.size()){
			caseActu = caseATester.get(cpt);
			if(caseActu == carte.getCaseFin()){
				caseFinTrouver = true;
			}
			
			posX = (int)caseActu.getPosition().x;
			posY = (int)caseActu.getPosition().y;
			
			
			if(caseActu.directContains(Direction.Gauche)){
				ACase caseTester = caseActu.getCaseVoisine(Direction.Gauche);
				if(game.verifCaseBatiment(caseTester, NomTypeBatiment.Barricade, false) == -1 && carte.getCaseNiv2().get((int)caseTester.getPosition().y).get((int)caseTester.getPosition().x).getValeurPassage() != 999){
					posTestX = (int)caseTester.getPosition().x;
					posTestY = (int)caseTester.getPosition().y;
					
					if(tabValeurs.get(posTestY).get(posTestX) > tabValeurs.get(posY).get(posX) + caseTester.getValeurPassage()){
						
						tabValeurs.get(posTestY).set(posTestX, tabValeurs.get(posY).get(posX) + caseTester.getValeurPassage());
						caseATester.add(caseTester);
					}					
				}
			}
			if(caseActu.directContains(Direction.Droite)){
				ACase caseTester = caseActu.getCaseVoisine(Direction.Droite);
				if(game.verifCaseBatiment(caseTester, NomTypeBatiment.Barricade, false) == -1 && carte.getCaseNiv2().get((int)caseTester.getPosition().y).get((int)caseTester.getPosition().x).getValeurPassage() != 999){
					posTestX = (int)caseTester.getPosition().x;
					posTestY = (int)caseTester.getPosition().y;
					
					if(tabValeurs.get(posTestY).get(posTestX) > tabValeurs.get(posY).get(posX) + caseTester.getValeurPassage()){
						
						tabValeurs.get(posTestY).set(posTestX, tabValeurs.get(posY).get(posX) + caseTester.getValeurPassage());
						caseATester.add(caseTester);
					}					
				}
			}
			if(caseActu.directContains(Direction.Haut)){
				ACase caseTester = caseActu.getCaseVoisine(Direction.Haut);
				if(game.verifCaseBatiment(caseTester, NomTypeBatiment.Barricade, false) == -1 && carte.getCaseNiv2().get((int)caseTester.getPosition().y).get((int)caseTester.getPosition().x).getValeurPassage() != 999){
					posTestX = (int)caseTester.getPosition().x;
					posTestY = (int)caseTester.getPosition().y;
					
					if(tabValeurs.get(posTestY).get(posTestX) > tabValeurs.get(posY).get(posX) + caseTester.getValeurPassage()){
						
						tabValeurs.get(posTestY).set(posTestX, tabValeurs.get(posY).get(posX) + caseTester.getValeurPassage());
						caseATester.add(caseTester);
					}					
				}
			}
			if(caseActu.directContains(Direction.Bas)){
				ACase caseTester = caseActu.getCaseVoisine(Direction.Bas);
				if(game.verifCaseBatiment(caseTester, NomTypeBatiment.Barricade, false) == -1 && carte.getCaseNiv2().get((int)caseTester.getPosition().y).get((int)caseTester.getPosition().x).getValeurPassage() != 999){
					posTestX = (int)caseTester.getPosition().x;
					posTestY = (int)caseTester.getPosition().y;
					
					if(tabValeurs.get(posTestY).get(posTestX) > tabValeurs.get(posY).get(posX) + caseTester.getValeurPassage()){
						
						tabValeurs.get(posTestY).set(posTestX, tabValeurs.get(posY).get(posX) + caseTester.getValeurPassage());
						caseATester.add(caseTester);
					}					
				}
			}
			
			cpt++;
		}
		
		if(caseFinTrouver){
			Boolean cheminFini = false;
			int valeurErreur = 0;
			cpt = 0;
			
			ArrayList<ACase> cheminCase = new ArrayList<ACase>();
			
			cheminCase.add(carte.getCaseFin());
			
			int valeurChemin = 0;
			Direction directChoisi = Direction.Gauche;
			
			while(!cheminFini && valeurErreur < 99999){
				valeurErreur++;
				
				ACase caseChoisi = cheminCase.get(cheminCase.size() - 1);
				
				posX = (int)caseChoisi.getPosition().x;
				posY = (int)caseChoisi.getPosition().y;
				
				valeurChemin = 999;
				
				if(caseChoisi.directContains(Direction.Gauche)){
					if(tabValeurs.get((int)caseChoisi.getCaseVoisine(Direction.Gauche).getPosition().y).get((int)caseChoisi.getCaseVoisine(Direction.Gauche).getPosition().x) < valeurChemin){
						valeurChemin = tabValeurs.get((int)caseChoisi.getCaseVoisine(Direction.Gauche).getPosition().y).get((int)caseChoisi.getCaseVoisine(Direction.Gauche).getPosition().x);
						directChoisi = Direction.Gauche;
					}
				}
				
				if(caseChoisi.directContains(Direction.Droite)){
					if(tabValeurs.get((int)caseChoisi.getCaseVoisine(Direction.Droite).getPosition().y).get((int)caseChoisi.getCaseVoisine(Direction.Droite).getPosition().x) < valeurChemin){
						valeurChemin = tabValeurs.get((int)caseChoisi.getCaseVoisine(Direction.Droite).getPosition().y).get((int)caseChoisi.getCaseVoisine(Direction.Droite).getPosition().x);
						directChoisi = Direction.Droite;
					}
				}
				if(caseChoisi.directContains(Direction.Haut)){
					if(tabValeurs.get((int)caseChoisi.getCaseVoisine(Direction.Haut).getPosition().y).get((int)caseChoisi.getCaseVoisine(Direction.Haut).getPosition().x) < valeurChemin){
						valeurChemin = tabValeurs.get((int)caseChoisi.getCaseVoisine(Direction.Haut).getPosition().y).get((int)caseChoisi.getCaseVoisine(Direction.Haut).getPosition().x);
						directChoisi = Direction.Haut;
					}
				}
				if(caseChoisi.directContains(Direction.Bas)){
					if(tabValeurs.get((int)caseChoisi.getCaseVoisine(Direction.Bas).getPosition().y).get((int)caseChoisi.getCaseVoisine(Direction.Bas).getPosition().x) < valeurChemin){
						valeurChemin = tabValeurs.get((int)caseChoisi.getCaseVoisine(Direction.Bas).getPosition().y).get((int)caseChoisi.getCaseVoisine(Direction.Bas).getPosition().x);
						directChoisi = Direction.Bas;
					}
				}
				
				if(caseChoisi.getCaseVoisine(directChoisi) != carte.getCaseDeb()){
					cheminCase.add(caseChoisi.getCaseVoisine(directChoisi));
				}
				else{
					cheminFini = true;
				}
			}
			
			if(!cheminFini){
				caseFinTrouver = false;
			}
		}
		
		
		return caseFinTrouver;
	}
	
	
	/**
	 * Calcul le chemin le plus court en partant d'une case quelconque � la zone d'arriv�
	 * Renvoi une liste de case pour faire le chemin de la case � la fin
	 * @param carte
	 * @param caseDebut
	 * @return
	 */
	public static ArrayList<ACase> calculDijkstra(AJeu game, ACase caseDebut){
		
		Carte carte = game.getCarte();
		
		Vector2 tailleMap = new Vector2(carte.getCase().get(0).size(), carte.getCase().size());
				
		ArrayList<ArrayList<Integer>> tabValeurs = new ArrayList<ArrayList<Integer>>();
		
		for(int i = 0 ; i < tailleMap.y ; i++){
			ArrayList<Integer> tabValeursTemp = new ArrayList<Integer>();
			for(int j = 0 ; j < tailleMap.x ; j++){
				tabValeursTemp.add(Integer.MAX_VALUE);
			}
			tabValeurs.add(tabValeursTemp);
		}
		
		
		boolean caseFinTrouver = false;
		int cpt = 0;
		ACase caseActu = caseDebut; 
		
		ArrayList<ACase> caseATester = new ArrayList<ACase>();
		caseATester.add(caseActu);
		
		
		int posX, posY, posTestX, posTestY;
		
		tabValeurs.get((int)caseActu.getPosition().y).set((int)caseActu.getPosition().x, caseActu.getValeurPassage());
		
		while(cpt < caseATester.size()){
			caseActu = caseATester.get(cpt);
			if(caseActu == carte.getCaseFin()){
				caseFinTrouver = true;
			}
			
			posX = (int)caseActu.getPosition().x;
			posY = (int)caseActu.getPosition().y;
			
			
			if(caseActu.directContains(Direction.Gauche)){
				ACase caseTester = caseActu.getCaseVoisine(Direction.Gauche);
				if(game.verifCaseBatiment(caseTester, NomTypeBatiment.Barricade, false) == -1 && carte.getCaseNiv2().get((int)caseTester.getPosition().y).get((int)caseTester.getPosition().x).getValeurPassage() != 999){
					posTestX = (int)caseTester.getPosition().x;
					posTestY = (int)caseTester.getPosition().y;
					
					if(tabValeurs.get(posTestY).get(posTestX) > tabValeurs.get(posY).get(posX) + caseTester.getValeurPassage()){
						
						tabValeurs.get(posTestY).set(posTestX, tabValeurs.get(posY).get(posX) + caseTester.getValeurPassage());
						caseATester.add(caseTester);
					}					
				}
			}
			if(caseActu.directContains(Direction.Droite)){
				ACase caseTester = caseActu.getCaseVoisine(Direction.Droite);
				if(game.verifCaseBatiment(caseTester, NomTypeBatiment.Barricade, false) == -1 && carte.getCaseNiv2().get((int)caseTester.getPosition().y).get((int)caseTester.getPosition().x).getValeurPassage() != 999){
					posTestX = (int)caseTester.getPosition().x;
					posTestY = (int)caseTester.getPosition().y;
					
					if(tabValeurs.get(posTestY).get(posTestX) > tabValeurs.get(posY).get(posX) + caseTester.getValeurPassage()){
						
						tabValeurs.get(posTestY).set(posTestX, tabValeurs.get(posY).get(posX) + caseTester.getValeurPassage());
						caseATester.add(caseTester);
					}					
				}
			}
			if(caseActu.directContains(Direction.Haut)){
				ACase caseTester = caseActu.getCaseVoisine(Direction.Haut);
				if(game.verifCaseBatiment(caseTester, NomTypeBatiment.Barricade, false) == -1 && carte.getCaseNiv2().get((int)caseTester.getPosition().y).get((int)caseTester.getPosition().x).getValeurPassage() != 999){
					posTestX = (int)caseTester.getPosition().x;
					posTestY = (int)caseTester.getPosition().y;
					
					if(tabValeurs.get(posTestY).get(posTestX) > tabValeurs.get(posY).get(posX) + caseTester.getValeurPassage()){
						
						tabValeurs.get(posTestY).set(posTestX, tabValeurs.get(posY).get(posX) + caseTester.getValeurPassage());
						caseATester.add(caseTester);
					}					
				}
			}
			if(caseActu.directContains(Direction.Bas)){
				ACase caseTester = caseActu.getCaseVoisine(Direction.Bas);
				if(game.verifCaseBatiment(caseTester, NomTypeBatiment.Barricade, false) == -1 && carte.getCaseNiv2().get((int)caseTester.getPosition().y).get((int)caseTester.getPosition().x).getValeurPassage() != 999){
					posTestX = (int)caseTester.getPosition().x;
					posTestY = (int)caseTester.getPosition().y;
					
					if(tabValeurs.get(posTestY).get(posTestX) > tabValeurs.get(posY).get(posX) + caseTester.getValeurPassage()){
						
						tabValeurs.get(posTestY).set(posTestX, tabValeurs.get(posY).get(posX) + caseTester.getValeurPassage());
						caseATester.add(caseTester);
					}					
				}
			}
			
			cpt++;
		}

		if(caseFinTrouver){
			Boolean cheminFini = false;
			int valeurErreur = 0;
			cpt = 0;
			
			ArrayList<ACase> cheminCase = new ArrayList<ACase>();
			
			cheminCase.add(carte.getCaseFin());
			
			int valeurChemin = 0;
			Direction directChoisi = Direction.Gauche;
			
			while(!cheminFini && valeurErreur < 99999){
				ACase caseChoisi = cheminCase.get(cheminCase.size() - 1);
				
				posX = (int)caseChoisi.getPosition().x;
				posY = (int)caseChoisi.getPosition().y;
				
				valeurChemin = 999;
				
				if(caseChoisi.directContains(Direction.Gauche)){
					if(tabValeurs.get((int)caseChoisi.getCaseVoisine(Direction.Gauche).getPosition().y).get((int)caseChoisi.getCaseVoisine(Direction.Gauche).getPosition().x) < valeurChemin){
						valeurChemin = tabValeurs.get((int)caseChoisi.getCaseVoisine(Direction.Gauche).getPosition().y).get((int)caseChoisi.getCaseVoisine(Direction.Gauche).getPosition().x);
						directChoisi = Direction.Gauche;
					}
				}
				
				if(caseChoisi.directContains(Direction.Droite)){
					if(tabValeurs.get((int)caseChoisi.getCaseVoisine(Direction.Droite).getPosition().y).get((int)caseChoisi.getCaseVoisine(Direction.Droite).getPosition().x) < valeurChemin){
						valeurChemin = tabValeurs.get((int)caseChoisi.getCaseVoisine(Direction.Droite).getPosition().y).get((int)caseChoisi.getCaseVoisine(Direction.Droite).getPosition().x);
						directChoisi = Direction.Droite;
					}
				}
				if(caseChoisi.directContains(Direction.Haut)){
					if(tabValeurs.get((int)caseChoisi.getCaseVoisine(Direction.Haut).getPosition().y).get((int)caseChoisi.getCaseVoisine(Direction.Haut).getPosition().x) < valeurChemin){
						valeurChemin = tabValeurs.get((int)caseChoisi.getCaseVoisine(Direction.Haut).getPosition().y).get((int)caseChoisi.getCaseVoisine(Direction.Haut).getPosition().x);
						directChoisi = Direction.Haut;
					}
				}
				if(caseChoisi.directContains(Direction.Bas)){
					if(tabValeurs.get((int)caseChoisi.getCaseVoisine(Direction.Bas).getPosition().y).get((int)caseChoisi.getCaseVoisine(Direction.Bas).getPosition().x) < valeurChemin){
						valeurChemin = tabValeurs.get((int)caseChoisi.getCaseVoisine(Direction.Bas).getPosition().y).get((int)caseChoisi.getCaseVoisine(Direction.Bas).getPosition().x);
						directChoisi = Direction.Bas;
					}
				}
				
				if(caseChoisi.getCaseVoisine(directChoisi) != caseDebut){
					cheminCase.add(caseChoisi.getCaseVoisine(directChoisi));
				}
				else{
					cheminFini = true;
				}
			}

			if(!cheminFini){
				return null;
			}
			
			cheminCase = Outils.reverse(cheminCase);
					
			return cheminCase;
		}
		
		return null;
	}
	
	
}





















