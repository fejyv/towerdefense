package outil.typeFichier;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * 
 * @author Florian
 *
 */
public class FichierVague extends AFichier{
		
	/**
	* Chargement d'un fichier et d�coupage de ces donn�es en un tableau 2D en fonction d'un nombre ind�terminer de ligne et de colonne
	* Le changement de "ligne" s'effectu lors de l'apparition de la valeur "-1", le chargement se finit lors de l'apparition de la valeur "-9" et de la fin du fichier
	* valFichierNonCorrompu : Permet de v�rifier si le fichier correspond au fichier voulu
	* @param nameFile
	* @param valFichierNonCorrompu
	*/
	public FichierVague(final String nameFile, final String valFichierNonCorrompu){
		m_tabDonnee = new ArrayList<ArrayList<String>>();
		
		String erreur = "";
		
		try
		{
			BufferedReader buff = new BufferedReader(new FileReader(nameFile));
			try
			{
				String ligneTemp;
				
				if((ligneTemp = buff.readLine()) != null){
					
					if(ligneTemp.equals(valFichierNonCorrompu)){
						
						int cptX = 0, cptY = 0;
						boolean fini = false, vagueFini = false;
						
						while (!fini) 
						{ 
							cptX = 0;
							ArrayList<String> m_tabDonneeTemp = new ArrayList<String>();
							vagueFini = false;
						
							while (!fini && !vagueFini) 
							{ 
								if((ligneTemp = buff.readLine()) != null){	
									if(ligneTemp.equals("-9")){
										fini = true;
										if(cptX != 0){
											erreur = "Fichier corrompu : attribut -9 sortie � la colonne " + cptX + " de la ligne " + cptY;
										}
									}
									else if(ligneTemp.equals("-1")){
										vagueFini = true;
									}
									else{
										String strTemp = ligneTemp;
										m_tabDonneeTemp.add(strTemp);
									}
								}
								else{
									fini = true;
									erreur = "Fichier corrompu : attribut -9 n'est pas apparu";
								}
								cptX++;
							}							
							if(!fini){
								m_tabDonnee.add(m_tabDonneeTemp);
							}
							cptY++;
						}
						
					}
					else{
						erreur = "Fichier corrompu : " + ligneTemp + " != " + valFichierNonCorrompu;
					}
				}
			}
			finally
			{
				// dans tous les cas, on ferme nos flux
				buff.close();
			}
		}
		catch (IOException ioe) {
			// erreur de fermeture des flux
			erreur = "Fichier corrompu : " + ioe.toString();
		}
		
		if(!erreur.equals("")){
			System.out.println("Fichier " + valFichierNonCorrompu + " charg�");
		}
		
	}
	
	/**
	* Renvoi "-1" car il est impossible de boucler sur une colonne en particulier car chaque ligne � son propre nombre de colonne
	* @param numTest
	* @param valTest
	* @return
	*/
	public int trouverInfoVal(final int numTest, final int valTest){
		return -1;
	}
	
	/**
	* Renvoi "{-1, -1}" car il est impossible de boucler sur une colonne en particulier car chaque ligne � son propre nombre de colonne
	* @param valTest
	* @return
	*/
	public int [] trouverInfoVal(final int valTest){
		return new int[] {-1, -1};
	}

	
	//Pour les tableaux de donn�es avec un nombre de colonne indetermin�
    public int getNbColonne(final int ligne){
        if(ligne >= 0 && ligne < m_tabDonnee.size()){
            return m_tabDonnee.get(ligne).size();
        }
        return -1;
    }
	
}

