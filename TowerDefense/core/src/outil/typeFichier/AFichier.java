package outil.typeFichier;

import java.util.ArrayList;

/**
 * 
 * @author Florian
 *
 */
public abstract class AFichier {
	
	protected ArrayList<ArrayList<String>> m_tabDonnee;
	
	public AFichier(){
		m_tabDonnee = new ArrayList<ArrayList<String>>();
	}
	
	/**
	* Recherche un donn�e particuli�re dans chaque lignes du tableau en fonction d'une colonne particuli�re
	* Renvoi la ligne correspondante � la donn�e
	* @param numTest
	* @param valTest
	* @return
	*/
	public abstract int trouverInfoVal(final int numTest, final int valTest);
	
	/**
	* Recherche un donn�e particuli�re dans toutes les cases du tableau 2D
	* Renvoi un tableau de 2 entiers repr�sentant les coordonn�es (y, x) de la position de la donn�e dans le tableau
	* @param valTest
	* @return
	*/
	public abstract int [] trouverInfoVal(final int valTest);
	
	/**
	* R�cup�re une donn�e en fonction de son num�ro de ligne et son num�ro de colonne
	* Return un String
	* @param numLigne
	* @param numColonne
	* @return
	*/
	public String getInfo(final int numLigne, final int numColonne){
		if(numLigne < 0 || numLigne >= m_tabDonnee.size()){
			return "Num�ro de ligne erronn�e : " + numLigne;
		}
		if(numColonne < 0 || numColonne >= m_tabDonnee.get(numLigne).size()){
			return "Num�ro de colonne erronn�e : " + numColonne;
		}

		return m_tabDonnee.get(numLigne).get(numColonne);
	}
	
	/**
	* R�cup�re une donn�e en fonction de son num�ro de ligne et son num�ro de colonne
	* Return un int
	* @param numLigne
	* @param numColonne
	* @return
	*/
	public int getInfoInt(final int numLigne, final int numColonne){
		if(numLigne < 0 || numLigne >= m_tabDonnee.size()){
			return -1;
		}
		if(numColonne < 0 || numColonne >= m_tabDonnee.get(numLigne).size()){
			return -1;
		}

		return Integer.parseInt(m_tabDonnee.get(numLigne).get(numColonne));
	}
	
	/**
	* R�cup�re une donn�e en fonction de son num�ro de ligne et son num�ro de colonne
	* Return un int
	* @param numLigne
	* @param numColonne
	* @return
	*/
	public double getInfoDouble(final int numLigne, final int numColonne){
		if(numLigne < 0 || numLigne >= m_tabDonnee.size()){
			return -1;
		}
		if(numColonne < 0 || numColonne >= m_tabDonnee.get(numLigne).size()){
			return -1;
		}

		return Double.parseDouble(m_tabDonnee.get(numLigne).get(numColonne));
	}
	
	public ArrayList<String> getLigne(final int numLigne){
		return m_tabDonnee.get(numLigne);
	}
	
	public int getNbLigne(){
		return m_tabDonnee.size();
	}
	
	//Valable pour un tableau de donn�es contenant toujours le meme nombre de colonne
	public int getNbColonne(){
		if(m_tabDonnee.size() > 0){
			return m_tabDonnee.get(0).size();
		}
		
		return -1;
	}
	
	public void affiche(){
		System.out.println();
		for(int j = 0 ; j < m_tabDonnee.size() ; j++){
			for(int i = 0 ; i < m_tabDonnee.get(j).size() ; i++){
				System.out.print(m_tabDonnee.get(j).get(i) + " ");
			}
			System.out.println();
		}
		System.out.println();
	}

}







