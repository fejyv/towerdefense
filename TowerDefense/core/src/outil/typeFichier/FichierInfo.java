package outil.typeFichier;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * 
 * @author Florian
 *
 */
public class FichierInfo extends AFichier{
	
	private String nameFile;
	private Boolean decoupageLigne;
	private String valFichierNonCorrompu;
	
	/**
	* Chargement d'un fichier et d�coupage de ces donn�es en un tableau 2D en fonction d'un nombre de ligne et de colonne d�fini dans le fichier
	* valFichierNonCorrompu : Permet de v�rifier si le fichier correspond au fichier voulu
	* decoupageLigne : 
	* 	- Si � faux : d�coupage est en fonction du retour � la ligne
	*	- Si � vrai : d�coupage en fonction de l'espace
	* @param nameFile
	* @param valFichierNonCorrompu
	* @param decoupageLigne
	*/
	public FichierInfo(final String nameFile, final String valFichierNonCorrompu, final Boolean decoupageLigne){
		this.decoupageLigne = decoupageLigne;
		this.nameFile = nameFile;
		this.valFichierNonCorrompu = valFichierNonCorrompu;
		
		m_tabDonnee = new ArrayList<ArrayList<String>>();
		
		String erreur = "";
		
		try
		{
			BufferedReader buff = new BufferedReader(new FileReader(nameFile));
			try
			{
				String ligneTemp;
				int nbLigne = 0, nbColonne = 0;
				
				if((ligneTemp = buff.readLine()) != null){
					
					if(ligneTemp.equals(valFichierNonCorrompu)){
						
						if((ligneTemp = buff.readLine()) != null){
							if(Integer.parseInt(ligneTemp) != -9){
								nbLigne = Integer.parseInt(ligneTemp);
								
								if((ligneTemp = buff.readLine()) != null){
									if(Integer.parseInt(ligneTemp) != -9){
										nbColonne = Integer.parseInt(ligneTemp);
										
										int cptX = 0, cptY = 0;
										boolean fini = false;
										
										while (!fini && cptY < nbLigne) 
										{ 
											cptX = 0;
											ArrayList<String> m_tabDonneeTemp = new ArrayList<String>();
											
											if(decoupageLigne){
												while (!fini && cptX < nbColonne) 
												{ 
													if((ligneTemp = buff.readLine()) != null){
														if(ligneTemp == "-9"){
															fini = true;
															if(cptX != 0){
																erreur = "Fichier corrompu : attribut -9 sortie � la colonne " + cptX + " de la ligne " + cptY;
															}
														}
														else{
															String strTemp = ligneTemp;
															m_tabDonneeTemp.add(strTemp);
														}
													}
													else{
														fini = true;
														erreur = "Fichier corrompu : attribut -9 n'est pas apparu";
													}
													cptX++;
												}
											}
											else{

												if((ligneTemp = buff.readLine()) != null){
													if(ligneTemp.equals("-9")){
														fini = true;
													}
													else{

														//Decoupage de la ligne
														boolean nextIter = true;
														boolean finIter = false;
														String strTempDecoupe = "";
														
														for(int i = 0 ; i < ligneTemp.length() ; i++){
															if(ligneTemp.charAt(i) != ' '){
																if(nextIter){
																	strTempDecoupe = "";
																}
																strTempDecoupe += ligneTemp.charAt(i);
																nextIter = false;
																
																if(i == ligneTemp.length() - 1){
																	finIter = true;
																}
															}
															else{
																if(!nextIter){
																	finIter = true;
																}
																nextIter = true;
															}
															
															if(finIter){
																finIter = false;
																String strTemp = strTempDecoupe;
																m_tabDonneeTemp.add(strTemp);
															}
														}
													}
												}
												else{
													fini = true;
													erreur = "Fichier corrompu : attribut -9 n'est pas apparu";
												}
											}
											
											
											
											if(!fini){
												m_tabDonnee.add(m_tabDonneeTemp);
											}
											cptY++;
										}
									}
									else{
										erreur = "Fichier corrompu : Nombre de colonne erron�es : " + ligneTemp;
									}
								}
							}
							else{
								erreur = "Fichier corrompu : Nombre de ligne erron�es : " + ligneTemp;
							}
						}
					}
					else{
						erreur = "Fichier corrompu : " + ligneTemp + " != " + valFichierNonCorrompu;
					}
				}
				
				
			}
			finally
			{
				// dans tous les cas, on ferme nos flux
				buff.close();
			}
		}
		catch (IOException ioe) {
			// erreur de fermeture des flux
			erreur = "Fichier corrompu : " + ioe.toString();
		}
		
		if(!erreur.equals("")){
			System.out.println("Fichier " + valFichierNonCorrompu + " charg�");
		}
	}
	
	/**
	* Recherche un donn�e particuli�re dans chaque lignes du tableau en fonction d'une colonne particuli�re
	* Renvoi la ligne correspondante � la donn�e
	* @param numTest
	* @param valTest
	* @return
	*/
	public int trouverInfoVal(final int numTest, final int valTest){
		boolean trouver = false;
		int cpt = 0;
		while(!trouver && cpt < getNbLigne()){
			if(getInfoInt(cpt, numTest) == valTest){
				trouver = true;
			}
			cpt++;
		}
		if(trouver){
			cpt--;
			return cpt;
		}
		return -1;
	}
	
	/**
	* Recherche un donn�e particuli�re dans toutes les cases du tableau 2D
	* Renvoi un tableau de 2 entiers repr�sentant les coordonn�es (y, x) de la position de la donn�e dans le tableau
	* @param valTest
	* @return
	*/
	public int [] trouverInfoVal(final int valTest){
		boolean trouver = false;
		int cptY = 0, cptX = 0;
		while(!trouver && cptY < getNbLigne()){
			cptX = 0;
			while(!trouver && cptX < getNbColonne()){
				if(getInfoInt(cptY, cptX) == valTest){
					trouver = true;
				}
				cptX++;
			}
			cptY++;
		}
		if(trouver){
			cptY--;
			cptX--;
			return new int[] {cptY, cptX};
		}
		return new int[] {-1, -1};
	}
	
	public void save(ArrayList<ArrayList<String>> lignes)
	{
		try {
			File fichier = new File(nameFile); 
			if(fichier.exists())
			{
				fichier.delete();
			}
			fichier.createNewFile();
			
			FileWriter flux=new FileWriter(fichier); 
			PrintWriter writer = new PrintWriter(flux);
			
			writer.println(valFichierNonCorrompu);
			writer.println(lignes.size());
			writer.println(this.getNbColonne());

			if(decoupageLigne)
			{
				for(ArrayList<String> ligne : lignes)
				{
					for(String colonne : ligne)
					{
						writer.println(colonne);
					}
				}
			}
			else
			{
				for(ArrayList<String> ligne : lignes)
				{
					String ligneTmp = "";
					for(String colonne : ligne)
					{
						ligneTmp += colonne + " ";
					}
					writer.println(ligneTmp);
				}
			}
			writer.println("-9");
			
			writer.close();
			flux.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public String getNom()
	{
		String[] tab = nameFile.split("/");
		return tab[tab.length-1];
	}
	
}







