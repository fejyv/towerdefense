package outil;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * 
 * @author Florian
 *
 */
public class Constantes {
	
	private static Properties prop;
	
	public static void init(final String file){
		prop = new Properties();
		try {
			prop.load(new FileInputStream(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		//System.out.println(prop);
	}
	
	public static String get(final String cle){
		return prop.getProperty(cle);
	}
	
	public static float getFloat(final String cle){
		return Float.parseFloat(prop.getProperty(cle));
	}
	
	public static int getInt(final String cle){
		return Integer.parseInt(prop.getProperty(cle));
	}
	
	
	
}
