package outil;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

import model.caseMod.ACase;

/**
 * 
 * @author Florian
 *
 */
public class Outils {
		
	/**
	 * Renvoie la coordonn�e en fonction de l'axe X-Y avec le 0-0 en haut � gauche
	 * @param posYActu
	 * @return
	 */
	public static float getCoordoY(final float posYActu){
		return Gdx.graphics.getHeight() - posYActu;
	}
	
	/**
	 * Passe des coordonn�es pixels en coordonn�es cases
	 * @param pos
	 * @return
	 */
	public static int coordoToCase(final float pos){
		return (int)(pos / Constantes.getFloat("TAILLE_IMAGE"));
	}
	
	public static float caseToCoordo(final float pos){
		return (int)(pos * Constantes.getFloat("TAILLE_IMAGE"));
	}

	public static Vector2 caseToCoordo(final ACase caseActu){
		Vector2 temp = new Vector2(Outils.caseToCoordo(caseActu.getPosition().x), Outils.getCoordoY(Outils.caseToCoordo(caseActu.getPosition().y)) );
		
		return temp;
	}
	
	public static Vector2 getCoordObjMilieu(final Vector2 coordReelles)
	{
		Vector2 temp = new Vector2();
		temp.x = coordReelles.x - Constantes.getFloat("TAILLE_IMAGE")/2;
		temp.y = coordReelles.y - Constantes.getFloat("TAILLE_IMAGE")/2;
		return temp;
	}
	
	/**
	 * Retourne une ArrayList
	 * @param list
	 * @return
	 */
	public static <T> ArrayList<T> reverse(ArrayList<T> list) {
	    int length = list.size();
	    ArrayList<T> result = new ArrayList<T>(length);

	    for (int i = length - 1; i >= 0; i--) {
	        result.add(list.get(i));
	    }

	    return result;
	}
}
















