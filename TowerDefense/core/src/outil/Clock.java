package outil;


/**
 * 
 * @author Florian
 *
 */
public class Clock {
	private long m_tempsDebut;
	
	public Clock(){
		restart();
	}
	
	/**
	 * R�initialise le clock
	 */
	public void restart(){
		m_tempsDebut = System.currentTimeMillis();
	}
	
	/**
	 * R�cup�re le temps en milliseconde
	 * @return
	 */
	public long getElapsedTimeMillis(){
		return System.currentTimeMillis() - m_tempsDebut;
	}
	
	/**
	 * R�cup�re le temps en seconde
	 * @return
	 */
	public double getElapsedTimeSecond(){
		return (double)getElapsedTimeMillis() / 1000;
	}
}
