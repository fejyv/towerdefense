package outil;

import model.caseMod.ACase;

public enum ClassName {
	
	
	
	PatternTourEtoile(model.batiment.tour.pattern.PatternTourEtoile.class, new Class[]{int.class, ACase.class}),
	PatternTourCroix(model.batiment.tour.pattern.PatternTourCroix.class, new Class[]{int.class, ACase.class}),
	PatternTirStandard(model.tir.pattern.PatternTirStandard.class, new Class[]{ int.class, double.class});
	
	@SuppressWarnings("rawtypes")
	private Class className;
	@SuppressWarnings("rawtypes")
	private Class[] paramsTypes;
	
	@SuppressWarnings("rawtypes")
	ClassName(Class className, Class[] paramsTypes) {
		this.className = className;
		this.paramsTypes = paramsTypes;
	}
	
	public String getClassName() {
		return this.className.getName();
	}
	
	public String getPackage() {
		return this.className.getPackage().getName();
	}
	
	@SuppressWarnings("rawtypes")
	public Class[] getParamsTypes() {
		return this.paramsTypes;
	}
	
	public static ClassName trouverNom(String nom){
		
		for (ClassName name : ClassName.values()) {
			if(name.className.getSimpleName().equals(nom)){
				return name;
			}
		}
		
		return null;
	}
}
