package typeElement;

import com.badlogic.gdx.math.Vector2;

/**
 * 
 * @author Florian
 *
 */
public interface IDeplacable {

	/**
	 * Classe implemantant les méthodes des objets déplacable tel que les tirs ou les monstres
	 */
	
	public void deplacer();
	public boolean estMort();
	public void disparaitre();
	public int getTypeNum();
	public Vector2 getPositionPixel();
}
