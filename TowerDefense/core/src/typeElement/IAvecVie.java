package typeElement;

import com.badlogic.gdx.math.Vector2;

public interface IAvecVie {
	public abstract float getPoucentageVie();
	public abstract Vector2 getPositionPixel();
}
