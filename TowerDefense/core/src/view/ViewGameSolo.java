package view;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;

import control.AControlGame;
import jeu.JeuSolo;
import model.batiment.piege.APiege;
import model.batiment.tour.ATour;
import model.caseMod.ACase;
import model.monstre.AMonstreCarte;
import model.tir.ATir;
import outil.Direction;
import outil.Outils;
import typeElement.IAvecVie;
import typeElement.IDeplacable;
import model.batiment.ABatiment;
import model.batiment.Barricade;

/**
 * 
 * @author Florian
 *
 */
public class ViewGameSolo extends AViewGame{
    
    /**
     * 
     * @param game
     * @param control
     */
	public ViewGameSolo(JeuSolo game, AControlGame control){
		
		super(game, control);

		textArgent.setY(Outils.getCoordoY(100));
		textVie.setY(Outils.getCoordoY(125));
		textScore.setY(Outils.getCoordoY(150));
	}
	
	/**
	 * Affiche la grille de jeu, les tours et les monstres
	 */
	public void afficher(){
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.graphics.getGL20().glClearColor( 1, 0, 0, 1 );
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		
		
		//Mise a jour du temps pass�
        stateTime += Gdx.graphics.getDeltaTime();           
        
        //Debut des draws
        spriteBatch.begin();
        
				
        affichageMagasin();
        
		affichageCarte();
        
		affichageBatiment();
        
        
        if(game.getTourEnModif() != -1){
        	//Affichage d'une croix sur la tour en cours, ainsi que des cases sur lesquelles elle peut tirer
        	ATour tourModif = (ATour) game.getBatiment().get(game.getTourEnModif()); 
        	
			spriteBatch.draw(textureCase[22], tourModif.getCase().getPosition().x * 32, Outils.getCoordoY((tourModif.getCase().getPosition().y * 32) + 32));
            
        	for(ACase caseVisee : tourModif.getModelPatternTour().getCasesTir()){
        		spriteBatch.draw(textureCase[77], caseVisee.getPosition().x * 32, Outils.getCoordoY((caseVisee.getPosition().y * 32) + 32));
        	}
        }
        
        affichageDeplacable();
        
        //Affichage du batiment plac� sous la souris
        switch(game.getTypeObjActu()){
        case Barricade:
        	spriteBatch.draw(textureBarricade[0], Gdx.input.getX() - 16, Outils.getCoordoY(Gdx.input.getY()) - 16);
        	break;
        case Tour:
        	spriteBatch.draw(textureBarricade[0], Gdx.input.getX() - 16, Outils.getCoordoY(Gdx.input.getY()) - 16);
        	spriteBatch.draw(listAnimationTour.get(game.getObjActu()).getKeyFrame(stateTime, true), Gdx.input.getX() - 16, Outils.getCoordoY(Gdx.input.getY()) - 16);
            break;
        case Piege:
        	spriteBatch.draw(listAnimationPiege.get(game.getObjActu()).getKeyFrame(stateTime, true), Gdx.input.getX() - 16, Outils.getCoordoY(Gdx.input.getY()) - 16);
            break;
        default:
        	break;
        }
        
        //Fin des draws
        spriteBatch.end();

        //Affichage des barres de vies
        ShapeRenderer shapeRenderer = new ShapeRenderer();
        for(IAvecVie unVivant : game.getVivants())
        {
        	shapeRenderer.begin(ShapeType.Filled);
        	shapeRenderer.setColor(1, 0, 0, 1);
        	shapeRenderer.rect(unVivant.getPositionPixel().x, unVivant.getPositionPixel().y+34, 32, 4);
        	shapeRenderer.setColor(0, 1, 0, 1);
        	shapeRenderer.rect(unVivant.getPositionPixel().x, unVivant.getPositionPixel().y+34, 32*unVivant.getPoucentageVie(), 4);
        	
        	shapeRenderer.end();
        }

        spriteBatch.begin();
        
        if(control.getAfficheQuitter()){
			spriteBatch.draw(tileQuitter, 0, 0);
        }
        
        //Fin des draws
        spriteBatch.end();
	}
	
	public void affichageMagasin(){
		spriteBatch.draw(tileMagasinFond,640,0);
		
		if(game.getTourEnModif() == -1){
			textVie.setText("Vie : " + this.game.getVieJoueur());
			//Choix des tours et pi�ges
			textVie.draw(spriteBatch, 255);			
			textScore.setText("Score : " + this.game.getScoreJoueur());
			textScore.draw(spriteBatch, 255);
			
			textPrix.setText("Prix : " + this.game.getPrixObjActu(false));
			textPrix.draw(spriteBatch, 255);
			
			textPrixVente.setText("Vente : " + this.game.getPrixObjActu(true));
			textPrixVente.draw(spriteBatch, 255);

			spriteBatch.draw(tileFondObjMag, control.getBtn("btnBarricade").getX(), Outils.getCoordoY(control.getBtn("btnBarricade").getY() + 32));
			spriteBatch.draw(textureBarricade[0], control.getBtn("btnBarricade").getX() + 2, Outils.getCoordoY(control.getBtn("btnBarricade").getY() + 30));
			
			for(int i = 0 ; i < control.getTabBtn("tabBtnTour").size() ; i++){
				spriteBatch.draw(tileFondObjMag, control.getTabBtn("tabBtnTour").get(i).getX(), Outils.getCoordoY(control.getTabBtn("tabBtnTour").get(i).getY() + 32));
				spriteBatch.draw(listTextureTour.get(i)[0], control.getTabBtn("tabBtnTour").get(i).getX() + 2, Outils.getCoordoY(control.getTabBtn("tabBtnTour").get(i).getY() + 30));
			}
			
			for(int i = 0 ; i < control.getTabBtn("tabBtnPiege").size() ; i++){
				spriteBatch.draw(tileFondObjMag, control.getTabBtn("tabBtnPiege").get(i).getX(), Outils.getCoordoY(control.getTabBtn("tabBtnPiege").get(i).getY() + 32));
				spriteBatch.draw(listTexturePiege.get(i)[0], control.getTabBtn("tabBtnPiege").get(i).getX() + 2, Outils.getCoordoY(control.getTabBtn("tabBtnPiege").get(i).getY() + 30));
			}
		}
		else{
			//Affichage des am�lioration de pattern
			
			spriteBatch.draw(tileMagasinChoixAmelioration,640,0);
			
			textPrix.setText("Prix : " + this.game.getPrixAmelioration());
			textPrix.draw(spriteBatch, 255);

			for(int i = 0 ; i < control.getTabBtn("tabPatternTour").size() ; i++){
				spriteBatch.draw(tileFondObjMag, control.getTabBtn("tabPatternTour").get(i).getX(), Outils.getCoordoY(control.getTabBtn("tabPatternTour").get(i).getY() + 32));
				spriteBatch.draw(texturePatternTour[i], control.getTabBtn("tabPatternTour").get(i).getX() + 2, Outils.getCoordoY(control.getTabBtn("tabPatternTour").get(i).getY() + 30));
			}

			for(int i = 0 ; i < control.getTabBtn("tabPatternTir").size() ; i++){
				spriteBatch.draw(tileFondObjMag, control.getTabBtn("tabPatternTir").get(i).getX(), Outils.getCoordoY(control.getTabBtn("tabPatternTir").get(i).getY() + 32));
				spriteBatch.draw(texturePatternTir[i], control.getTabBtn("tabPatternTir").get(i).getX() + 2, Outils.getCoordoY(control.getTabBtn("tabPatternTir").get(i).getY() + 30));
			}
			
			switch(game.getTypeAmelioration()){
			case 0:
				Rectangle rec = control.getTabBtn("tabPatternTour").get(game.getAmelioration());
				spriteBatch.draw(textureCase[78], rec.getX() + 2, Outils.getCoordoY(rec.getY() + 30));
				break;
			case 1:
				Rectangle rec2 = control.getTabBtn("tabPatternTir").get(game.getAmelioration());
				spriteBatch.draw(textureCase[78], rec2.getX() + 2, Outils.getCoordoY(rec2.getY() + 30));
				break;
			}
		
		}

		textArgent.setText("Argent : " + this.game.getArgentJoueur() + "$");
		textArgent.draw(spriteBatch, 255);
	}
	
	public void affichageCarte(){
		//affichage des cases
        for (ArrayList<ACase> uneLigneCase : game.getCarte().getCase()){ 
        	for (ACase uneCase : uneLigneCase){ 
        		spriteBatch.draw(textureCase[uneCase.getNum()], uneCase.getPosition().x * 32, Outils.getCoordoY((uneCase.getPosition().y * 32) + 32));
        		
        		if(!uneCase.directContains(Direction.Gauche)){
        			spriteBatch.draw(textureCase[23], uneCase.getPosition().x * 32, Outils.getCoordoY((uneCase.getPosition().y * 32) + 32));
        		}
        		if(!uneCase.directContains(Direction.Droite)){
        			spriteBatch.draw(textureCase[24], uneCase.getPosition().x * 32, Outils.getCoordoY((uneCase.getPosition().y * 32) + 32));
        		}
        		if(!uneCase.directContains(Direction.Haut)){
        			spriteBatch.draw(textureCase[25], uneCase.getPosition().x * 32, Outils.getCoordoY((uneCase.getPosition().y * 32) + 32));
        		}
        		if(!uneCase.directContains(Direction.Bas)){
        			spriteBatch.draw(textureCase[26], uneCase.getPosition().x * 32, Outils.getCoordoY((uneCase.getPosition().y * 32) + 32));
        		}
        	}
    	}
        for (ArrayList<ACase> uneLigneCase : game.getCarte().getCaseNiv2()){ 
        	for (ACase uneCase : uneLigneCase){ 
        		spriteBatch.draw(textureCase[uneCase.getNum()], uneCase.getPosition().x * 32, Outils.getCoordoY((uneCase.getPosition().y * 32) + 32));
        	}
    	}
        if(game.getCarte().getCaseEnCooldown() != null){
	        for (ACase uneCase : game.getCarte().getCaseEnCooldown()){ 
	    		spriteBatch.draw(textureCase[22], uneCase.getPosition().x * 32, Outils.getCoordoY((uneCase.getPosition().y * 32) + 32));
	    	}
        }
	}
	
	public void affichageBatiment(){
		for (ABatiment unBatiment : game.getBatiment()){ 
        	
        	if(unBatiment instanceof Barricade){
            	spriteBatch.draw(textureBarricade[0], unBatiment.getGestionPosition().getPixelPosition().x, unBatiment.getGestionPosition().getPixelPosition().y - 32);
        	}
        	if(unBatiment instanceof ATour){
            	spriteBatch.draw(listAnimationTour.get(unBatiment.getTypeNum()).getKeyFrame(stateTime, true), unBatiment.getCase().getPosition().x * 32, Outils.getCoordoY((unBatiment.getCase().getPosition().y * 32) + 32));
        	}
        	if(unBatiment instanceof APiege){
            	spriteBatch.draw(listAnimationPiege.get(unBatiment.getTypeNum()).getKeyFrame(stateTime, true), unBatiment.getCase().getPosition().x * 32, Outils.getCoordoY((unBatiment.getCase().getPosition().y * 32) + 32));
        	}
        }
	}
	
	public void affichageDeplacable(){
		for (IDeplacable unDeplacable : game.getDeplacable()){ 
        	
        	if(unDeplacable instanceof ATir){
        		
        		currentFrame = listAnimationTir.get(unDeplacable.getTypeNum()).getKeyFrame(stateTime, true);
        		
        		spriteBatch.draw(currentFrame, ((ATir)unDeplacable).getGestionPos().getPixelPosition().x, ((ATir)unDeplacable).getGestionPos().getPixelPosition().y, 
            			currentFrame.getRegionWidth() / 2f, currentFrame.getRegionHeight() / 2f, currentFrame.getRegionWidth(), currentFrame.getRegionHeight(), 
            			1, 1, ((ATir)unDeplacable).getGestionPos().getRotationTir()); 
        	}
        	else if(unDeplacable instanceof AMonstreCarte){
        		
        		if(((AMonstreCarte)unDeplacable).getDirection() == null){
            		System.out.println("unMonstre.getDirection() null");
            	}
            	else if(unDeplacable.getTypeNum() < 0 || unDeplacable.getTypeNum() >= listDirectAnimation.get(((AMonstreCarte)unDeplacable).getDirection()).size()){
            		System.out.println("unMonstre.getNum() taille");
            	}
            	else if(listDirectAnimation.get(((AMonstreCarte)unDeplacable).getDirection()).get(unDeplacable.getTypeNum()).getKeyFrame(stateTime, true) == null){
            		System.out.println("listDirectAnimation.get(unMonstre.getDirection()).get(unMonstre.getNum()).getKeyFrame(stateTime, true) null");
            	}
            	else 
            		spriteBatch.draw(listDirectAnimation.get(((AMonstreCarte)unDeplacable).getDirection()).get(unDeplacable.getTypeNum()).getKeyFrame(stateTime, true), 
            				unDeplacable.getPositionPixel().x, unDeplacable.getPositionPixel().y);
        	}
		}
	}
	
	public void gameOver()
	{
		spriteBatch.begin();

        affichageMagasin();
		affichageCarte();
		affichageBatiment();
		spriteBatch.draw(MonGameOver,0,120);
		
		spriteBatch.end();
	}
	
	public void gameWin(){
		spriteBatch.begin();
		
        affichageMagasin();
		affichageCarte();
		affichageBatiment();
		spriteBatch.draw(MonGameWin,0,120);
		
		spriteBatch.end();
	}
	
	
	/**
	 * Lorsque l'application est ferm�e, net
	 * 
	 * toye les textures charg�es
	 */
	public void disposeGame(){
		
	}
	
}
