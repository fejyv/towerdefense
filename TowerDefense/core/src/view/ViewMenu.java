package view;

import java.net.InetAddress;
import java.net.UnknownHostException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;


import control.ControlMenu;
import outil.Outils;


/**
 * 
 * @author Florian
 *
 */
public class ViewMenu implements IView {
	
	private ControlMenu control;
	private SpriteBatch spriteBatch;  
	
    private BitmapFont font;

    private Texture tileFond;
    private Texture tileFondPrincipal;
    private Texture tileFondCredit;
    private Texture tileFondMulti;
    private Label textIpLocale;
    private Label textIpDestination;

	public ViewMenu(ControlMenu control) {
		this.control = control;

        //Endroit ou draw l'animation
        spriteBatch = new SpriteBatch();  
		
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("font/SFCartoonistHand.ttf"));
		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		
		parameter.size = 22;
		font = generator.generateFont(parameter); 
		generator.dispose();
		
		LabelStyle textStyle = new LabelStyle(font, Color.BLACK);
		
		tileFond = new Texture(Gdx.files.internal("images/menuFond.png"));
		tileFondPrincipal = new Texture(Gdx.files.internal("images/menuPrincipal.png"));
		tileFondCredit = new Texture(Gdx.files.internal("images/menuCredit.png"));
		tileFondMulti = new Texture(Gdx.files.internal("images/menuMulti.png"));
		
		String ip = "";
		
		try {
			ip = InetAddress.getLocalHost().getHostAddress().toString();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		
		textIpLocale = new Label("IP Locale : " + ip, textStyle);
		textIpLocale.setX(640);
		textIpLocale.setY(Outils.getCoordoY(280));

		textIpDestination = new Label("IP Connexion : " + this.control.getIpConnexion(), textStyle);
		textIpDestination.setX(640);
		textIpDestination.setY(Outils.getCoordoY(400));
	}
	
	public void afficher() {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.graphics.getGL20().glClearColor( 1, 0, 0, 1 );
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		
		
        //Debut des draws
        spriteBatch.begin();
        
        spriteBatch.draw(tileFond, 0, 0);
    
        if(control.getAffichageCredit()){
        	spriteBatch.draw(tileFondCredit, 0, 0);
        }
        else if(control.getAffichageMulti()){
        	spriteBatch.draw(tileFondMulti, 0, 0);

        	textIpLocale.draw(spriteBatch, 255);
    		
            textIpDestination.setText("IP Connexion : " + this.control.getIpConnexion());
    		textIpDestination.draw(spriteBatch, 255);
        }
        else{
        	spriteBatch.draw(tileFondPrincipal, 0, 0);
        }
        
        
    	

        spriteBatch.end();
	}

	public void dispose() {
		tileFond.dispose();
		tileFondPrincipal.dispose();
		tileFondCredit.dispose();
		tileFondMulti.dispose();
		font.dispose();
	}

}
