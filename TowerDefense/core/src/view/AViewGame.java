package view;

import java.util.ArrayList;
import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;

import control.AControlGame;
import jeu.AJeu;
import outil.Constantes;
import outil.Direction;
import outil.Outils;

/**
 * 
 * @author Florian
 *
 */
public abstract class AViewGame implements IView {         

    protected AControlGame control;
    protected SpriteBatch spriteBatch;  
    
	protected Texture gameOver;
	protected TextureRegion MonGameOver;
	protected Texture gameWin;
	protected TextureRegion MonGameWin;
    
	protected Texture tileMagasinFond, tileMagasinChoixAmelioration;

	protected Texture tileFondObjMag;
	
	protected Texture tileQuitter;
    
	protected Texture tilesetMap;            
    protected Texture tilesetMonstre;        
    protected Texture tilesetTour;        
    protected Texture tilesetTir; 
    protected Texture tilesetPiege;   
    protected Texture tilesetPatternTour; 
    protected Texture tilesetPatternTir;
    
    
    protected TextureRegion[]	textureCase;
    protected TextureRegion[]	textureBarricade;          // #6
    protected TextureRegion   currentFrame;
    
    protected ArrayList<Animation> listAnimationBas;
    protected ArrayList<Animation> listAnimationDroite;
    protected ArrayList<Animation> listAnimationGauche;
    protected ArrayList<Animation> listAnimationHaut;
    protected HashMap<Direction, ArrayList<Animation>> listDirectAnimation;
    protected ArrayList<HashMap<Direction, TextureRegion[]>> listDirectTexture;

    protected ArrayList<TextureRegion[]> listTexturePiege;
    protected ArrayList<Animation> listAnimationPiege;
    
    protected ArrayList<TextureRegion[]> listTextureTour;
    protected ArrayList<Animation> listAnimationTour;
    
    protected ArrayList<TextureRegion[]> listTextureTir;
    protected ArrayList<Animation> listAnimationTir;
   
    protected TextureRegion[] texturePatternTour;
    protected TextureRegion[] texturePatternTir;
    
    protected BitmapFont font;
    protected LabelStyle textStyle;
    protected Label textArgent;
    protected Label textVie;
    protected Label textScore;
    protected Label textPrix;
    protected Label textPrixVente;
                                        

    protected AJeu game;
    protected float stateTime;  
    
    

	public AViewGame(AJeu game, AControlGame control){
		this.control = control;
		this.game = game;
        //Endroit ou draw l'animation
        spriteBatch = new SpriteBatch();  
		
		
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("font/SFCartoonistHand.ttf"));
		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		
		parameter.size = 26;
		font = generator.generateFont(parameter); 
		generator.dispose();
		
		textStyle = new LabelStyle(font, Color.BLACK);

		textArgent = new Label("Argent : " + this.game.getArgentJoueur() + "$", textStyle);
		textArgent.setX(715);
		
		textVie = new Label("Vie : " + this.game.getVieJoueur(), textStyle);
		textVie.setX(715);
		
		textScore = new Label("Score : " + this.game.getScoreJoueur(), textStyle);
		textScore.setX(715);
		
		textPrix = new Label("Prix : ", textStyle);
		textPrix.setX(680);
		textPrix.setY(Outils.getCoordoY(610));
		
		textPrixVente = new Label("Vente : ", textStyle);
		textPrixVente.setX(800);
		textPrixVente.setY(Outils.getCoordoY(610));
		
		
        
		gameOver = new Texture(Gdx.files.internal("images/gameOver.png"));
		MonGameOver=new TextureRegion(gameOver, 0, 1, 928, 420); 
		
		gameWin = new Texture(Gdx.files.internal("images/gameWin.png"));
		MonGameWin=new TextureRegion(gameWin, 0, 1, 928, 420); 
		
		tileMagasinFond = new Texture(Gdx.files.internal("images/MagasinFond.png"));
		
		tileMagasinChoixAmelioration = new Texture(Gdx.files.internal("images/MagasinChoixAmelioration.png"));
		
		tileFondObjMag = new Texture(Gdx.files.internal("images/fondObjMag.png"));
		
		tileQuitter = new Texture(Gdx.files.internal("images/vouloirQuitter.png"));
		
        
        //temps pass�
        stateTime = 0f;
		
		//Changement de l'image du curseur (doit �tre une puissance de 2
		Pixmap pm = new Pixmap(Gdx.files.internal("images/curseurNonClique.png"));
		Gdx.graphics.setCursor(Gdx.graphics.newCursor(pm, 0, 0));
		pm.dispose();
		
		//Recuperation des images � decouper
		
		tilesetMap = new Texture(Gdx.files.internal("images/tilesetMap.png")); 
		tilesetMonstre = new Texture(Gdx.files.internal("images/tilesetEnnemi.png")); 
		tilesetTour = new Texture(Gdx.files.internal("images/tilesetTour.png")); 
		tilesetTir = new Texture(Gdx.files.internal("images/tilesetTir.png")); 
		tilesetPiege = new Texture(Gdx.files.internal("images/tilesetPiege.png"));
		tilesetPatternTour = new Texture(Gdx.files.internal("images/tilesetPatternTour.png"));
		tilesetPatternTir = new Texture(Gdx.files.internal("images/tilesetPatternTir.png"));
		
		//Decoupage des tileset		
        TextureRegion[][] tileMap = TextureRegion.split(tilesetMap, tilesetMap.getWidth()/Constantes.getInt("NB_IMG_MAP_COLS"), tilesetMap.getHeight()/Constantes.getInt("NB_IMG_MAP_ROWS"));   
        TextureRegion[][] tileMonstre = TextureRegion.split(tilesetMonstre, tilesetMonstre.getWidth()/Constantes.getInt("NB_IMG_MOB_COLS"), tilesetMonstre.getHeight()/Constantes.getInt("NB_IMG_MOB_ROWS"));  
        TextureRegion[][] tileTour = TextureRegion.split(tilesetTour, tilesetTour.getWidth()/Constantes.getInt("NB_IMG_TOUR_COLS"), tilesetTour.getHeight()/Constantes.getInt("NB_IMG_TOUR_ROWS"));  
        TextureRegion[][] tileTir= TextureRegion.split(tilesetTir, tilesetTir.getWidth()/Constantes.getInt("NB_IMG_TIR_COLS"), tilesetTir.getHeight()/Constantes.getInt("NB_IMG_TIR_ROWS"));       
        TextureRegion[][] tilePiege= TextureRegion.split(tilesetPiege, tilesetPiege.getWidth()/Constantes.getInt("NB_IMG_PIEGE_COLS"), tilesetPiege.getHeight()/Constantes.getInt("NB_IMG_PIEGE_ROWS"));  
        TextureRegion[][] tilePatternTour= TextureRegion.split(tilesetPatternTour, tilesetPatternTour.getWidth()/Constantes.getInt("NB_IMG_PATTERN_TOUR_COLS"), tilesetPatternTour.getHeight()/Constantes.getInt("NB_IMG_PATTERN_TOUR_ROWS")); 
        TextureRegion[][] tilePatternTir= TextureRegion.split(tilesetPatternTir, tilesetPatternTir.getWidth()/Constantes.getInt("NB_IMG_PATTERN_TIR_COLS"), tilesetPatternTir.getHeight()/Constantes.getInt("NB_IMG_PATTERN_TIR_ROWS"));      

		//Texture contenant un type de terrain pour la carte par case
        textureCase = new TextureRegion[Constantes.getInt("NB_IMG_MAP_COLS") * Constantes.getInt("NB_IMG_MAP_ROWS")];
        for(int j = 0 ; j < Constantes.getInt("NB_IMG_MAP_ROWS") ; j++){
            for(int i = 0 ; i < Constantes.getInt("NB_IMG_MAP_COLS") ; i++){
            	textureCase[i + (Constantes.getInt("NB_IMG_MAP_COLS") * j)] = tileMap[j][i];
            }
        }

        textureBarricade = new TextureRegion[1];
        textureBarricade[0] = tileMap[0][1];
        
        //Texture contenant un type de terrain pour la carte par case
        texturePatternTour = new TextureRegion[game.getFic("PatternTour").getNbLigne()];
        for(int j = 0 ; j < Constantes.getInt("NB_IMG_PATTERN_TOUR_ROWS") ; j++){
            for(int i = 0 ; i < Constantes.getInt("NB_IMG_PATTERN_TOUR_COLS") ; i++){
            	if(i + (Constantes.getInt("NB_IMG_PATTERN_TOUR_COLS") * j) < game.getFic("PatternTour").getNbLigne()){
            		texturePatternTour[i + (Constantes.getInt("NB_IMG_PATTERN_TOUR_COLS") * j)] = tilePatternTour[j][i];
            	}
            }
        }
        
        //Texture contenant un type de terrain pour la carte par case
        texturePatternTir = new TextureRegion[game.getFic("PatternTir").getNbLigne()];
        for(int j = 0 ; j < Constantes.getInt("NB_IMG_PATTERN_TIR_ROWS") ; j++){
            for(int i = 0 ; i < Constantes.getInt("NB_IMG_PATTERN_TIR_COLS") ; i++){
            	if(i + (Constantes.getInt("NB_IMG_PATTERN_TIR_COLS") * j) < game.getFic("PatternTir").getNbLigne()){
            		texturePatternTir[i + (Constantes.getInt("NB_IMG_PATTERN_TIR_COLS") * j)] = tilePatternTir[j][i];
            	}
            }
        }
        
        /**
         * Cr�ation des animations des tours
         */
        listTextureTour = new ArrayList<TextureRegion[]>();
        listAnimationTour = new ArrayList<Animation>();
        
        for (int i = 0; i < Constantes.getInt("NB_IMG_TOUR_ROWS"); i++) {
        	TextureRegion[] textureTourTemp = new TextureRegion[Constantes.getInt("NB_IMG_TOUR_COLS")];
        	for (int j = 0; j < Constantes.getInt("NB_IMG_TOUR_COLS") ; j++) {
            	
        		textureTourTemp[j] = tileTour[i][j];
	            
        	}
        	listTextureTour.add(textureTourTemp);
        }

        for(int i = 0 ; i < listTextureTour.size(); i++){
        	listAnimationTour.add(new Animation(0.4f, listTextureTour.get(i)));
        }
        /**
         * Fin de cr�ation des animations des tours
         */

        /**
         * Cr�ation des animations des tirs
         */
        listTextureTir = new ArrayList<TextureRegion[]>();
        listAnimationTir = new ArrayList<Animation>();
        
        for (int i = 0; i < Constantes.getInt("NB_IMG_TIR_ROWS"); i++) {
        	TextureRegion[] textureTirTemp = new TextureRegion[Constantes.getInt("NB_IMG_TIR_COLS")];
        	for (int j = 0; j < Constantes.getInt("NB_IMG_TIR_COLS") ; j++) {
            	
        		textureTirTemp[j] = tileTir[i][j];
	            
        	}
        	listTextureTir.add(textureTirTemp);
        }

        for(int i = 0 ; i < listTextureTir.size(); i++){
        	listAnimationTir.add(new Animation(0.4f, listTextureTir.get(i)));
        }
        /**
         * Fin de cr�ation des animations des tirs
         */
        
        /**
         * Cr�ation des animations des pieges
         */
        listTexturePiege = new ArrayList<TextureRegion[]>();
        listAnimationPiege = new ArrayList<Animation>();
        
        
        for (int i = 0; i < Constantes.getInt("NB_IMG_PIEGE_ROWS"); i++) {
        	TextureRegion[] texturePiegeTemp = new TextureRegion[game.getFic("Piege").getInfoInt(i, Constantes.getInt("FIC_PIEGE_NB_IMG"))];

        	for (int j = 0; j < game.getFic("Piege").getInfoInt(i, Constantes.getInt("FIC_PIEGE_NB_IMG")) ; j++) {
            	
        		texturePiegeTemp[j] = tilePiege[i][j];
	            
        	}
        	listTexturePiege.add(texturePiegeTemp);
        }

        for(int i = 0 ; i < listTexturePiege.size(); i++){
        	listAnimationPiege.add(new Animation(0.2f, listTexturePiege.get(i)));
        }
        /**
         * Fin de cr�ation des animations des pieges
         */
        
        
        /**
         * Cr�ation des animations des monstres
         */
        listDirectTexture = new ArrayList<HashMap<Direction, TextureRegion[]>>();
        
        for (int i = 0; i < Constantes.getInt("NB_IMG_MOB_ROWS"); i++) {
        	HashMap<Direction, TextureRegion[]> listDirectTextureTemp = new HashMap<Direction, TextureRegion[]>();
        	for (int k = 0; k < 4 ; k++) {
            	TextureRegion[] textureTemp = new TextureRegion[4];
	            for (int j = 0; j < 4; j++) {
	            	
	            	textureTemp[j] = tileMonstre[i][j + (k * 4)];
	            }
	            switch(k){
            	case 0:
            		listDirectTextureTemp.put(Direction.Bas, textureTemp);
            		break;
            	case 1:
            		listDirectTextureTemp.put(Direction.Droite, textureTemp);
            		break;
            	case 2:
            		listDirectTextureTemp.put(Direction.Gauche, textureTemp);
            		break;
            	case 3:
            		listDirectTextureTemp.put(Direction.Haut, textureTemp);
            		break;
            	}
	            
        	}
        	listDirectTexture.add(listDirectTextureTemp);
        }
        
        /**
		* Cr�ation des animations en fonction du cot� de l'image : Gauche - Droite ; Haut - Bas
		*/
        listAnimationBas = new ArrayList<Animation>();
        listAnimationDroite = new ArrayList<Animation>();
        listAnimationGauche = new ArrayList<Animation>();
        listAnimationHaut = new ArrayList<Animation>();
        
        for(int i = 0 ; i < listDirectTexture.size(); i++){
        	listAnimationBas.add(new Animation(0.3f, listDirectTexture.get(i).get(Direction.Bas)));
        }
        for(int i = 0 ; i < listDirectTexture.size(); i++){
        	listAnimationDroite.add(new Animation(0.3f, listDirectTexture.get(i).get(Direction.Droite)));
        }
        for(int i = 0 ; i < listDirectTexture.size(); i++){
        	listAnimationGauche.add(new Animation(0.3f, listDirectTexture.get(i).get(Direction.Gauche)));
        }
        for(int i = 0 ; i < listDirectTexture.size(); i++){
        	listAnimationHaut.add(new Animation(0.3f, listDirectTexture.get(i).get(Direction.Haut)));
        }
        
        listDirectAnimation = new HashMap<Direction, ArrayList<Animation>>();
        listDirectAnimation.put(Direction.Bas, listAnimationBas);
        listDirectAnimation.put(Direction.Droite, listAnimationDroite);
        listDirectAnimation.put(Direction.Gauche, listAnimationGauche);
        listDirectAnimation.put(Direction.Haut, listAnimationHaut);
        /**
         * Fin de cr�ation des animations des monstres
         */
		
	}
	
	public void setControleur(AControlGame control){
		this.control = control;
	}
	
	public void dispose(){
        tilesetMap.dispose();
        tilesetMonstre.dispose();
        tilesetTour.dispose();
        tilesetTir.dispose();
        tilesetPiege.dispose();
        tilesetPatternTour.dispose();
        tilesetPatternTir.dispose();
        tileFondObjMag.dispose();
        tileQuitter.dispose();
        gameOver.dispose();
        gameWin.dispose();
        tileMagasinFond.dispose();
        tileMagasinChoixAmelioration.dispose();
        font.dispose();
        
		disposeGame();
	}
	
	public abstract void afficher();
	public abstract void disposeGame();
	public abstract void gameOver();
	public abstract void gameWin();
}
























