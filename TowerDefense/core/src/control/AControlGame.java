package control;


/**
 * 
 * @author Florian
 *
 */
public abstract class AControlGame extends AControleur {

	protected PageMagasin pageActu;
	protected boolean retourMenu;
	protected boolean afficheQuitter;
	
	public AControlGame(){
		
		
		pageActu = PageMagasin.MagTourPiege;
	
		retourMenu = false;
		afficheQuitter = false;
		
		creationBtn("btnQuitterOui", 258, 315, 74, 40);
		creationBtn("btnQuitterNon", 364, 315, 74, 40);

	}
	
	/**
	 * Gestion des touches lors de la fin du jeu
	 */
	public abstract void gestionTouche();
	
	public void setRetourMenu(boolean newVal){
		retourMenu = newVal;
	}
	
	public boolean getRetourMenu(){
		return retourMenu;
	}
	
	public boolean getAfficheQuitter(){
		return afficheQuitter;
	}
	
	public PageMagasin getPage(){
		return pageActu;
	}
}
