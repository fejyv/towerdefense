package control;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;

import jeu.JeuMulti;
import model.batiment.NomTypeBatiment;
import model.caseMod.ACase;
import stats.Stats;

/**
 * 
 * @author Florian
 *
 */
public class ControlGameMulti  extends AControlGame {

	protected JeuMulti game;
	
	public ControlGameMulti(JeuMulti game) {

		this.game = game;

		creationTabBtn("tabBtnTour", game.getNbTour(), 211, 36, 36);
		creationTabBtn("tabBtnPiege", game.getNbPiege(), 449, 36, 36);

		creationTabBtn("tabPatternTour", game.getFic("PatternTour").getNbLigne(), 211, 36, 36);
		creationTabBtn("tabPatternTir", game.getFic("PatternTir").getNbLigne(), 449, 36, 36);

		creationTabBtn("tabBtnMonstre", game.getFic("Monstre").getNbLigne(), 168, 36, 36);

		creationTabBtn("tabBtnMobVague", 0, 168, 36, 36);
		
		
		creationBtn("btnAchat_Ajout_Envoi", 814, 570, 83, 53);
		creationBtn("btnBarricade", 659, 139, 36, 36);
		creationBtn("btnMagasin", 640, 0, 288, 640);
		
		creationBtn("btnOngletTourPiege", 646, 80, 92, 40);
		creationBtn("btnOngletMonstre", 738, 80, 92, 40);
		creationBtn("btnOngletVague", 830, 80, 92, 40);

		// Lancement du chronom�tre de la classe stats
		Stats.startChrono();
	}

	/**
	 * Gestion des touches simples: - Gauche : pose une tour ou choisi un
	 * Batiment - Droite : supprime une tour
	 */
	public void gestionTouche() {

		Gdx.input.setInputProcessor(new InputAdapter() {// actif lors d'un
														// clique
			public boolean touchDown(int x, int y, int pointer, int button) {

				if (button == Input.Buttons.RIGHT) {
					
					if(!afficheQuitter){
						game.setTypeObjActu(NomTypeBatiment.None);
						game.setTourEnModif(-1);
	
						if (!tabBtn.get("btnMagasin").contains(Gdx.input.getX(), Gdx.input.getY())) {
							ACase caseTemp = game.getCarte().trouverCasePixel(Gdx.input.getX(), Gdx.input.getY());
							game.suppressionBatiment(caseTemp);
						}
					}
				} else if (button == Input.Buttons.LEFT) {
					if(!afficheQuitter){
						// V�rifie si l'on clique dans le magasin ou pas
						if (tabBtn.get("btnMagasin").contains(Gdx.input.getX(), Gdx.input.getY())) {
							
							if(tabBtn.get("btnOngletTourPiege").contains(Gdx.input.getX(), Gdx.input.getY())){
								pageActu = PageMagasin.MagTourPiege;
							}
							else if(tabBtn.get("btnOngletMonstre").contains(Gdx.input.getX(), Gdx.input.getY())){
								game.setTypeObjActu(NomTypeBatiment.None);
								game.setTourEnModif(-1);
								
								pageActu = PageMagasin.MagChoixMonstre;
							}
							else if(tabBtn.get("btnOngletVague").contains(Gdx.input.getX(), Gdx.input.getY())){
								game.setTypeObjActu(NomTypeBatiment.None);
								game.setTourEnModif(-1);
								
								pageActu = PageMagasin.MagVague;
							}						
							
							
							switch(pageActu){
							case MagTourPiege:
								// Si aucune tour n'est en cours de modification
								if (game.getTourEnModif() == -1) {
									if (tabBtn.get("btnBarricade").contains(Gdx.input.getX(), Gdx.input.getY())) {
										game.setTypeObjActu(NomTypeBatiment.Barricade);
										game.setTourEnModif(-1);
									} else {
										int num = trouverIndexTabBtn("tabBtnTour", Gdx.input.getX(), Gdx.input.getY());
		
										// Si un des boutons de tour est cliqu�
										if (num != -1) {
											game.setTypeObjActu(NomTypeBatiment.Tour);
											game.setObjActu(num);
											game.setTourEnModif(-1);
										} else {
											num = trouverIndexTabBtn("tabBtnPiege", Gdx.input.getX(), Gdx.input.getY());
		
											// Si un des boutons de pi�ge est cliqu�
											if (num != -1) {
												game.setTypeObjActu(NomTypeBatiment.Piege);
												game.setObjActu(num);
												game.setTourEnModif(-1);
											} else {
												// Sinon ne rien faire
												game.setTypeObjActu(NomTypeBatiment.None);
											}
										}
									}
								} else {
									// Si on valide une am�lioration
									if (tabBtn.get("btnAchat_Ajout_Envoi").contains(Gdx.input.getX(), Gdx.input.getY())) {
										// On v�rifie si une am�lioration avait �t�
										// choisie
										if (game.getAmelioration() != -1) {
		
											game.lancerAmelioration();
		
											// R�initialisation du choix d'am�lioration
											game.setAmelioration(-1, -1);
										}
									} else {
										int num = trouverIndexTabBtn("tabPatternTour", Gdx.input.getX(), Gdx.input.getY());
		
										// Si un des boutons de pattern tour est cliqu�
										if (num != -1) {
											game.setAmelioration(0, num);
										} else {
											num = trouverIndexTabBtn("tabPatternTir", Gdx.input.getX(), Gdx.input.getY());
											// Si un des boutons de pattern tir est
											// cliqu�
											if (num != -1) {
												game.setAmelioration(1, num);
											}
										}
									}
								}
								break;
							case MagChoixMonstre:
								if (tabBtn.get("btnAchat_Ajout_Envoi").contains(Gdx.input.getX(), Gdx.input.getY())) {
									if(game.getMobPourVague() != -1 && game.getVagueEnvoi().get().size() < 24){
										game.addMobVagueEnvoi();
										addBtnToListe("tabBtnMobVague", 168, 36, 36);
									}
								}
								else{
									int num = trouverIndexTabBtn("tabBtnMonstre", Gdx.input.getX(), Gdx.input.getY());
									
									// Si un des boutons de monstre est cliqu�
									if (num != -1) {
										game.setMobPourVague(num);
									}
									else{
										game.setMobPourVague(-1);
									}
								}
								break;
							case MagVague:
								if (tabBtn.get("btnAchat_Ajout_Envoi").contains(Gdx.input.getX(), Gdx.input.getY())) {
									game.envoiVague(true);
								}
								break;
							}
						}
						// Sinon on est sur la carte
						else {
							ACase caseTemp = game.getCarte().trouverCasePixel(Gdx.input.getX(), Gdx.input.getY());
	
							// Si une case a �t� cliqu�
							if (caseTemp != null) {
								// Si aucun batiment n'a �t� cr��
								if (!game.fabriqueBatiment(caseTemp)) {
									int tourTrouve = game.verifCaseBatiment(caseTemp, NomTypeBatiment.Tour, false);
									if(tourTrouve != -1){
										game.setTourEnModif(tourTrouve);
										pageActu = PageMagasin.MagTourPiege;
									}
								} else {
									game.setTourEnModif(-1);
								}
							}
						}
					}
					else{
						if(tabBtn.get("btnQuitterOui").contains(Gdx.input.getX(), Gdx.input.getY())){
							retourMenu = true;
						}
						else{
							afficheQuitter = false;
						}
					}

				}
				return true;
			}

			public boolean keyDown(int keycode) {
				if (keycode == Input.Keys.ESCAPE) {
					afficheQuitter = !afficheQuitter;
				}
				else if (Gdx.input.isKeyPressed(Input.Keys.F1) && Gdx.input.isKeyPressed(Input.Keys.CONTROL_RIGHT)
						&& Gdx.input.isKeyPressed(Input.Keys.F12)) {
					game.joueurGagneArgent(2500);
				}
				// Code d'argent infini
				else if (Gdx.input.isKeyPressed(Input.Keys.F1) && Gdx.input.isKeyPressed(Input.Keys.CONTROL_RIGHT)
						&& Gdx.input.isKeyPressed(Input.Keys.F12)) {
					game.joueurGagneArgent(25);
				}

				return true;
			}

		});
	}
};








