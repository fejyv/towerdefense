package control;

public enum TypePartie {
	Solo,
	MultiClient,
	MultiServeur,
	Autre;
}
