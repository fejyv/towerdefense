package control;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;

import outil.libSong.LibSong;

/**
 * 
 * @author Florian
 *
 */
public class ControlMenu extends AControleur {

	
	private TypePartie typeGameChoisi;
	private boolean affichageCredit;
	private boolean affichageMulti;
	private boolean enEcriture;
	private String ipAJoindre;
	
	public ControlMenu() {

		typeGameChoisi = TypePartie.Autre;
		affichageCredit = false;
		affichageMulti = false;
		enEcriture = false;
		ipAJoindre = "";
		
		creationBtn("btnPartieSolo", 317, 117, 280, 79);
		creationBtn("btnPartieMulti_Creer", 317, 232, 280, 79);
		creationBtn("btnCredit_Rejoindre", 317, 347, 280, 79);
		creationBtn("btnIpDestination", 622, 347, 280, 79);
		creationBtn("btnQuitter", 317, 462, 280, 79);
		
	}
	
	/**
	 * Gestion des touches lors de la fin du jeu
	 */
	public void gestionTouche(){
		
        Gdx.input.setInputProcessor(new 
               InputAdapter () {
               public boolean touchDown (int x, int y, int pointer, int button) {
            	  
               if ( button == Input.Buttons.LEFT){
            	   
            	   
            	   if(affichageCredit){
            		   if(tabBtn.get("btnQuitter").contains(Gdx.input.getX(), Gdx.input.getY())){
            			   affichageCredit = false;
	            	   }
            	   }
            	   else if(affichageMulti){
            		   
            		   if(tabBtn.get("btnIpDestination").contains(Gdx.input.getX(), Gdx.input.getY())){
            			   enEcriture = true;
	            	   }
            		   else {
            			   enEcriture = false;
            		   }
            		   
            		   if(tabBtn.get("btnQuitter").contains(Gdx.input.getX(), Gdx.input.getY())){
            			   affichageMulti = false;
	            	   }
            		   else if(tabBtn.get("btnPartieMulti_Creer").contains(Gdx.input.getX(), Gdx.input.getY())){
            			   typeGameChoisi = TypePartie.MultiServeur;
	            	   }
            		   else if(tabBtn.get("btnCredit_Rejoindre").contains(Gdx.input.getX(), Gdx.input.getY())){
            			   typeGameChoisi = TypePartie.MultiClient;
	            	   }
            	   }
            	   else{
	            	   if(tabBtn.get("btnPartieSolo").contains(Gdx.input.getX(), Gdx.input.getY())){
	            		   typeGameChoisi = TypePartie.Solo;
	            	   }
	            	   else if(tabBtn.get("btnPartieMulti_Creer").contains(Gdx.input.getX(), Gdx.input.getY())){
	            		   affichageMulti = true;
	            	   }
	            	   else if(tabBtn.get("btnCredit_Rejoindre").contains(Gdx.input.getX(), Gdx.input.getY())){
	            		   affichageCredit = true;
	            	   }
	            	   else if(tabBtn.get("btnQuitter").contains(Gdx.input.getX(), Gdx.input.getY())){
	            		   Gdx.app.exit();
	            	   }
            	   }
            	   
            	   LibSong.playSound("Son Pose");
                }
               return true;
             }
               
           public boolean keyDown(int keycode){
        	   if (keycode == Input.Keys.ESCAPE){
        		   if(affichageCredit){
        			   affichageCredit = false;
        		   }
        		   else if(affichageMulti){
        			   affichageMulti = false;
        		   }
        		   else{
        			   Gdx.app.exit();
        		   }
        	   }
        	   
        	   if(enEcriture){
        		   if(ipAJoindre.length() < 15 && ((keycode >= Input.Keys.NUM_0 && keycode <= Input.Keys.NUM_9) || (keycode >= Input.Keys.NUMPAD_0 && keycode <= Input.Keys.NUMPAD_9) || keycode == 56)){
        			   if(keycode >= Input.Keys.NUMPAD_0 && keycode <= Input.Keys.NUMPAD_9){
        				   keycode -= 137;
        			   }
        			   ipAJoindre += Input.Keys.toString(keycode);
        		   }
        		   else if(keycode == Input.Keys.BACKSPACE && ipAJoindre.length() > 0){
        			   ipAJoindre = ipAJoindre.substring(0, ipAJoindre.length() - 1);
        		   }
        		   else if(keycode == Input.Keys.ENTER){
        			   typeGameChoisi = TypePartie.MultiClient;
        		   }
        	   }
        	   
        	   
        	   return true;
           }
               
        });
        
        
		
	}

	public TypePartie getTypeGameChoisi(){
		return typeGameChoisi;
	}
	
	public boolean getAffichageCredit(){
		return affichageCredit;
	}

	public boolean getAffichageMulti(){
		return affichageMulti;
	}
	
	public String getIpConnexion(){
		return ipAJoindre;
	}
}
