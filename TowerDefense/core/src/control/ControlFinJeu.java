package control;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;

import jeu.AJeu;
import stats.Stats;

/**
 * 
 * @author Florian
 *
 */
public class ControlFinJeu extends AControlGame{
	
	private AJeu game;
	
	public ControlFinJeu(AJeu game){
		this.game = game;
			
		creationBtn("btnOui", 492, 454, 88, 54);
		creationBtn("btnNon", 620, 454, 88, 54);
		
		retourMenu = false;
		
		// Fin des stats pour affichage
		// Note : Les stats s'affichent quand le jeu est fini
		Stats.endOfGame();
	}
	
	/**
	 * Gestion des touches lors de la fin du jeu
	 */
	public void gestionTouche(){
		
        Gdx.input.setInputProcessor(new 
               InputAdapter () {
               public boolean touchDown (int x, int y, int pointer, int button) {
            	  
               if ( button == Input.Buttons.LEFT){
            	   
            	   if(tabBtn.get("btnOui").contains(Gdx.input.getX(), Gdx.input.getY())){
            		   game.setNouvellePartie(true);
            	   }
            	   else if(tabBtn.get("btnNon").contains(Gdx.input.getX(), Gdx.input.getY())){
            		   retourMenu = true;
            	   }
                }
               return true;
             }
               
           public boolean keyDown(int keycode){
        	   if (keycode == Input.Keys.ESCAPE){
        		   retourMenu = true;
        	   }
        	   
        	   return true;
           }
               
        });
        
        
		
	}
	
};