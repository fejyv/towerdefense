package control;

import java.util.ArrayList;
import java.util.HashMap;

import com.badlogic.gdx.math.Rectangle;

public abstract class AControleur {
	
	
	protected HashMap<String, ArrayList<Rectangle>> tabListBtn;
	protected HashMap<String, Rectangle> tabBtn;
	
	public AControleur(){
		tabListBtn = new HashMap<String, ArrayList<Rectangle>>();
		tabBtn = new HashMap<String, Rectangle>();
	}
	
	public abstract void gestionTouche();
	
	/**
	* Cr�er un tableau de bouton en fonction d'un nombre de bouton donn�e et de la position de d�part de ceux-ci
	* Le nombre de bouton en largeur ainsi que le nombre de pixel s�parant chacun est not� dans le code
	* @param tabBtn
	* @param nbMax
	* @param posYDeb
	*/
	protected boolean creationTabBtn(String nomTabBtn, int nbMax, int posYDeb, int sizeX, int sizeY){
		
		if(!tabListBtn.containsKey(nomTabBtn)){
			ArrayList<Rectangle> tabBtnTemp = new ArrayList<Rectangle>();
			int cpt = 0, cptY = 0, cptX = 0;
			boolean fini = false;
			
			while(!fini && cpt < nbMax){
				cptX = 0;
				
				while(!fini && cptX < 4 && cpt < nbMax){
					
					tabBtnTemp.add(new Rectangle(659 + (71 * cptX), posYDeb + (72 * cptY), sizeX, sizeY));
					
					if(cpt >= nbMax){
						fini = true;
					}
					
					cpt++;
					cptX++;
				}
				cptY++;
			}
			tabListBtn.put(nomTabBtn, tabBtnTemp);
			
			return true;
		}
		
		return false;
	}
	
	protected void addBtnToListe(String nomTabBtn, int posYDeb, int sizeX, int sizeY){
		if(tabListBtn.containsKey(nomTabBtn)){
			int nbElem = tabListBtn.get(nomTabBtn).size();
			
			int nbLigne = (int) Math.floor(((double)nbElem / 4));
			
			int numColonneActu = nbElem%4;
						
			tabListBtn.get(nomTabBtn).add(new Rectangle(659 + (71 * numColonneActu), posYDeb + (72 * nbLigne), sizeX, sizeY));
		}
	}
	
	protected boolean creationBtn(String nomBtn, int posX, int posY, int sizeX, int sizeY){
		
		if(!tabBtn.containsKey(nomBtn)){
			
			tabBtn.put(nomBtn, new Rectangle(posX, posY, sizeX, sizeY));
			
			return true;
		}
		
		return false;
	}
	
	/**
	* Cherche dans un tableau de bouton un bouton qui se trouverai sous les coordonn�es pass� en param�tre
	* @param tabBtn
	* @param posX
	* @param posY
	*/
	protected int trouverIndexTabBtn(String nomTabBtn, int posX, int posY){
		
		int valRetour = -1;
		
		if(tabListBtn.containsKey(nomTabBtn)){
			int cpt = 0;
			
			while(valRetour == -1 && cpt < tabListBtn.get(nomTabBtn).size()){
				if(tabListBtn.get(nomTabBtn).get(cpt).contains(posX, posY)){
					valRetour = cpt;
				}
				cpt++;
			}
		}
		
		return valRetour;
	}
	
	public ArrayList<Rectangle> getTabBtn(String nomTabBtn){
		if(tabListBtn.containsKey(nomTabBtn)){
			return tabListBtn.get(nomTabBtn);
		}
		return null;
	}
	
	public Rectangle getBtn(String nomBtn){
		if(tabBtn.containsKey(nomBtn)){
			return tabBtn.get(nomBtn);
		}
		return null;
	}
}









