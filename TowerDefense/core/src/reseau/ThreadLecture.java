package reseau;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.net.SocketException;

import jeu.JeuMulti;
import model.monstre.Vague;


public class ThreadLecture implements Runnable {
	private Socket socket;
	private ObjectInputStream in;
	private boolean estConnecte = true;
	private JeuMulti game;
	
	public ThreadLecture(final Socket socket, final JeuMulti game) {
		this.socket = socket;
		this.game = game;
		
		try {
			this.in = new ObjectInputStream(this.socket.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		
		while(this.estConnecte) {
			Object objRecup;
			 
			try {

				objRecup = (Object) in.readObject();
				
				if(objRecup != null) {
					//System.out.println("Obj r�cup�r�");
					if(objRecup instanceof Vague){
						
						game.addVagueListe((Vague) objRecup);
					}
					else if(objRecup instanceof String){
						
						//System.out.println(((String)objRecup));
						
						
						if(((String)objRecup).equals("end game")){
							this.estConnecte = false;
							this.game.setPartieFini(true);
							this.game.setJouerPossible(false);
						}
						else if(((String)objRecup).equals("Victoire")){
							this.game.setPartieFini(true);
						}
						else if(((String)objRecup).equals("NewGame")){
							this.game.setJouerPossible(true);
						}
					}
					else{
						System.out.println("Je ne sais pas ce que c'est");
					}
				}
				
			} catch (ClassNotFoundException e) {
				System.out.println(e);
			} catch(SocketException e){
				System.out.println(e);
			} catch(EOFException e){
				e.printStackTrace();
			} catch (IOException e) {
				System.out.println(e);
			} 
			
		 }		
	}
	
	public void dispose() {
		try {
			this.in.close();
			this.estConnecte = false;
			System.out.println("Lecture ferm�e");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	
}
