package reseau;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import jeu.JeuMulti;



public class ReseauClient extends AReseau{

	private int port = 25000;
	private String address;
	private Socket socket;
	
	public ReseauClient(final String address) {
		this.address = address;
	}
	
	public ReseauClient(final String address, final int port) {
		this.address = address;
		this.port = port;
	}
	
	public void init(final JeuMulti game) {
		try {
			
			this.socket = new Socket();
			socket.connect(new InetSocketAddress(this.address, port), 10000);
		} catch(SocketTimeoutException e){
			System.out.println(e);
			coTrouver = false;
		} catch(ConnectException e){
			System.out.println(e);
			coTrouver = false;
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		
		if(coTrouver){
			try {
				System.out.println("Client connect� � " + socket.getInetAddress().getHostName());
				this.gestionEnvoie = new GestionEnvoie(new ObjectOutputStream(socket.getOutputStream()));
				this.lecture = new ThreadLecture(socket, game);
				new Thread(this.lecture).start();
			} catch (IOException e) {
				e.printStackTrace();
			} 
		}
	}
		
	public void fermetureSocket() throws IOException {
		this.socket.close();
		System.out.println("Client ferm�");
	}
	
}
