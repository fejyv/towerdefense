package reseau;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.SocketException;


public class GestionEnvoie {
	private ObjectOutputStream out;
	
	public GestionEnvoie(final ObjectOutputStream out) {
		this.out = out;
	}

	public void envoyerObjet(final Object objet) {
		try {
			
			// On envoie l'objet et on nettoie le flux sortant

			out.writeObject(objet);
			out.flush();
		} catch(SocketException e){
			System.out.println(e);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void dispose() {
		try {
			this.out.close();
			System.out.println("GestionEnvoie ferm�e");
		} catch (SocketException e){
			System.out.println(e);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}
