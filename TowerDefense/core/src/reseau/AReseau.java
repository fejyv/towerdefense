package reseau;

import java.io.IOException;

import jeu.JeuMulti;

public abstract class AReseau {

	protected GestionEnvoie gestionEnvoie;
	protected ThreadLecture lecture;
	protected JeuMulti game;
	
	protected boolean coTrouver;
	
	{
		coTrouver = true;
	}
	
	public abstract void init(JeuMulti game);
	
	public abstract void fermetureSocket() throws IOException;
	
	public void envoyerObjet(Object objet) {
		this.gestionEnvoie.envoyerObjet(objet);
	}
	
	public boolean getCoTrouver(){
		return coTrouver;
	}

	public void dispose() {
		try {
			this.envoyerObjet("end game");
			this.lecture.dispose();
			this.gestionEnvoie.dispose();
			this.fermetureSocket();
		} catch (IOException e) {
			System.out.println(e);
		}
	}
}
