package reseau;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

import jeu.JeuMulti;


public class ReseauServeur extends AReseau {

	private ServerSocket serveurSocket;
	
	public ReseauServeur() {
		
	}
	
	public void init(final JeuMulti game){
		Socket socket = null;
		
		try {
			this.serveurSocket = new ServerSocket(25000);
			System.out.println("Serveur ouvert sur le port : " + this.serveurSocket.getLocalPort());	
			//timeout after 10 seconds
			serveurSocket.setSoTimeout(10000);
			socket = serveurSocket.accept();
		} catch(SocketTimeoutException e){
			System.out.println(e);
			coTrouver = false;
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if(coTrouver){
			try {
				System.out.println(socket.getLocalAddress().getHostName() + " est connect�");
				this.lecture = new ThreadLecture(socket, game);
				new Thread(this.lecture).start();
				this.gestionEnvoie = new GestionEnvoie(new ObjectOutputStream(socket.getOutputStream()));
			} catch (IOException e) {
				e.printStackTrace();
			} 
		}
		else{
			try {
				fermetureSocket();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void fermetureSocket() throws IOException {
		this.serveurSocket.close();
		System.out.println("Serveur ferm�");
	}

	
}
