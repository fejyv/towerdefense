package jeu;

import java.util.ArrayList;
import java.util.HashMap;

import com.badlogic.gdx.math.Vector2;

import amelioration.AmeliorationPatternTour;
import amelioration.AmeliorationTypeTir;
import model.Carte;
import model.batiment.ABatiment;
import model.batiment.Barricade;
import model.batiment.NomTypeBatiment;
import model.batiment.piege.APiege;
import model.batiment.piege.EffetPiegeDegat;
import model.batiment.piege.PiegeStandard;
import model.batiment.tour.ATour;
import model.batiment.tour.TourStandard;
import model.batiment.tour.pattern.APatternTour;
import model.batiment.tour.pattern.TypePatternTour;
import model.caseMod.ACase;
import model.gestionPosition.GestionPositionBatiment;
import model.gestionPosition.GestionPositionMonstreStandard;
import model.gestionPosition.GestionPositionTirStandard;
import model.joueur.JoueurReel;
import model.monstre.AMonstre;
import model.monstre.AMonstreCarte;
import model.monstre.MonstreBasique;
import model.monstre.Vague;
import model.tir.ATir;
import model.tir.TirStandard;
import model.tir.pattern.APatternTir;
import model.tir.pattern.TypePatternTir;
import outil.typeFichier.AFichier;
import outil.typeFichier.FichierInfo;
import outil.typeFichier.FichierVague;
import stats.Stats;
import typeElement.IAttaquant;
import typeElement.IAvecVie;
import typeElement.IDeplacable;
import outil.ClassName;
import outil.Clock;
import outil.Constantes;
import outil.Dijkstra;
import outil.FactoryInstanceByNom;
import outil.libSong.LibSong;

/**
 * 
 * @author Florian
 *
 */
public abstract class AJeu {
	
	protected Carte m_carte;
	protected JoueurReel m_joueur;
	protected int objChoisiActu;
	protected NomTypeBatiment typeObjChoisi;
	protected int tourEnModif;
	protected int typeAmeliorationChoisi;
	protected int numAmeliorationChoisi;
	
	protected Clock clockOrTime;
	protected Clock clockCreatMob;
	
	protected boolean partieFini;
	protected int tauxVente;
	
	protected HashMap<String, AFichier> tabFic;
	
	protected ArrayList<IAttaquant> tabAttaquant;
	protected ArrayList<IDeplacable> tabDeplacable;
	
	protected Vague vagueEnnemi;
	
	protected ArrayList<ABatiment> m_tabBatiment;
	protected ArrayList<IAvecVie> tabVivant;

	protected boolean jeuPossible;
	protected boolean nouvellePartie;
	
	
	public AJeu(){
		
		this.tabFic = new HashMap<String, AFichier>();
		
		this.tabFic.put("Monstre", new FichierInfo("../core/assets/properties/infoMonstre.propData", "#Chargement info Monstre", true));
		this.tabFic.put("Carte", new FichierInfo("../core/assets/properties/mapNiv1_1.propData", "#Chargement info Carte", false));
		this.tabFic.put("CarteNiv2", new FichierInfo("../core/assets/properties/mapNiv2_1.propData", "#Chargement info Carte", false));
		this.tabFic.put("Tile", new FichierInfo("../core/assets/properties/infoTile.propData", "#Chargement info Tile", false));
		this.tabFic.put("PatternTour", new FichierInfo("../core/assets/properties/infoPatternTour.propData", "#Chargement info PatternTour", false));
		this.tabFic.put("PatternTir", new FichierInfo("../core/assets/properties/infoPatternTir.propData", "#Chargement info PatternTir", false));
		this.tabFic.put("Tour", new FichierInfo("../core/assets/properties/infoTour.propData", "#Chargement info Tour", false));
		this.tabFic.put("EffetPiege", new FichierInfo("../core/assets/properties/infoEffetPiege.propData", "#Chargement info EffetPiege", false));
		this.tabFic.put("Piege", new FichierInfo("../core/assets/properties/infoPiege.propData", "#Chargement info Piege", false));
		this.tabFic.put("TileCaseVoisine", new FichierInfo("../core/assets/properties/tileCaseVoisine.propData", "#Chargement info tileCaseVoisine", false));
		this.tabFic.put("MapCaseVoisine", new FichierInfo("../core/assets/properties/mapCaseVoisine.propData", "#Chargement info mapCaseVoisine", false));
		this.tabFic.put("Vague", new FichierVague("../core/assets/properties/vague.propData", "#Chargement info vague"));

		this.tauxVente = Constantes.getInt("TAUX_VENTE");
				
		newPartie();
		
		jeuPossible = true;
		nouvellePartie = false;
	}
	
	public abstract void newPartiePlus();
	
	public void newPartie(){
		this.m_carte = new Carte(tabFic);
		this.m_joueur = new JoueurReel(Constantes.getInt("JOUEUR_VIE_DEPART"), Constantes.getInt("JOUEUR_ARGENT_DEPART"));
		this.objChoisiActu = 0;
		this.typeObjChoisi = NomTypeBatiment.None;
		this.tourEnModif = -1;
		this.typeAmeliorationChoisi = -1;
		this.numAmeliorationChoisi = -1;
		
		this.clockCreatMob = new Clock();
		
		this.partieFini = false;
		
		this.clockOrTime = new Clock();
		
		this.tabAttaquant = new ArrayList<IAttaquant>();
		this.tabDeplacable = new ArrayList<IDeplacable>();
		this.vagueEnnemi = new Vague();
		this.m_tabBatiment = new ArrayList<ABatiment>();
		this.tabVivant = new ArrayList<IAvecVie>();
		
		newPartiePlus();
	}
	
	/**
	* Permet au joueur de gagner x or toutes les x secondes
	*/
	public void gainOrContinu(){
		if(clockOrTime.getElapsedTimeSecond() > Constantes.getInt("TIME_OR_CONTINU")){
			m_joueur.gagnerArgent(Constantes.getInt("GAIN_OR_CONTINU"));
			Stats.ajoutOrGagne(Constantes.getInt("GAIN_OR_CONTINU"));
			clockOrTime.restart();
		}
	}
	
	/**
	 * inflige les degats des monstres presents sur la derniere case au joueur
	 */
	public void gererDegatsJoueur()
	{
		int degats = 0;
		for(AMonstreCarte unMonstre : m_carte.getCaseFin().getEnnemi())
		{
			degats += unMonstre.getDegats();
			tabDeplacable.remove(unMonstre);
			vagueEnnemi.del(unMonstre);
		}
		tabVivant.removeAll(m_carte.getCaseFin().getEnnemi());
		m_carte.getCaseFin().getEnnemi().clear();
		
		if(degats > 0)
		{
			m_joueur.perdreDesPointsDeVie(degats);
			//m_joueur.afficherVie();
		}
	}
	
	public boolean joueurEstMort()
	{
		return m_joueur.estMort();
	}
	
	public Carte getCarte(){
		return m_carte;
	}
	
	public void bouger()
	{
		for(IDeplacable obj : tabDeplacable){
			obj.deplacer();
		}
	}

	public void actionsBatiments() 
	{
		for(IAttaquant obj : tabAttaquant){
			tabDeplacable.addAll(obj.attaquer());
		}
	}
	
	/**
	* Supprime les d�placables mort (monstres et tirs)
	* Augmente l'or et le score du joueur en fonction
	*/
	public void nettoyerMorts()
	{
		int argent = 0;
		int score = 0;
		
		ArrayList<IDeplacable> morts = new ArrayList<IDeplacable>();
		for(IDeplacable obj : tabDeplacable)
		{
			if(obj.estMort())
			{
				obj.disparaitre();
				
				if(obj instanceof AMonstre){

					argent += ((AMonstre)obj).getValeur();
					score += ((AMonstre)obj).getScore();
				}
				
				morts.add(obj);
			}
		}

		tabDeplacable.removeAll(morts);
		vagueEnnemi.delAll(morts);
		tabVivant.removeAll(morts);
		
		if(argent != 0)
		{
			m_joueur.gagnerArgent(argent);
			Stats.ajoutOrGagne(argent);
			//m_joueur.afficherArgent();
		}
		
		if(score != 0){
			m_joueur.gagnerScore(score);
		}
	}
	
	/**
	* V�rifie si il reste des monstres en vie dans la vague
	* @return
	*/
	public boolean monstreVivant(){
		if(vagueEnnemi.get().size() > 0){
			return true;
		}
		return false;
	}
	
	/**
	 * Cr�er un monstre � placer sur la carte
	 * @param numMonstre
	 * @return
	 */
	public AMonstreCarte creatMob(int numMonstre){
		if(numMonstre >= 0 && numMonstre < tabFic.get("Monstre").getNbLigne()){
			int vieMax = tabFic.get("Monstre").getInfoInt(numMonstre, Constantes.getInt("FIC_MOB_VIE"));
			int degats = tabFic.get("Monstre").getInfoInt(numMonstre, Constantes.getInt("FIC_MOB_DEGAT"));
			int valeur = tabFic.get("Monstre").getInfoInt(numMonstre, Constantes.getInt("FIC_MOB_ARGENT"));
			double vitesse = tabFic.get("Monstre").getInfoDouble(numMonstre, Constantes.getInt("FIC_MOB_VITESSE"));
			int score = tabFic.get("Monstre").getInfoInt(numMonstre, Constantes.getInt("FIC_MOB_SCORE"));
			int prix = tabFic.get("Monstre").getInfoInt(numMonstre, Constantes.getInt("FIC_MOB_PRIX"));
			
			AMonstreCarte mobTemp = new MonstreBasique(numMonstre, vieMax, degats, valeur, vitesse, score, new GestionPositionMonstreStandard(m_carte.getCaseDeb()),prix);

			mobTemp.getGestionPos().calculerChemin(this);
			return mobTemp;
		}
		return null;
	}
	

	/**
	 * Fonction v�rifiant :
	 *  	- Si le chemin de la case de d�part � la case d'arriv� est libre
	 * 		- S'il reste un chemin possible pour chaque monstre
	 * @return
	 */
	public Boolean isCheminBloquer(){
		return (!Dijkstra.calculDijkstra(this) || dijkstraMob());
	}
	
	/**
	* Boucle sur chaque monstre afin de v�rifier si ils peuvent toujours atteindre la case de fin
	* @return
	*/
	public Boolean dijkstraMob(){
		Boolean cheminBloquer = false;
		int cpt = 0;
		
		while(!cheminBloquer && cpt < vagueEnnemi.get().size()){
			if(!((AMonstreCarte) vagueEnnemi.get().get(cpt)).getGestionPos().calculerChemin(this)){
				cheminBloquer = true;
			}
			cpt++;
		}
		return cheminBloquer;
	}
	
	/**
	* Supprime le batiment plac� sur la case, on recalcule ensuite le meilleur chemin et le joueur perd une certaine somme d'argent
	* @param caseChoisi
	*/
	public void suppressionBatiment(ACase caseChoisi){
		int sommePerdu = 0;
		
		//Pour tester les tours en premi�res et ne pas toucher au barricade si une tour est dessus
		int num = verifCaseBatiment(caseChoisi, NomTypeBatiment.Tour, false);
		
		if(num == -1){
			num = verifCaseBatiment(caseChoisi, NomTypeBatiment.Tour, true);
		}
		
		if(num != -1){
			if((double)m_tabBatiment.get(num).getPrix() * ((double)tauxVente / 100) <= m_joueur.getArgent()){
				sommePerdu = m_tabBatiment.get(num).getPrix();
				
				if(m_tabBatiment.get(num) instanceof Barricade){
					m_carte.startCaseCooldown(caseChoisi);
				}
				else{
					tabAttaquant.remove(m_tabBatiment.get(num));
				}
				suppressionBatiment(num);
				
				LibSong.playSound("Son suppression Batiment");
			}
		}
		
		isCheminBloquer();
		
		m_joueur.perdreArgent((int)((double)sommePerdu * ((double)tauxVente / 100)));
	}

	/**
	 * Renvoi un entier correspondant � la place dans le tableau de batiment.
	 * Le type pass� en param�tre permet de choisir un type en particulier � tester ou � exclure.
	 * @param caseChoisi
	 * @param nomTypeBatiment
	 * @param different
	 * @return
	 */
	public int verifCaseBatiment(ACase caseChoisi, NomTypeBatiment nomTypeBatiment, boolean different){
		int vide = -1;
		int cpt = 0;
		
		while(cpt < m_tabBatiment.size() && vide == -1){
			if(m_tabBatiment.get(cpt).getCase() == caseChoisi && 
				((!different && m_tabBatiment.get(cpt).getTypeBatiment() == nomTypeBatiment) || 
				 (different && m_tabBatiment.get(cpt).getTypeBatiment() != nomTypeBatiment))){
				vide = cpt;
			}
			cpt++;
		}
		return vide;
	}
	
	public void ajoutBatiment(final ABatiment batiment){
		m_tabBatiment.add(batiment);
	}
		
	public void suppressionBatiment(final int numDel){
		m_tabBatiment.remove(numDel);
	}
	
	/**
	* Fabrique � batiment regroupant les diff�rents types de batiment possibles
	* @param caseChoisi
	* @return
	*/
	public boolean fabriqueBatiment(ACase caseChoisi){
		boolean batimentCreer = false;
		boolean batimentExist = true;
		if(verifCaseBatiment(caseChoisi, NomTypeBatiment.Piege, false) == -1 && m_carte.getCaseNiv2().get((int)caseChoisi.getPosition().y).get((int)caseChoisi.getPosition().x).getValeurPassage() != 999 && !m_carte.isCaseInCooldown(caseChoisi)){
			switch(typeObjChoisi){
	        case Barricade:
	        	if(!m_carte.haveEnnemi(caseChoisi) && verifCaseBatiment(caseChoisi, NomTypeBatiment.Barricade, false) == -1){
	        		if(ajouterBarricade(caseChoisi)){
	        		
		        		if(isCheminBloquer() || m_carte.getCaseDeb() == caseChoisi){
		        			suppressionBatiment(m_tabBatiment.size() - 1);
		     		   }
		        	   else {
		        			batimentCreer = true;
		        			m_joueur.perdreArgent(m_tabBatiment.get(m_tabBatiment.size() - 1).getPrix());
		        			Stats.ajoutOrDepense(m_tabBatiment.get(m_tabBatiment.size() - 1).getPrix());
		        	   }
	        		}
	        	}
	        	break;
	        case Tour:
	        	if(!m_carte.haveEnnemi(caseChoisi)){
	        		if(verifCaseBatiment(caseChoisi, NomTypeBatiment.Tour, false) == -1){
	        			boolean poserTour = true;
	        			
		        		if(verifCaseBatiment(caseChoisi, NomTypeBatiment.Barricade, false) == -1){
		        			if(ajouterBarricade(caseChoisi)){
		        			
			        			if(isCheminBloquer() || m_carte.getCaseDeb() == caseChoisi){
				        			suppressionBatiment(m_tabBatiment.size() - 1);
				        			poserTour = false;
				     		   }
			        			else{
				        			m_joueur.perdreArgent(m_tabBatiment.get(m_tabBatiment.size() - 1).getPrix());
				        			Stats.ajoutOrDepense(m_tabBatiment.get(m_tabBatiment.size() - 1).getPrix());
			        			}
			        		}
		        		}
		        		if(poserTour){
		        			if(ajouterTour(objChoisiActu, caseChoisi)){
			        		
				        		if(isCheminBloquer() || m_carte.getCaseDeb() == caseChoisi){
				        			suppressionBatiment(m_tabBatiment.size() - 1);
				        			tabAttaquant.remove(tabAttaquant.size() - 1);
				        			batimentCreer = false;
				     		   }
				        	   else {
				        		    Stats.poseTour();
				        			batimentCreer = true;
				        			m_joueur.perdreArgent(m_tabBatiment.get(m_tabBatiment.size() - 1).getPrix());
				        			Stats.ajoutOrDepense(m_tabBatiment.get(m_tabBatiment.size() - 1).getPrix());
				        	   }
		        			}
		        		}
	        		}
	        	}
	        	break;
	        case Piege:
	        	if(verifCaseBatiment(caseChoisi, NomTypeBatiment.Barricade, false) == -1){
	        		if(ajouterPiege(objChoisiActu, caseChoisi)){
	        			batimentCreer = true;
	        			m_joueur.perdreArgent(m_tabBatiment.get(m_tabBatiment.size() - 1).getPrix());
	        			Stats.ajoutOrDepense(m_tabBatiment.get(m_tabBatiment.size() - 1).getPrix());
	        		}
	        	}
	        	break;
	        default:
	        	batimentExist = false;
	        	break;
	        }
		}
		if(batimentCreer){
			LibSong.playSound("Son Pose");
		}
		
		return batimentExist;
	}
	
	/**
	* Fonction de cr�ation d'une barricade
	* @param caseChoisi
	*/
	public boolean ajouterBarricade(final ACase caseChoisi){
		Barricade barricade = new Barricade(new GestionPositionBatiment(caseChoisi), Constantes.getInt("PRIX_BARRICADE"));
		
		if(m_joueur.verificationPossedeAssezArgentPourAcheter(barricade.getPrix())){
			ajoutBatiment(barricade);
			return true;
		}
		return false;
	}
	
	/**
	* Fonction de cr�ation d'une tour
	* @param caseChoisi
	*/
	public boolean ajouterTour(int idTour, ACase caseTemp)
	{ 
		 /*
		  *  I) typeTour : model.batiment.tour (TourDuSoleil, TourArchere, TourBombardier, etc...)
		  */
		 //TypeTour typeTour = TypeTour.values()[ficTour.getInfoInt(idTour, Constantes.getInt("FIC_TOUR_NUM"))];
		 
		 
		 /* 
		  * II) patternTour : model.batiment.tour.pattern (PatternTourEtoile, PatternTourCroix, etc...).
		  * 	1) On r�cup�re le ID du PatternTour � partir du fichier infoTour(ficTour).
		  * 	2) On r�cup�re le nom de la classe avec le fichier infoPAtternTour(ficPatternTour).
		  * 	3) On r�cup�re la portee avec le fichier infoPAtternTour(ficPatternTour).
		  * 	4) On instancie la classe avec le nom r�cup�rer pr�cedemment.
		  */
		 int iDPatternTour = tabFic.get("Tour").getInfoInt(idTour, Constantes.getInt("FIC_TOUR_NUM_PATTERN_TOUR"));
		 String nomPatternTour = TypePatternTour.values()[tabFic.get("PatternTour").getInfoInt(iDPatternTour, Constantes.getInt("FIC_PATTERN_TOUR_FORME"))].getNomClasse();
		 int portee = tabFic.get("PatternTour").getInfoInt(iDPatternTour, Constantes.getInt("FIC_PATTERN_TOUR_PORTE"));
		// String cheminPackage = "model.batiment.tour.pattern.";
		 		 
		 Object objet = FactoryInstanceByNom.getInstance(ClassName.trouverNom(nomPatternTour), new Object[] { portee, caseTemp});
		 
		 if(objet != null){
			 APatternTour patternTour = (APatternTour) objet;
			 
			 /*
			  * III) patternTir : model.tir.pattern (APatternTir)
			  * 	1) On r�cup�re le ID du PatternTir � partir du fichier infoTour(ficTour).
			  * 	2) On r�cup�re les degats avec le fichier infoPAtternTir(ficPatternTir).
			  * 	3) On r�cup�re la cadence avec le fichier infoPAtternTir(ficPatternTir).
			  * 	4) On instancie un PaternTir avec les infos r�cuper�s. 
			  */
			 int idPatternTir = tabFic.get("Tour").getInfoInt(idTour, Constantes.getInt("FIC_TOUR_NUM_PATTERN_TIR"));
			 int prix = tabFic.get("Tour").getInfoInt(idTour, Constantes.getInt("FIC_TOUR_PRIX"));
			 
			 String nomPatternTir = TypePatternTir.values()[tabFic.get("PatternTir").getInfoInt(idPatternTir, Constantes.getInt("FIC_PATTERN_TIR_TYPE"))].getNomClasse();
			 int degat = tabFic.get("PatternTir").getInfoInt(idPatternTir, Constantes.getInt("FIC_PATTERN_TIR_DEGAT"));
			 double cadence = tabFic.get("PatternTir").getInfoDouble(idPatternTir, Constantes.getInt("FIC_PATTERN_TIR_CADENCE"));
			 
			 Object objetPatternTir = FactoryInstanceByNom.getInstance(ClassName.trouverNom(nomPatternTir), new Object[] { degat, cadence});
			 
			 if(objetPatternTir != null){
				 APatternTir patternTir = (APatternTir) objetPatternTir;
				 
				 /*
				  * IV) typeTir : model.tir (ATir)
				  */
				 ATir typeTir = new TirStandard(5, new GestionPositionTirStandard(caseTemp, new Vector2(0, 0), 1));
				 
				 
				 // Instance de ATour
				 ATour tour = new TourStandard(prix, caseTemp, patternTour, idTour, patternTir, typeTir);
				 
				 if(m_joueur.verificationPossedeAssezArgentPourAcheter(tour.getPrix())){
					 ajoutBatiment(tour);
					 tabAttaquant.add(tour);
					return true;
				 }
			 }
		 }
		return false;
		
	}
	
	/**
	* Fonction de cr�ation d'un pi�ge
	* @param caseChoisi
	*/
	public boolean ajouterPiege(int idPiege, ACase caseTemp)
	{ 
		int numEffetPiege = tabFic.get("Piege").getInfoInt(idPiege, Constantes.getInt("FIC_PIEGE_NUM_EFFET_PIEGE"));
		int degatPiege = tabFic.get("EffetPiege").getInfoInt(numEffetPiege, Constantes.getInt("FIC_EFFET_PIEGE_DEGAT"));
		double cadencePiege = tabFic.get("Piege").getInfoDouble(idPiege, Constantes.getInt("FIC_PIEGE_CADENCE"));
		int prix = tabFic.get("Piege").getInfoInt(idPiege, Constantes.getInt("FIC_PIEGE_PRIX"));
		
		APiege lePiege = new PiegeStandard(idPiege, new GestionPositionBatiment(caseTemp), prix, new EffetPiegeDegat(degatPiege), cadencePiege);
		
		 
		 if(m_joueur.verificationPossedeAssezArgentPourAcheter(lePiege.getPrix())){
			 ajoutBatiment(lePiege);
			 tabAttaquant.add(lePiege);
			return true;
		 }
		return false;
		
	}
	
	/**
	* Lance la bonne fonction d'am�lioration en fonction du type d'am�lioration choisi
	*/
	public void lancerAmelioration(){
		switch(typeAmeliorationChoisi){
		case 0:
			ameliorerPatternTour();
			break;
		case 1:
			ameliorerPatternTir();
			break;
		default:
			break;
		}
	}
	
	public void ameliorerPatternTour(){
		int prixAmelioration = tabFic.get("PatternTour").getInfoInt(numAmeliorationChoisi, Constantes.getInt("FIC_PATTERN_TOUR_PRIX"));
	
		if(m_joueur.verificationPossedeAssezArgentPourAcheter(prixAmelioration)){
			 
			String nomPatternTour = TypePatternTour.values()[tabFic.get("PatternTour").getInfoInt(numAmeliorationChoisi, Constantes.getInt("FIC_PATTERN_TOUR_FORME"))].getNomClasse();
			int portee = tabFic.get("PatternTour").getInfoInt(numAmeliorationChoisi, Constantes.getInt("FIC_PATTERN_TOUR_PORTE"));
			
			new AmeliorationPatternTour(nomPatternTour, m_tabBatiment.get(tourEnModif), new Object[] { portee, m_tabBatiment.get(tourEnModif).getCase() }).ExecuterSiPossible();
		
			m_joueur.perdreArgent(prixAmelioration);
		}
	}
	
	public void ameliorerPatternTir(){
		int prixAmelioration = tabFic.get("PatternTir").getInfoInt(numAmeliorationChoisi, Constantes.getInt("FIC_PATTERN_TIR_PRIX"));
	
		if(m_joueur.verificationPossedeAssezArgentPourAcheter(prixAmelioration)){
			 
			String nomPatternTir = TypePatternTir.values()[tabFic.get("PatternTir").getInfoInt(numAmeliorationChoisi, Constantes.getInt("FIC_PATTERN_TIR_TYPE"))].getNomClasse();
			int degat = tabFic.get("PatternTir").getInfoInt(numAmeliorationChoisi, Constantes.getInt("FIC_PATTERN_TIR_DEGAT"));
			double cadence = tabFic.get("PatternTir").getInfoDouble(numAmeliorationChoisi, Constantes.getInt("FIC_PATTERN_TIR_CADENCE"));
			 
			new AmeliorationTypeTir(nomPatternTir, m_tabBatiment.get(tourEnModif), new Object[] { degat, cadence }).ExecuterSiPossible();
		
			m_joueur.perdreArgent(prixAmelioration);
		}
	}
	
	public void setAmelioration(final int typeAmelioration, final int numAmelioration){
		
		typeAmeliorationChoisi = typeAmelioration;
		numAmeliorationChoisi = numAmelioration;
	}
	
	/**
	* Retourne le prix de l'am�lioration courante, en fonction de son type
	* @return
	*/
	public int getPrixAmelioration(){
		int prixReturn = 0;
		switch(typeAmeliorationChoisi){
		case 0:
			prixReturn = tabFic.get("PatternTour").getInfoInt(numAmeliorationChoisi, Constantes.getInt("FIC_PATTERN_TOUR_PRIX"));
			break;
		case 1:
			prixReturn = tabFic.get("PatternTir").getInfoInt(numAmeliorationChoisi, Constantes.getInt("FIC_PATTERN_TIR_PRIX"));
			break;
		default:
			break;
		}
		
		return prixReturn;
	}
	
	public int getAmelioration(){
		return numAmeliorationChoisi;
	}
	
	public int getTypeAmelioration(){
		return typeAmeliorationChoisi;
	}
	
	public void cooldownSupervision()
	{
		m_carte.cooldownSupervision();
	}
	
	public void setObjActu(final int newNum){
		//V�rification que le num�ro est bien compris entre 0 et le maximum du type choisi
		if(newNum >= 0 && ((typeObjChoisi == NomTypeBatiment.Tour && newNum < getNbTour()) || (typeObjChoisi == NomTypeBatiment.Piege && newNum < getNbPiege()))){
			objChoisiActu = newNum;
		}
	}
	
	public int getObjActu(){
		return objChoisiActu;
	}
	
	public void setTypeObjActu(final NomTypeBatiment newType){
		typeObjChoisi = newType;
	}
	
	public NomTypeBatiment getTypeObjActu(){
		return typeObjChoisi;
	}
	
	public void setTourEnModif(final int newTour){
		tourEnModif = newTour;
		
		if(getTypeAmelioration() != -1){
			setAmelioration(-1, -1);
		}
	}
	
	public int getTourEnModif(){
		return tourEnModif;
	}
	
	public int getNbTour(){
		return tabFic.get("Tour").getNbLigne();
	}
	
	public int getNbPiege(){
		return tabFic.get("Piege").getNbLigne();
	}
	
	public AFichier getFic(final String cle){
		if(tabFic.containsKey(cle)){
			return tabFic.get(cle);
		}
		
		return null;
	}
	
	public boolean verificationJoueurPossedeAssezArgentPourAcheter(final int argent) {
		return this.m_joueur.verificationPossedeAssezArgentPourAcheter(argent);
	}
	
	public void joueurPerdArgent(final int argent) {
		this.m_joueur.perdreArgent(argent);
	}
	
	/**
	 * Pour code de triche
	 * @param argent
	 */
	public void joueurGagneArgent(final int argent) {
		this.m_joueur.gagnerArgent(argent);
	}
	
	public int getArgentJoueur() {
		return this.m_joueur.getArgent();
	}
	
	public int getVieJoueur() {
		return this.m_joueur.getPointsDeVie();
	}
	
	public int getScoreJoueur(){
		return this.m_joueur.getScore();
	}
	
	public int getPrixObjActu(final boolean avecTauxVente){
		int prixRetour = 0;
		
		switch(typeObjChoisi){
        case Barricade:
        	prixRetour = Constantes.getInt("PRIX_BARRICADE");
        	break;
        case Tour:
        	prixRetour = tabFic.get("Tour").getInfoInt(objChoisiActu, Constantes.getInt("FIC_TOUR_PRIX"));
        	break;
        case Piege:
        	prixRetour = tabFic.get("Piege").getInfoInt(objChoisiActu, Constantes.getInt("FIC_PIEGE_PRIX"));
        	break;
        default:
        	break;
        }
		
		if(avecTauxVente){
			prixRetour = (int)((double)prixRetour * ((double)tauxVente / 100));
		}
		
		return prixRetour;
	}
	
	
	
	public boolean getPartieFini(){
		return partieFini;
	}
	
	public void setPartieFini(boolean newVal){
		partieFini = newVal;
	}
	
	public ArrayList<ABatiment> getBatiment(){
		return m_tabBatiment;
	}
	
	public ArrayList<IDeplacable> getDeplacable(){
		return tabDeplacable;
	}
	
	public ArrayList<IAvecVie> getVivants(){
		return tabVivant;
	}
	
	public Vague getVague(){
		return vagueEnnemi;
	}
	
	public void setJouerPossible(boolean newVal){
		jeuPossible = newVal;
	}
	
	public boolean getJouerPossible(){
		return jeuPossible;
	}

	public abstract void setNouvellePartie(boolean newVal);
	
	public boolean getNouvellePartie(){
		return nouvellePartie;
	}
	
	/**
	* Cr�ation de la vague
	* Chaque monstre est envoy� tout les x secondes, en fonction du param�tre d�fini dans le fichier de vague, pour chaque vague
	*/
	public abstract void creationVague();
}





