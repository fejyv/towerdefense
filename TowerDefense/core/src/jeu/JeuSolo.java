package jeu;

import model.monstre.AMonstreCarte;
import model.monstre.TypeMonstre;
import outil.Constantes;
import outil.typeFichier.FichierVague;

public class JeuSolo extends AJeu {

	private int numVagueActu;
	private int numMobCreatVagueActu;
	private boolean creatVague;
	
	public JeuSolo(){
		super();
	}
	
	public void newPartiePlus(){
		this.numVagueActu = 0;
		this.numMobCreatVagueActu = 0;
		this.creatVague = true;
	}
	
	
	/**
	* Fabrique � monstre en fonction du type de celui-ci
	* @param numMonstre
	*/
	public void ajouterMonstre(int numMonstre)
	{
		TypeMonstre typeMonstre = TypeMonstre.values()[tabFic.get("Monstre").getInfoInt(numMonstre, Constantes.getInt("FIC_MOB_TYPE"))];
		
		switch(typeMonstre)
		{
		case Standard:
			AMonstreCarte mobTemp = creatMob(numMonstre);
			tabDeplacable.add(mobTemp);
			vagueEnnemi.add(mobTemp);
			tabVivant.add(mobTemp);
			break;
		default:
			break;
		}
	}
	
	/**
	 * V�rifie si il reste des monstres dans la vague en cours
	 * Si oui, on envoie au fur et � mesure les monstres
	 * Sinon on r�cup�re la prochaine vague du fichier
	 */
	public void creationVague(){

		if(clockCreatMob.getElapsedTimeSecond() > tabFic.get("Vague").getInfoDouble(numVagueActu, Constantes.getInt("FIC_VAGUE_TIMER"))){

			if(numVagueActu >= tabFic.get("Vague").getNbLigne()){
				partieFini = true;
			}
			else{
				ajouterMonstre(tabFic.get("Vague").getInfoInt(numVagueActu, Constantes.getInt("FIC_VAGUE_DEB_MOB") + numMobCreatVagueActu));
				numMobCreatVagueActu++;
				clockCreatMob.restart();
				
				if(numMobCreatVagueActu >= ((FichierVague) tabFic.get("Vague")).getNbColonne(numVagueActu) - 1){
					numVagueActu++;
					numMobCreatVagueActu = 0;
					creatVague = false;
				}
			}
		}
	}

	public void setCreatVague(final boolean newVal){
		creatVague = newVal;
	}
	
	public void setNouvellePartie(boolean newVal){
		nouvellePartie = newVal;
	}
	
	public boolean getCreatVague(){
		return creatVague;
	}
}
