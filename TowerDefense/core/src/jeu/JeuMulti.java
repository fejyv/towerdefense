package jeu;

import java.util.ArrayList;

import model.gestionPosition.GestionPositionMonstreStandard;
import model.monstre.AMonstre;
import model.monstre.AMonstreCarte;
import model.monstre.MonstreBasique;
import model.monstre.MonstreToReseau;
import model.monstre.Vague;
import outil.Clock;
import outil.Constantes;
import reseau.AReseau;
import stats.Stats;

public class JeuMulti extends AJeu {

	private Vague vagueEnvoi;
	private Vague vagueEnCours;
	private ArrayList<Vague> vagueEnAttente;
	private int numMobPourVague;
	private Clock clockAvEnvoi;
	private AReseau reseau;
	
	public JeuMulti(AReseau reseau){
		super();
		this.reseau = reseau;
		this.reseau.init(this);
		
	}
	
	public void newPartiePlus(){
		vagueEnvoi = new Vague();
		vagueEnCours = new Vague();
		vagueEnAttente = new ArrayList<Vague>();
		clockAvEnvoi = new Clock();
		numMobPourVague = -1;
	}

	/**
	 * Fonction de cr�ation de monstre destin� � �tre envoy� en r�seau
	 * @param numMonstre
	 * @return
	 */
	public MonstreToReseau creatMobReseau(int numMonstre){
		if(numMonstre >= 0 && numMonstre < tabFic.get("Monstre").getNbLigne()){
			int vieMax = tabFic.get("Monstre").getInfoInt(numMonstre, Constantes.getInt("FIC_MOB_VIE"));
			int degats = tabFic.get("Monstre").getInfoInt(numMonstre, Constantes.getInt("FIC_MOB_DEGAT"));
			int valeur = tabFic.get("Monstre").getInfoInt(numMonstre, Constantes.getInt("FIC_MOB_ARGENT"));
			double vitesse = tabFic.get("Monstre").getInfoDouble(numMonstre, Constantes.getInt("FIC_MOB_VITESSE"));
			int score = tabFic.get("Monstre").getInfoInt(numMonstre, Constantes.getInt("FIC_MOB_SCORE"));
			int prix = tabFic.get("Monstre").getInfoInt(numMonstre, Constantes.getInt("FIC_MOB_PRIX"));

			MonstreToReseau mobTemp = new MonstreToReseau(numMonstre, vieMax, degats, valeur, vitesse, score, prix);
			return mobTemp;
		}
		return null;
	}
	
	public void addMobVagueEnvoi(){
		
		MonstreToReseau tmp = creatMobReseau(numMobPourVague);
		if(getArgentJoueur() >= tmp.getPrix())
		{
			joueurPerdArgent(tmp.getPrix());
			Stats.ajoutOrDepense(tmp.getPrix());
			vagueEnvoi.add(tmp);
		}
	}
	
	public void addVagueListe(Vague newVague){
		//System.out.println("Reception : " + newVague.get().size() + " monstres");
		vagueEnAttente.add(newVague);
	}
	
	public void setMobPourVague(int num){
		numMobPourVague = num;
	}
	
	public int getMobPourVague(){
		return numMobPourVague;
	}
	
	/**
	 * Fonction v�rifiant si le timer d'envoi est �coul� ou si l'utilisateur souhaite directement envoyer sa vague
	 * Cr�er une copie de la vague qui est envoy� (probl�me si directement envoy�, envoi de l'image du premier envoi en boucle
	 * @param forcerEnvoi
	 */
	public void envoiVague(boolean forcerEnvoi){
		if(clockAvEnvoi.getElapsedTimeSecond() > 10 || forcerEnvoi){
			//System.out.println("Avant Envoi : " + vagueEnvoi.get().size() + " monstres");
		
			if(vagueEnvoi.get().size() > 0){
				Vague temp = new Vague();
				
				for(AMonstre tmp : vagueEnvoi.get()){
					if(tmp instanceof MonstreToReseau){
						temp.add((MonstreToReseau)tmp);
					}
				}
				
				reseau.envoyerObjet(temp);
				//System.out.println("Envoi : " + vagueEnvoi.get().size() + " monstres");
				vagueEnvoi.clear();
				clockAvEnvoi.restart();
			}
			else if(!forcerEnvoi){
				clockAvEnvoi.restart();
			}
		}
	}
	
	
	
	public Vague getVagueEnvoi(){
		return vagueEnvoi;
	}
	
	public Clock getClockAvEnvoi(){
		return clockAvEnvoi;
	}
	
	public boolean getCoTrouver(){
		return reseau.getCoTrouver();
	}
	
	/**
	* Fabrique � monstre en fonction du type de celui-ci
	* @param numMonstre
	*/
	public void ajouterMonstre(AMonstreCarte mob)
	{
		mob.setGestionPos(new GestionPositionMonstreStandard(m_carte.getCaseDeb()));
		mob.getGestionPos().calculerChemin(this);
	
		AMonstreCarte mobTemp = mob;
		tabDeplacable.add(mobTemp);
		vagueEnnemi.add(mobTemp);
		tabVivant.add(mobTemp);
	}

	
	/**
	* Fabrique � monstre en fonction du type de celui-ci
	* @param numMonstre
	*/
	public void ajouterMonstre(AMonstre mob)
	{
		AMonstreCarte tmp = new MonstreBasique(mob, new GestionPositionMonstreStandard(m_carte.getCaseDeb()));
		tmp.getGestionPos().calculerChemin(this);
		tabDeplacable.add(tmp);
		vagueEnnemi.add(tmp);
		tabVivant.add(tmp);
	}
	
	/**
	 * V�rifi� si il reste des monstres dans la vague en cours
	 * Si oui, on envoie au fur et � mesure les monstres
	 * Sinon on r�cup�re la premi�re vague de la liste d'attente
	 */
	public void creationVague(){
		
		if(vagueEnCours.get().size() == 0){
			if(vagueEnAttente.size() > 0){
				vagueEnCours = vagueEnAttente.get(0);
				vagueEnAttente.remove(0);
				//System.out.println("Ajout de " + vagueEnCours.get().size() + " monstres a la vague en cours");
			}
		}
		else{
			if(clockCreatMob.getElapsedTimeSecond() > tabFic.get("Vague").getInfoDouble(0, Constantes.getInt("FIC_VAGUE_TIMER"))){
	
				ajouterMonstre(vagueEnCours.get().get(0));
				vagueEnCours.del(0);
				clockCreatMob.restart();
			}
		}
	}
	
	public void envoyerMsg(String msg){
		reseau.envoyerObjet(msg);
	}
	
	public void setNouvellePartie(boolean newVal){
		nouvellePartie = newVal;
		envoyerMsg("NewGame");
	}
	
	/**
	 * Retourne le temps avant le prochain envoi de la vague
	 * @return
	 */
	public int getTimeEnvoi(){
		if((int) (10 - clockAvEnvoi.getElapsedTimeSecond()) > 0){
			return (int) (10 - clockAvEnvoi.getElapsedTimeSecond());
		}

		return 0;
	}
	
	public void dispose(){
		if(reseau.getCoTrouver()){
			reseau.dispose();
		}
	}
}
