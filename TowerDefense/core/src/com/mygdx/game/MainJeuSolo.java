package com.mygdx.game;


import control.AControlGame;
import control.ControlFinJeu;
import control.ControlGameSolo;
import jeu.JeuSolo;
import view.AViewGame;
import view.ViewGameSolo;

/**
 * 
 * @author Florian
 *
 */
public class MainJeuSolo implements IMain{
	
	private AViewGame vue;
	private AControlGame control;
	private JeuSolo game;
	
	public MainJeuSolo(){
		game = new JeuSolo();
		
		this.control = new ControlGameSolo(game);
		control.gestionTouche();
		
		this.vue = new ViewGameSolo(game, control);

	}

	public void render() {
		if(!game.getPartieFini()){
			if(!game.joueurEstMort())
			{
				
				
				vue.afficher();
				game.gainOrContinu();
				game.actionsBatiments();
				game.nettoyerMorts();
				game.bouger();
				game.gererDegatsJoueur();
				game.cooldownSupervision();
				
				if(game.getCreatVague()){
					game.creationVague();
				}
				else {
					if(!game.monstreVivant()){
						game.setCreatVague(true);
					}
				}
				
				if(game.getPartieFini() || game.joueurEstMort()){
					control = new ControlFinJeu(game);
					control.gestionTouche();
					System.out.println("Fin Jeu");
					
				}
			}
			else
			{
				vue.gameOver();
			}
		}
		else{
			vue.gameWin();
		}
		
		if(game.getNouvellePartie()){
			game.newPartie();
			
			control = new ControlGameSolo(game);
			control.gestionTouche();
			vue.setControleur(control);
			
			game.setNouvellePartie(false);
		}
	}

	public void dispose() {
		vue.dispose();
	}
	
	public boolean getRetourMenu(){
		return control.getRetourMenu();
	}
}
