package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;

import outil.Constantes;
import outil.libSong.LibSong;
import reseau.ReseauClient;
import reseau.ReseauServeur;

public class MyGdxGame extends ApplicationAdapter {
	
	IMain partie;
	LibSong libSong;

		
	@Override
	public void create () {
		Constantes.init("../core/assets/properties/ficConstante.properties");

		partie = new MainMenu();
		
		libSong = new LibSong();
		
		//Chargement des sons et musiques
		LibSong.initSound("son/Sound/poserBatiment.wav", "Son Pose");
		LibSong.initSound("son/Sound/suppresionBatiment.wav", "Son suppression Batiment");
		
		//LibSong.initMusic("son/Music/chaos sur terre.wav", "Musique fond");
		//LibSong.initMusic("son/Music/Epic Greenland - Magical.mp3", "Musique fond 2");
		
		LibSong.listerRepertoire("son/Music/");
		LibSong.playMusic();
		
		
	}

	@Override
	public void render() {
		partie.render();
		
		if(partie instanceof MainJeuSolo){
			if(((MainJeuSolo)partie).getRetourMenu()){
				partie.dispose();
				
				partie = new MainMenu();
			}
		}
		else if(partie instanceof MainJeuMulti){
			if(((MainJeuMulti)partie).getRetourMenu()){
				partie.dispose();
				
				partie = new MainMenu();
			}
		}
		else if(partie instanceof MainMenu){
			switch(((MainMenu)partie).getTypeGameChoisi()){
			case Solo:
				partie.dispose();
				
				partie = new MainJeuSolo();
				break;
			case MultiServeur:
				partie.dispose();
				
				partie = new MainJeuMulti(new ReseauServeur());
				break;
			case MultiClient:
				partie.dispose();
				
				partie = new MainJeuMulti(new ReseauClient(((MainMenu) partie).getIpConnexion()));
				break;
			default:
				break;
			}
		}
		
	}
	
	@Override
	public void dispose() {
		partie.dispose();
		LibSong.dispose();
	}
}
