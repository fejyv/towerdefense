package com.mygdx.game;


import control.ControlMenu;
import control.TypePartie;
import stats.Stats;
import view.ViewMenu;

/**
 * 
 * @author Florian
 *
 */
public class MainMenu implements IMain {
	
	private ViewMenu vue;
	private ControlMenu control;
	
	public MainMenu(){
		
		this.control = new ControlMenu();
		control.gestionTouche();

		this.vue = new ViewMenu(control);
		
		Stats.resetGame();
	}

	public void render() {

		vue.afficher();
	}

	public void dispose() {
		vue.dispose();
	}
	
	public TypePartie getTypeGameChoisi(){
		return control.getTypeGameChoisi();
	}

	public String getIpConnexion(){
		return control.getIpConnexion();
	}
	
}
