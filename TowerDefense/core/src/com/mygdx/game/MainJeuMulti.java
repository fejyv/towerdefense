package com.mygdx.game;

import chat.GestionChat;
import control.AControlGame;
import control.ControlFinJeu;
import control.ControlGameMulti;
import jeu.JeuMulti;
import reseau.AReseau;
import reseau.ReseauServeur;
import view.AViewGame;
import view.ViewGameMulti;

/**
 * 
 * @author Florian
 *
 */
public class MainJeuMulti implements IMain{
	
	private AViewGame vue;
	private AControlGame control;
	private JeuMulti game;
	private boolean peuxPasser;
	
	public MainJeuMulti(AReseau reseau){
		game = new JeuMulti(reseau);
		
		this.control = new ControlGameMulti(game);
		control.gestionTouche();
		
		this.vue = new ViewGameMulti(game, control);
		
		peuxPasser = true;
		
		if(!game.getCoTrouver()){
			control.setRetourMenu(true);
		}
		else{
			if(reseau instanceof ReseauServeur){
				GestionChat gestionChat = new GestionChat();
				gestionChat.lancerChat();
			}
		}
	}

	public void render() {
		
		if(game.getCoTrouver()){
			if(!game.getPartieFini()){
				if(!game.joueurEstMort())
				{
					vue.afficher();
					game.gainOrContinu();
					game.actionsBatiments();
					game.nettoyerMorts();
					game.bouger();
					game.gererDegatsJoueur();
					game.cooldownSupervision();
					
					game.envoiVague(false);
					
					game.creationVague();
	
				}
				else
				{
					vue.gameOver();
				}
			}
			else{
				vue.gameWin();
			}
			
			if(game.getNouvellePartie() && game.getJouerPossible()){
				game.newPartie();
				
				control = new ControlGameMulti(game);
				control.gestionTouche();
				vue.setControleur(control);
				
				game.setNouvellePartie(false);
				peuxPasser = true;
			}
	
			if(peuxPasser && (game.getPartieFini() || game.joueurEstMort())){
				if(game.joueurEstMort()){
					game.envoyerMsg("Victoire");
				}
				
				control = new ControlFinJeu(game);
				control.gestionTouche();
				System.out.println("Fin Jeu");
				game.setJouerPossible(false);
				peuxPasser = false;
			}
		}
		
	}

	public void dispose() {
		vue.dispose();
		game.dispose();
	}
	
	public boolean getRetourMenu(){
		return control.getRetourMenu();
	}
}
