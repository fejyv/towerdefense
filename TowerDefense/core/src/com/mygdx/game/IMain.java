package com.mygdx.game;


/**
 * 
 * @author Florian
 *
 */
public interface IMain {
	
	public abstract void render();
	public abstract void dispose();
}
