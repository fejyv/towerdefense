package amelioration;

import model.batiment.ABatiment;
import model.batiment.tour.ATour;
import outil.ClassName;

public abstract class AAmelioration {
	
	protected ClassName nomAmelioration;
	
	protected ATour tourAAmeliorer;
	protected Object[] param;
	
	public AAmelioration(String nomAmelioration, ABatiment tour, Object[] param) {
		this.tourAAmeliorer = (ATour)tour;
		this.param = param;
		
		this.nomAmelioration = ClassName.trouverNom(nomAmelioration);
		
		if(this.nomAmelioration == null){
			System.out.println("L'am�lioration n'a pas pu �tre trouv�e. Mise en place de l'am�lioration par d�faut");
			this.nomAmelioration = ClassName.PatternTourEtoile;
		}
	}
	
	public void ExecuterSiPossible() {

		// Si la tour ne poss�de pas deja l'amelioration

		if(!posedeDejaAmelioration(this.nomAmelioration)) {

			// Si on arrive � instancier l'am�lioration
			if(genererInstanceAmelioration(this.nomAmelioration)) {

				// On am�liore
				ameliorer();
				System.out.println("Am�lioration r�ussie, la tour poss�de maintenant le : " + this.nomAmelioration.getClassName());
			}
			else{
				System.out.println("Erreur lors de la g�n�ration de l'am�lioration amelioration");
			}
		
		}
	}
	
	// Retourne Vrai si on a pu instancier la classe
	protected abstract boolean genererInstanceAmelioration(ClassName classe);

	protected abstract void ameliorer();
	protected abstract boolean posedeDejaAmelioration(ClassName nomAmelioration);
	protected abstract boolean importationClasseReussi(Object objet);
}
