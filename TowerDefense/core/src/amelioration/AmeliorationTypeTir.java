package amelioration;

import model.batiment.ABatiment;
import model.tir.pattern.APatternTir;
import outil.ClassName;
import outil.FactoryInstanceByNom;

public class AmeliorationTypeTir extends AAmelioration{

	private APatternTir modeleAImporter;
	
	public AmeliorationTypeTir(String nomAmelioration, ABatiment tour, Object[] param) {
		super(nomAmelioration, tour, param);
	}

	@Override
	protected boolean posedeDejaAmelioration(ClassName classe) {
		try{
			if(this.tourAAmeliorer.getModelPatternTir().getClass().getName().equals(classe.getClassName()) && 
					tourAAmeliorer.getModelPatternTir().getDegat() == (Integer)param[0] && 
					tourAAmeliorer.getModelPatternTir().getCadence() == (Double)param[1]) {
				System.out.println("La tour poss�de d�j� le Pattern de tir : " + this.nomAmelioration.getClassName());
				return true;
			}
		}
		catch (NullPointerException ex){
			System.out.println(ex);
		}
		
		return false;
	}

	@Override
	protected boolean importationClasseReussi(Object objet) {
		
		this.modeleAImporter = (APatternTir) objet;
		
		if(this.modeleAImporter != null) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	protected void ameliorer() {
		this.tourAAmeliorer.ameliorerTypeDeTir(this.modeleAImporter);
	}

	@Override
	protected boolean genererInstanceAmelioration(ClassName classe) {
		Object objet = FactoryInstanceByNom.getInstance(classe, param);
		return this.importationClasseReussi(objet);
	}

}
