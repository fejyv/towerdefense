package amelioration;

import outil.ClassName;
import outil.FactoryInstanceByNom;
import model.batiment.ABatiment;
import model.batiment.tour.pattern.APatternTour;

public class AmeliorationPatternTour extends AAmelioration {

	private APatternTour modeleAImporter;
	
	public AmeliorationPatternTour(String nomAmelioration, ABatiment tour, Object[] param) {
		super(nomAmelioration, tour, param);
	}

	@Override
	protected boolean importationClasseReussi(Object objet) {
		this.modeleAImporter = (APatternTour) objet;
		if(this.modeleAImporter != null) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	protected boolean posedeDejaAmelioration(ClassName classe) {
		try{
			if(this.tourAAmeliorer.getModelPatternTour().getClass().getName().equals(classe.getClassName()) && 
					tourAAmeliorer.getModelPatternTour().getPortee() == (Integer)param[0]) {
				System.out.println("La tour poss�de d�j� le Pattern de tour : " + this.nomAmelioration.getClassName());
				return true;
			}
		}
		catch (NullPointerException ex){
			System.out.println(ex);
		}
		
		return false;
	}

	@Override
	protected void ameliorer() {
		this.tourAAmeliorer.ameliorerPatternTour(this.modeleAImporter);
	}

	@Override
	protected boolean genererInstanceAmelioration(ClassName classe) {
		Object objet = FactoryInstanceByNom.getInstance(classe, param);
		return this.importationClasseReussi(objet);
	}

	
}
