package model.batiment.piege;

import model.monstre.AMonstre;

public abstract class AEffetPiege {

	public abstract void appliquerEffet(AMonstre unMonstre);
}