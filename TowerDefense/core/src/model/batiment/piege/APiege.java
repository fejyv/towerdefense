package model.batiment.piege;

import java.util.ArrayList;

import model.batiment.ABatiment;
import model.batiment.NomTypeBatiment;
import model.gestionPosition.GestionPositionBatiment;
import outil.Clock;
import typeElement.IAttaquant;
import typeElement.IDeplacable;
   
public abstract class APiege extends ABatiment implements IAttaquant{
	
	protected Clock clock;
	protected AEffetPiege effet;
	protected double cadence;
	
	{
		this.clock = new Clock();
	}
	

	public APiege(final int typePiege, final GestionPositionBatiment position, final int prix,  final AEffetPiege effet, final double cadence)
	{
		super(NomTypeBatiment.Piege, position, typePiege, prix);

		this.effet = effet;
		this.cadence = cadence;
	}
	
	
	abstract public ArrayList<IDeplacable> attaquer();
	
}
