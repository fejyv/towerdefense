package model.batiment.piege;

import model.monstre.AMonstre;

public class EffetPiegeDegat extends AEffetPiege {
	private int degats;
	
	public EffetPiegeDegat(final int degats)
	{
		this.degats = degats;
	}
	
	public void appliquerEffet(AMonstre unMonstre) {
		unMonstre.blesser(degats);
	}

}
