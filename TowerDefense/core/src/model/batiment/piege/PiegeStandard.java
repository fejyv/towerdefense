package model.batiment.piege;

import java.util.ArrayList;

import model.gestionPosition.GestionPositionBatiment;
import model.monstre.AMonstreCarte;
import typeElement.IDeplacable;

public class PiegeStandard extends APiege {

	public PiegeStandard(final int typePiege, final GestionPositionBatiment position, final int prix, final AEffetPiege effet, final double cadence) {
		super(typePiege, position, prix, effet, cadence);
	}

	/**
	 * Si le pige attaque, il r�cup�re tout les monstres qui sont sur �a case et leur inflige des dommages
	 */
	public ArrayList<IDeplacable> attaquer(){
		if (this.clock.getElapsedTimeSecond() > cadence) {
			ArrayList<AMonstreCarte> listeMonstre = position.getCase().getEnnemi();
			for(AMonstreCarte unMonstre : listeMonstre)
			{
				effet.appliquerEffet(unMonstre);
			}
			clock.restart();
		}
		
		return new ArrayList<IDeplacable>();
	}



}
