package model.batiment;

/**
 * 
 * @author Florian
 *
 */
public enum NomTypeBatiment {
	Barricade,
	Tour,
	Piege,
	None;
}
