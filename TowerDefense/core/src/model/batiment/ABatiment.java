package model.batiment;

import model.caseMod.ACase;
import model.gestionPosition.GestionPositionBatiment;

/**
 * 
 * @author Florian
 *
 */

public class ABatiment {
	
	protected GestionPositionBatiment position;
	protected int prix;
	protected int typeNum;
	protected NomTypeBatiment typeDeBatiment;
	
	/**
	 * 
	 * @param typeDeBatiment
	 * @param position
	 * @param typeNum
	 * @param prix
	 */
	public ABatiment(final NomTypeBatiment typeDeBatiment, final GestionPositionBatiment position, final int typeNum, final int prix)
	{
		this.position = position;
		this.typeNum = typeNum;
		this.prix = prix;
		this.typeDeBatiment = typeDeBatiment;
	}
	
	public ACase getCase()
	{
		return position.getCase();
	}
	
	public int getPrix(){
		return prix;
	}
	
	public GestionPositionBatiment getGestionPosition(){
		return position;
	}
	
	public NomTypeBatiment getTypeBatiment(){
		return typeDeBatiment;
	}

	public int getTypeNum(){
		return typeNum;
	}
}
