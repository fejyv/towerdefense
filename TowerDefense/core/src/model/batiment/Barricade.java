package model.batiment;

import model.gestionPosition.GestionPositionBatiment;

/**
 * 
 * @author Florian
 *
 */
public class Barricade extends ABatiment {

	/**
	 * Batiment ne pouvant pas attaquer
	 * @param position
	 * @param prix
	 */
	public Barricade(final GestionPositionBatiment position, final int prix) {
		super(NomTypeBatiment.Barricade, position, 0, prix);
	}

}
