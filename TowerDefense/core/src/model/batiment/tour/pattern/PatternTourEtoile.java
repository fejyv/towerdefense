package model.batiment.tour.pattern;

import java.util.ArrayList;

import model.Carte;
import model.caseMod.ACase;

public class PatternTourEtoile extends APatternTour{

	public PatternTourEtoile(final int portee, final ACase c) {
		super(portee, c);
	}

	@Override
	public void setCasesTirPossible(final ACase c) {
		Carte carte = c.getCarte();
		int x = (int)c.getPosition().x;
		int y = (int)c.getPosition().y;
		
		this.casesTir = new ArrayList<ACase>();
		
		for(int i = 1; i <= this.portee; i++) {
			this.casesTir.add(carte.trouverCasePixelInt(x-i, y+i));
			this.casesTir.add(carte.trouverCasePixelInt(x+i, y+i));
			this.casesTir.add(carte.trouverCasePixelInt(x+i, y-i));
			this.casesTir.add(carte.trouverCasePixelInt(x-i, y-i));
		}
		
		this.retirerCaseEnDehorsCarte();
		
	}

}
