package model.batiment.tour.pattern;

import java.util.ArrayList;

import model.Carte;
import model.caseMod.ACase;

public class PatternTourStandard extends APatternTour{

	public PatternTourStandard(final int portee, final ACase c) {
		super(portee, c);
	}

	public PatternTourStandard(final ACase c) {
		super(c);
	}

	@Override
	public void setCasesTirPossible(final ACase c) {
		Carte carte = c.getCarte();
		int x = (int)c.getPosition().x;
		int y = (int)c.getPosition().y;
		
		ArrayList<ACase> cases = new ArrayList<ACase>();
		
		for(int i = x-this.portee; i <= this.portee; i++) {
			for(int j = y-this.portee; j <= this.portee; j++) {
				cases.add(carte.trouverCasePixelInt(x, y));
			}
		}
		cases.remove(c);
		this.retirerCaseEnDehorsCarte();
	}
	
}
