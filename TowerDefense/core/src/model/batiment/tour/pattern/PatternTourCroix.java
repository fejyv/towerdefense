package model.batiment.tour.pattern;

import java.util.ArrayList;

import model.Carte;
import model.caseMod.ACase;

public class PatternTourCroix extends APatternTour{

	public PatternTourCroix(final int portee, final ACase c) {
		super(portee, c);
	}

	public PatternTourCroix(ACase c) {
		super(c);
	}

	@Override
	public void setCasesTirPossible(final ACase c) {
		Carte carte = c.getCarte();
		int x = (int)c.getPosition().x;
		int y = (int)c.getPosition().y;
		
		this.casesTir = new ArrayList<ACase>();
		
		for(int i = 1; i <= this.portee; i++) {
			this.casesTir.add(carte.trouverCasePixelInt(x-i, y));
			this.casesTir.add(carte.trouverCasePixelInt(x+i, y));
			this.casesTir.add(carte.trouverCasePixelInt(x, y+i));
			this.casesTir.add(carte.trouverCasePixelInt(x, y-i));
		}
		this.retirerCaseEnDehorsCarte();
	}

}
