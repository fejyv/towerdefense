package model.batiment.tour.pattern;

import java.util.ArrayList;
import java.util.Collection;

import model.caseMod.ACase;

public abstract class APatternTour {

	
	public APatternTour(final ACase c) {
		setCasesTirPossible(c);
	}
	// Collection de ACase sur lesquels la tour peut tirer
	protected Collection<ACase> casesTir;
	protected int portee;
	
	public int getPortee() {
		return portee;
	}

	public void setPortee(final int portee) {
		this.portee = portee;
	}

	// Getter pour recup�rer casesTir
	public Collection<ACase> getCasesTir() {
		return casesTir;
	}
	
	public APatternTour(final int portee, final ACase c) {
		this.portee = portee;
		setCasesTirPossible(c);
	}
	
	// Setter qui initialise casesTir
	public abstract void setCasesTirPossible(final ACase c);

	// Retire les cases qui se trouvent en dehors de la carte
	protected void retirerCaseEnDehorsCarte() {
		ArrayList<ACase> casesTemp = new ArrayList<ACase>();
		try{
			for(ACase uneCase : this.casesTir) {
				if(uneCase == null){
					casesTemp.add(uneCase);
				}
			}
		}catch (NullPointerException e) {
			 // TODO Auto-generated catch block
			 e.printStackTrace();
		}
		this.casesTir.removeAll(casesTemp);
	}
	
	
}
