package model.batiment.tour.pattern;

public enum TypePatternTour {
	TourCroix(0, "PatternTourCroix"),
	TourEtoile(1, "PatternTourEtoile");

	private int num;
	private String nomClasse;
   
	TypePatternTour(final int num, final String nomClasse){
		this.num = num;
		this.nomClasse = nomClasse;
	}
   
	public int getNum(){
		return this.num;
	}
	
	public String getNomClasse() {
		return this.nomClasse;
	}
	
	public boolean equals(final int num){
		return (this.num == num);
	}
}
