package model.batiment.tour;

public enum TypeTour {
	TourDuSoleil(0),
	TourArchere(1),
	TourBombardier(2),
	TourCristauxRouge(3),
	TourCristauxBleu(4),
	TourCristauxVert(5),
	TourCristauxViolet(6);
	

	private int num;
   
	TypeTour(final int num){
		this.num = num;
	}
   
	public int getNum(){
		return num;
	}
	
	public boolean equals(final int num){
		return (this.num == num);
	}
   
	/*public String toString(){
	  return name;
	}*/
}
