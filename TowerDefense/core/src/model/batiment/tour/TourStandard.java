package model.batiment.tour;


import java.util.ArrayList;

import com.badlogic.gdx.math.Vector2;

import model.caseMod.ACase;
import model.gestionPosition.GestionPositionTirStandard;
import model.monstre.AMonstreCarte;
import model.batiment.tour.pattern.APatternTour;
import model.tir.pattern.APatternTir;
import outil.Constantes;
import outil.Outils;
import typeElement.IDeplacable;
import model.tir.ATir;

public class TourStandard extends ATour{
	

	public TourStandard(final int prix, final ACase c, final APatternTour modeleTir, final int typeTour, final APatternTir typePatternTir, final ATir typeTir)
	{
		super(prix, c, modeleTir, typeTour, typePatternTir, typeTir);
	}

	
	public ArrayList<IDeplacable> attaquer() {
		// Si la cadence est atteinte alors on v�rifie les monstre que la tour
		// peut tirer dessus
		ArrayList<IDeplacable> deplacableCree = new ArrayList<IDeplacable>();
		if (this.clock.getElapsedTimeSecond() > this.typePatternTir.getCadence()) {
			ArrayList<AMonstreCarte> listeMonstre = new ArrayList<AMonstreCarte>();
			try{
				for (ACase caseTemp : this.typePatternTour.getCasesTir()) {
					listeMonstre.addAll(caseTemp.getEnnemi());
				}
			}catch (NullPointerException e) {
				 // TODO Auto-generated catch block
				 e.printStackTrace();
			}
			if (!listeMonstre.isEmpty()) {
				AMonstreCarte cible = listeMonstre.get(0);
				for (AMonstreCarte monstre : listeMonstre) {
					if(monstre != null){
						if (monstre.getTailleChemin() < cible.getTailleChemin()) {
							cible = monstre;
						}
					}
				}
				
				//Creation du Tir
				
				ATir newTir = typeTir.getNewInstance();
				newTir.setTour(this);
				newTir.setGestionPosition(
				new GestionPositionTirStandard(cible.getCase(), Outils.getCoordObjMilieu(new Vector2(position.getPixelPosition().x + (Constantes.getInt("TAILLE_IMAGE") / 2), position.getPixelPosition().y - (Constantes.getInt("TAILLE_IMAGE") / 2))), 
						typeTir.getGestionPos().getVitesse(), cible.getGestionPos().getPixelPosition()));

				deplacableCree.add(newTir);
			}

			this.clock.restart();
		}
		
		return deplacableCree;
	}	
}
