
package model.batiment.tour;

import model.batiment.ABatiment;
import model.batiment.NomTypeBatiment;
import model.caseMod.ACase;
import model.gestionPosition.GestionPositionBatiment;
import model.batiment.tour.pattern.APatternTour;
import model.tir.ATir;
import model.tir.pattern.APatternTir;
import outil.Clock;
import typeElement.IAttaquant;

import java.util.HashMap;
import java.util.Iterator;


public abstract class ATour extends ABatiment implements IAttaquant{

	protected boolean enVie = true;

	protected APatternTour typePatternTour;
	protected APatternTir typePatternTir;
	protected ATir typeTir;
	
	protected HashMap<Integer, String> listeAmeliorationModeleDeTir;
	protected HashMap<Integer, String> listeAmeliorationTypeTir;

	protected Clock clock;


	public ATour(final int prix, final ACase uneCase, final APatternTour typePatternTour, final int typeTour, final APatternTir typePatternTir, final ATir typeTir) {
		super(NomTypeBatiment.Tour, new GestionPositionBatiment(uneCase), typeTour, prix);

		this.typePatternTour = typePatternTour;
		this.typePatternTir = typePatternTir;
		this.typeTir = typeTir;
		this.typeTir.setTour(this);
		chargerAmeliorationModeleDeTir();
		clock = new Clock();
	}

	
	private void chargerAmeliorationModeleDeTir() {
		this.listeAmeliorationModeleDeTir = new HashMap<Integer, String>();
		this.listeAmeliorationModeleDeTir.put(10, "PatternTourCroix");
		this.listeAmeliorationModeleDeTir.put(10, "PatternTourEtoile");
		//Faire une boucle qui r�cup�re le nom de classe des diff�rents mod�le de tir avec le co�t
	}
	
	@SuppressWarnings("unused")
	private void chargerAmeliorationTypeTir() {
		this.listeAmeliorationTypeTir = new HashMap<Integer, String>();
		//Faire une boucle qui r�cup�re le nom de classe des diff�rents type de tir avec le co�t
	}
	
	public void ameliorerPatternTour(final APatternTour typePatternTourAChanger) {
		this.typePatternTour = typePatternTourAChanger;
	}
	
	public void ameliorerTypeDeTir(final APatternTir typeDeTirAChanger) {
		this.typePatternTir = typeDeTirAChanger;
	}
	
	//A SUPPRIMER
	public int getPrixAmeliorationModeleDeTir(final String nomModele) {
		
		//R�cup�rer le prix du co�t du ModeleDeTir
		Iterator<Integer> keySetIterator = this.listeAmeliorationModeleDeTir.keySet().iterator(); 
		
		boolean trouve = false;
		int prix = -1;
		
		if(!this.listeAmeliorationModeleDeTir.isEmpty()) {
			while(keySetIterator.hasNext() && !trouve) { 
				Integer key = keySetIterator.next(); 
				if(this.listeAmeliorationModeleDeTir.get(key).toLowerCase().equals(nomModele.toLowerCase())) {
					trouve = true;
					prix = key;
				}
			}
		}
		
		return prix;
	}
	
	public APatternTour getModelPatternTour() {
		return this.typePatternTour;
	}
	
	public APatternTir getModelPatternTir(){
		return this.typePatternTir;
	}

}
