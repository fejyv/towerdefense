
package model;

import java.util.ArrayList;
import java.util.HashMap;

import model.caseMod.ACase;
import model.caseMod.CaseStandard;
import outil.Constantes;
import outil.Direction;
import outil.typeFichier.AFichier;
import outil.Outils;

/**
 * 
 * @author Florian
 *
 */
public class Carte {
	
	private ArrayList<ArrayList<ACase>> m_tabCase;
	private ArrayList<ArrayList<ACase>> m_tabCaseNiveau2;
	private ArrayList<ACase> m_caseEnCooldown;
		
	private ACase m_caseDeb;
	private ACase m_caseFin;
	
	//Donn�es contenus dans un fichier properties
	public static int NB_CASE_LARGEUR;
	public static int NB_CASE_HAUTEUR;
	
	/**
	 * 
	 * @param ficCarte
	 * @param ficCarteNiv2
	 * @param ficTile
	 * @param ficMapCaseVoisine
	 * @param ficTileCaseVoisine
	 */
	public Carte(final HashMap<String, AFichier> tabFic){
		m_tabCase = new ArrayList<ArrayList<ACase>>();
		m_tabCaseNiveau2 = new ArrayList<ArrayList<ACase>>();
		m_caseEnCooldown = new ArrayList<ACase>();
		
		NB_CASE_LARGEUR = tabFic.get("Carte").getNbColonne(); 
		NB_CASE_HAUTEUR = tabFic.get("Carte").getNbLigne(); 
		
		/**
		 * Initialisation d'une case :
		 * - posX
		 * - posY
		 * - temps maximum pass�
		 * - carte actu
		 */
		int numCaseChoisi;
		for(int j = 0 ; j < NB_CASE_HAUTEUR ; j++){
			ArrayList<ACase> tabCaseLigne = new ArrayList<ACase>();
			for(int i = 0 ; i < NB_CASE_LARGEUR ; i++){
				numCaseChoisi = tabFic.get("Carte").getInfoInt(j, i);
				tabCaseLigne.add(new CaseStandard(numCaseChoisi, i, j, tabFic.get("Tile").getInfoInt(numCaseChoisi, 1), 1, this));
			}
			m_tabCase.add(tabCaseLigne);
		}
		
		//D�finition du deuxi�me niveau de transparence, mise en place seulement si le fichier est conforme au premier niveau de transparence en terme d'hauteur et de largeur
		if(NB_CASE_LARGEUR == tabFic.get("CarteNiv2").getNbColonne() && NB_CASE_HAUTEUR == tabFic.get("CarteNiv2").getNbLigne()){
			for(int j = 0 ; j < NB_CASE_HAUTEUR ; j++){
				ArrayList<ACase> tabCaseLigne = new ArrayList<ACase>();
				for(int i = 0 ; i < NB_CASE_LARGEUR ; i++){
					numCaseChoisi = tabFic.get("CarteNiv2").getInfoInt(j, i);
					tabCaseLigne.add(new CaseStandard(numCaseChoisi, i, j, tabFic.get("Tile").getInfoInt(numCaseChoisi, 1), 1, this));
				}
				m_tabCaseNiveau2.add(tabCaseLigne);
			}
		}
		
		//D�finition des cases voisines  mise en place seulement si le fichier est conforme au premier niveau de transparence en terme d'hauteur et de largeur
		if(NB_CASE_HAUTEUR == tabFic.get("MapCaseVoisine").getNbLigne() && NB_CASE_LARGEUR == tabFic.get("MapCaseVoisine").getNbColonne()){
			System.out.println("Chargement des cases voisines en fonction du fichier texte");	
		
			int [] tabCoordo;
			//D�finition de la case de d�but et de la case de fin
			tabCoordo = tabFic.get("MapCaseVoisine").trouverInfoVal(Constantes.getInt("CASE_DEPART"));
			m_caseDeb = m_tabCase.get(tabCoordo[0]).get(tabCoordo[1]);
			
			tabCoordo = tabFic.get("MapCaseVoisine").trouverInfoVal(Constantes.getInt("CASE_FIN"));
			m_caseFin = m_tabCase.get(tabCoordo[0]).get(tabCoordo[1]);

			int [] tabDirec;
			ArrayList<String> tabDonnee;
			
			for(int cpt = 0 ; cpt < tabFic.get("TileCaseVoisine").getNbLigne() ; cpt++){
				tabDonnee = tabFic.get("TileCaseVoisine").getLigne(cpt);
				
				tabCoordo = tabFic.get("MapCaseVoisine").trouverInfoVal(Integer.parseInt(tabDonnee.get(0)));
				
				HashMap<Direction, ACase> caseVoisines = new HashMap<Direction, ACase>();
				HashMap<ACase, Direction> directionVoisines = new HashMap<ACase, Direction>();
				
				tabDirec = tabFic.get("MapCaseVoisine").trouverInfoVal(Integer.parseInt(tabDonnee.get(1)));
				if(tabDirec[0] != -1){
					caseVoisines.put(Direction.Gauche, m_tabCase.get(tabDirec[0]).get(tabDirec[1]));
					directionVoisines.put(m_tabCase.get(tabDirec[0]).get(tabDirec[1]), Direction.Gauche);
				}
			
				tabDirec = tabFic.get("MapCaseVoisine").trouverInfoVal(Integer.parseInt(tabDonnee.get(2)));
				if(tabDirec[0] != -1){
						caseVoisines.put(Direction.Droite, m_tabCase.get(tabDirec[0]).get(tabDirec[1]));
						directionVoisines.put(m_tabCase.get(tabDirec[0]).get(tabDirec[1]), Direction.Droite);
				}
				
				tabDirec = tabFic.get("MapCaseVoisine").trouverInfoVal(Integer.parseInt(tabDonnee.get(3)));
				if(tabDirec[0] != -1){
					caseVoisines.put(Direction.Haut, m_tabCase.get(tabDirec[0]).get(tabDirec[1]));
					directionVoisines.put(m_tabCase.get(tabDirec[0]).get(tabDirec[1]), Direction.Haut);
				}
				
				tabDirec = tabFic.get("MapCaseVoisine").trouverInfoVal(Integer.parseInt(tabDonnee.get(4)));
				if(tabDirec[0] != -1){
					caseVoisines.put(Direction.Bas, m_tabCase.get(tabDirec[0]).get(tabDirec[1]));
					directionVoisines.put(m_tabCase.get(tabDirec[0]).get(tabDirec[1]), Direction.Bas);
				}
				
				m_tabCase.get(tabCoordo[0]).get(tabCoordo[1]).setCaseVoisine(caseVoisines, directionVoisines);
			}
		}
		else{
			System.out.println("Chargement des cases voisines de fa�on automatique.");	
		
			//D�finition de la case de d�but et de la case de fin
			m_caseDeb = m_tabCase.get(0).get(0);
			m_caseFin = m_tabCase.get(NB_CASE_HAUTEUR - 1).get(NB_CASE_LARGEUR - 1);
		
			/**
			 * Initialisation des cases voisines
			 * De base, celle plac�s � cot� de la case actuelle
			 */
			for(int i = 0 ; i < m_tabCase.size() ; i++){
				for(int j = 0 ; j < m_tabCase.get(i).size() ; j++){
					HashMap<Direction, ACase> caseVoisines = new HashMap<Direction, ACase>();
					HashMap<ACase, Direction> directionVoisines = new HashMap<ACase, Direction>();
					
					if(j > 0){
						caseVoisines.put(Direction.Gauche, m_tabCase.get(i).get(j - 1));
						directionVoisines.put(m_tabCase.get(i).get(j - 1), Direction.Gauche);
					}
					if(j < m_tabCase.get(i).size() - 1){
						caseVoisines.put(Direction.Droite, m_tabCase.get(i).get(j + 1));
						directionVoisines.put(m_tabCase.get(i).get(j + 1), Direction.Droite);
					}
					if(i > 0){
						caseVoisines.put(Direction.Haut, m_tabCase.get(i - 1).get(j));
						directionVoisines.put(m_tabCase.get(i - 1).get(j), Direction.Haut);
					}
					if(i < m_tabCase.size() - 1){
						caseVoisines.put(Direction.Bas, m_tabCase.get(i + 1).get(j));
						directionVoisines.put(m_tabCase.get(i + 1).get(j), Direction.Bas);
					}
					
					m_tabCase.get(i).get(j).setCaseVoisine(caseVoisines, directionVoisines);
				}
			}
		}
	}
	
	/**
	 * Lancement du blocage de case
	 * @param uneCase
	 */
	public void startCaseCooldown(ACase uneCase)
	{
		uneCase.startBuildCooldown();
		m_caseEnCooldown.add(uneCase);
	}
	
	/**
	 * Test de fin de blocage de case
	 */
	public void cooldownSupervision()
	{
		ArrayList<ACase> caseTemp = new ArrayList<ACase>();
		for(ACase uneCase : m_caseEnCooldown)
		{
			if(uneCase.isReadyToBuild())
			{
				caseTemp.add(uneCase);
			}
		}
		m_caseEnCooldown.removeAll(caseTemp);
	}

	/**
	 * V�rifie si une case est bloqu�e
	 * @param uneCase
	 * @return
	 */
	public boolean isCaseInCooldown(final ACase uneCase)
	{
		return m_caseEnCooldown.contains(uneCase);
	}
	
	/**
	 * Fonction v�rifiant si une case poss�de des ennemis
	 * @param caseChoisi
	 * @return
	 */
	public Boolean haveEnnemi(final ACase caseChoisi){
		return caseChoisi.haveEnnemi();
	}
	
	/**
	 * Recup�re une coordonn�e et retourne la case correspondante
	 * @param posX
	 * @param posY
	 * @return
	 */
	public ACase trouverCasePixel(final float posX, final float posY){
		return calculValToCase(Outils.coordoToCase(posX), Outils.coordoToCase(posY));
	}
	
	/**
	 * Fonction permettant de retrouver une case en fonction de deux coordonn�es comprise entre 0 et MAX_CASE - 1
	 * @param posX
	 * @param posY
	 * @return
	 */
	public ACase trouverCasePixelInt(final int posX, final int posY){
		return calculValToCase(posX, posY);
	}
	
	/**
	 * Recup�re une coordonn�e et retourne la case correspondante
	 * @param posX
	 * @param posY
	 * @return
	 */
	private ACase calculValToCase(final int posX, final int posY){
		boolean trouver = false;
		int cptX = 0, cptY = 0;
		
		while(!trouver && cptY < m_tabCase.size()){
			cptX = 0;
			while(!trouver && cptX < m_tabCase.get(cptY).size()){
				
				if(m_tabCase.get(cptY).get(cptX).getPosition().x == posX && m_tabCase.get(cptY).get(cptX).getPosition().y == posY){
					trouver = true;
				}
				
				cptX++;
			}
			cptY++;
		}
		
		if(trouver){
			cptX--;
			cptY--;
			
			return m_tabCase.get(cptY).get(cptX);
		}
		return null;
	}
		
	public int getNbCaseLargeur(){
		return NB_CASE_LARGEUR;
	}
	
	public int getNbCaseHauteur(){
		return NB_CASE_HAUTEUR;
	}
	
	public ArrayList<ArrayList<ACase>> getCase(){
		return m_tabCase;
	}

	public ACase getCase(final int numX, final int numY){
		if(numY >= 0 && numY < m_tabCase.size()){
			if(numX >= 0 && numX < m_tabCase.get(numY).size()){
				return m_tabCase.get(numY).get(numX);
			}
		}
		return null;
	}
	
	public ArrayList<ArrayList<ACase>> getCaseNiv2(){
		return m_tabCaseNiveau2;
	}
	
	public ArrayList<ACase> getCaseEnCooldown(){
		return m_caseEnCooldown;
	}
	
	public ACase getCaseDeb(){
		return m_caseDeb;
	}

	public ACase getCaseFin(){
		return m_caseFin;
	}
	
}





