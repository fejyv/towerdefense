package model.tir.pattern;


public class PatternTirStandard extends APatternTir {
	
	public PatternTirStandard(final int degat, final double cadence) {
		super(degat, cadence);
	}

	@Override
	public PatternTirStandard getNewInstance() {
		
		return new PatternTirStandard(this.degat, this.cadence);
	}
}
