package model.tir.pattern;


public abstract class APatternTir {
	
	protected int degat;
	protected double cadence;

	public APatternTir(final int degat, final double cadence) {
		setDegat(degat);
		setCadence(cadence);
	}
	

	public abstract APatternTir getNewInstance();
	
	// Getters & Setters
	public int getDegat() {
		return degat;
	}
	public void setDegat(final int degat) {
		this.degat = degat;
	}
	
	public double getCadence() {
		return cadence;
	}
	public void setCadence(final double cadence) {
		this.cadence = cadence;
	}
}
