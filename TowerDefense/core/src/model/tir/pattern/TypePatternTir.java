package model.tir.pattern;


public enum TypePatternTir {
	TirStandard(0, "PatternTirStandard");

	private int num;
	private String nomClasse;
   
	TypePatternTir(final int num, final String nomClasse){
		this.num = num;
		this.nomClasse = nomClasse;
	}
   
	public int getNum(){
		return this.num;
	}
	
	public String getNomClasse() {
		return this.nomClasse;
	}
	
	public boolean equals(final int num){
		return (this.num == num);
	}
}
