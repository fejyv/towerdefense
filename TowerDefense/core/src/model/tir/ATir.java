package model.tir;

import com.badlogic.gdx.math.Vector2;

import model.batiment.tour.ATour;
import model.gestionPosition.AGestionPositionTir;
import model.monstre.AMonstreCarte;
import typeElement.IDeplacable;

public abstract class ATir implements IDeplacable {
	
	
	protected AGestionPositionTir gestionPosition;
	protected ATour tour;
	protected int numType;
	
	public ATir(final int numType, final AGestionPositionTir gestionPosition) {
	
		this.gestionPosition= gestionPosition;
		this.numType = numType;
	}
	
	public abstract ATir getNewInstance();
	
	
	public ATour getTour() {
		return tour;
	}
	
	public int getTypeNum(){
		return numType;
	}
	
	public void setTour(final ATour tour){
		this.tour = tour;
	}
	
	public AGestionPositionTir getGestionPos(){
		return gestionPosition;
	}

	public void setGestionPosition(final AGestionPositionTir gestionPosition) {
		this.gestionPosition = gestionPosition;
	}
	
	/**
	 * V�rifie si le tir touche un monstre ou si celui-ci � atteint la case qu'il d�signait
	 */
	public boolean estMort(){
		boolean mort = false;
		
		if(gestionPosition.disparaitre()){
			mort = true;
		}
		else{
			for (AMonstreCarte monstre : gestionPosition.getCase().getEnnemi()) {
				if(monstre != null && !mort){
					
					if(monstre.getGestionPos().getRect().intersects(gestionPosition.getRect())){
						
						try{
							monstre.blesser(tour.getModelPatternTir().getDegat());
						}
						catch(java.lang.NullPointerException ex){
							System.out.println(ex + " Tour null");
						}
						
						mort = true;
					}
				}
			}
		}
		return mort;
	}
	
	public void disparaitre()	{
	}

	public Vector2 getPositionPixel()
	{
		return gestionPosition.getPixelPosition();
	}

	
}
