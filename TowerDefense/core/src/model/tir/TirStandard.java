package model.tir;

import model.gestionPosition.AGestionPositionTir;

public class TirStandard extends ATir {
	
	public TirStandard(final int numType, final AGestionPositionTir gestionPosition) {
		super(numType, gestionPosition);
	}

	@Override
	public TirStandard getNewInstance() {
		
		return new TirStandard(this.numType, this.gestionPosition);
	}

	
	public void deplacer()
	{
		gestionPosition.bouger();
	}
	
}
