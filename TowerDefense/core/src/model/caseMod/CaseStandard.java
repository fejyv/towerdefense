package model.caseMod;

import model.Carte;

/**
 * 
 * @author Florian
 *
 */
public class CaseStandard extends ACase{
	
	public CaseStandard(final int num, final int posX, final int posY, final int valeurPassage, final double tempsMax, final Carte carte){
		super(num, posX, posY, valeurPassage, tempsMax, carte);
	}
}
