
package model.caseMod;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.HashMap;

import com.badlogic.gdx.math.Vector2;
import model.monstre.AMonstreCarte;
import outil.Clock;
import outil.Constantes;
import outil.Direction;
import outil.Outils;
import model.Carte;

/**
 * 
 * @author Florian
 *
 */
public abstract class ACase {
	
	protected Clock cooldownConstruction;
	
	protected Vector2 m_position;
	protected Rectangle m_rect;
	protected ArrayList<AMonstreCarte> m_tabMonstre;

	protected double m_tempsMax; 
	protected int m_valeurPassage;
	protected Carte m_carte;
	
	protected int m_num;
	
	protected HashMap<Direction, ACase> m_tabCaseVoisine;
	protected HashMap<ACase, Direction> m_tabDirectionVoisine;
	
	/**
	 * Constructeur de case
	 * @param posX
	 * @param posY
	 * @param tempsMax
	 * @param carte
	 */
	public ACase(final int num, final int posX, final int posY, final int valeurPassage, final double tempsMax, final Carte carte){
		m_tabMonstre = new ArrayList<AMonstreCarte>();
		
		m_position = new Vector2(posX, posY);
		m_rect = new Rectangle((posX) * Constantes.getInt("TAILLE_IMAGE"), (int)Outils.getCoordoY((posY + 1) * Constantes.getInt("TAILLE_IMAGE")), Constantes.getInt("TAILLE_IMAGE"), Constantes.getInt("TAILLE_IMAGE"));
				
		m_tabCaseVoisine = new HashMap<Direction, ACase>();
		m_tabDirectionVoisine = new HashMap<ACase, Direction>();
		
		
		m_tempsMax = tempsMax;
		m_carte = carte;
		//de 1 � 5 => 1 le plus rapide, 5 le plus lent
		m_valeurPassage = valeurPassage; 
		m_num = num;
		cooldownConstruction = new Clock();

	}
	
	public void setCaseVoisine(final HashMap<Direction, ACase> caseVoisines, final HashMap<ACase, Direction> directionVoisines){
		m_tabCaseVoisine = caseVoisines;
		m_tabDirectionVoisine = directionVoisines;
	}
	
	public void ajoutMonstre(final AMonstreCarte newMonstre){
		m_tabMonstre.add(newMonstre);
	}
	
	public void supprMonstre(final AMonstreCarte newMonstre){
		m_tabMonstre.remove(newMonstre);
	}
	
	public Boolean haveEnnemi(){
		return !m_tabMonstre.isEmpty();
	}
	
	public int getNum(){
		return m_num;
	}
	
	public Vector2 getPosition(){
		return m_position;
	}
	
	public Rectangle getRect(){
		return m_rect;
	}
	
	public ArrayList<AMonstreCarte> getEnnemi(){
		return m_tabMonstre;
	}
	
	public double getTempsMax(){
		return m_tempsMax;
	}
	
	public Carte getCarte(){
		return m_carte;
	}
	
	public int getValeurPassage(){
		return m_valeurPassage;
	}
	
	public ACase getCaseVoisine(final Direction directionVoulu){
		return m_tabCaseVoisine.get(directionVoulu);
	}
	
	public Direction getDirection(final ACase caseVoisine){
		return m_tabDirectionVoisine.get(caseVoisine);
	}
	
	/**
	 * On v�rifie si la case contient une case voisine dans la direction souhait�
	 * @param directionVoulu
	 * @return
	 */
	public Boolean directContains(final Direction directionVoulu){
		return m_tabCaseVoisine.containsKey(directionVoulu);
	}
	
	public boolean isReadyToBuild()
	{
		if(cooldownConstruction.getElapsedTimeSecond() > Constantes.getInt("COOLDOWN"))
		{
			return true;
		}
		
		return false;
	}
	
	public void startBuildCooldown()
	{
		cooldownConstruction.restart();
	}
}
