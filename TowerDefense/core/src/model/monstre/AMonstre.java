
package model.monstre;

import java.io.Serializable;

import stats.Stats;

/**
 * 
 * @author julien.rembinski
 *
 */
@SuppressWarnings("serial")
public abstract class AMonstre implements Serializable{
	protected String nomMonstre; 
	protected int numLigneImage;
	//le vie du mob quand il est full life
	protected int vieMax;
	//la vie restante du monstre
	protected int vie;
	//le d�gats qu'inflige le monstre au joueur
	protected int degats;
	//l'argent gagn� lors de la mort du mob
	protected int valeur;
	//le multiplicateur de temps pass� sur une case. Plus il est grand, plus le monstre est lent
	protected double coefTempsParCase;
	
	protected int score;
	protected int prix;
	
		
	public AMonstre(final int numImage, final int vieMax, final int degats, final int valeur, final double coefTempsParCase, final int score, final int prix)
	{
		this.numLigneImage = numImage;
		this.vieMax = vieMax;
		this.vie = vieMax;
		this.valeur = valeur;
		this.degats = degats;
		this.coefTempsParCase = coefTempsParCase;
		this.score = score;
		this.prix = prix;
	}
	
	public int getPrix() {
		return prix;
	}

	public void setPrix(int prix) {
		this.prix = prix;
	}

	public AMonstre(final AMonstre mob)
	{
		this.numLigneImage = mob.getTypeNum();
		this.vieMax = mob.getVieMax();
		this.vie = mob.getVieMax();
		this.valeur = mob.getValeur();
		this.degats = mob.getDegats();
		this.coefTempsParCase = mob.getCoefTempsParCase();
		this.score = mob.getScore();
	}
	
	
	public int getVieMax(){
		return vieMax;
	}
	
	public double getCoefTempsParCase(){
		return coefTempsParCase;
	}
	
	public int getDegats()
	{
		return degats;
	}

	public int getScore()
	{
		return score;
	}
	
	public int getValeur()
	{
		return valeur;
	}
	
	
	/**
	 * Inflige les d�g�ts pass�s en param�tre au monstre
	 * @param degatsSubis
	 */
	public void blesser(final int degatsSubis)
	{
		this.vie -= degatsSubis;
		if(vie < 0){
			vie = 0;
		}
		//System.out.println(vie);
	}
	
	public void soigner(final int valeurSoin)
	{
		this.vie += valeurSoin;
		if(vie > vieMax){
			vie = vieMax;
		}
	}
	
	/**
	 * indique si les pdv du monstre sont <= 0
	 * @return
	 */
	public boolean estMort()
	{
		if(vie <= 0)
		{
			Stats.mortMonstre();
			return true;
		}
		return false;
	}
	
	public int getTypeNum(){
		return numLigneImage;
	}
}