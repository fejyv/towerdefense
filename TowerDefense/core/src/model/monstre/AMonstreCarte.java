package model.monstre;

import com.badlogic.gdx.math.Vector2;

import model.caseMod.ACase;
import model.gestionPosition.AGestionPositionMonstre;
import outil.Direction;
import typeElement.IAvecVie;
import typeElement.IDeplacable;

@SuppressWarnings("serial")
public abstract class AMonstreCarte extends AMonstre implements IAvecVie, IDeplacable{

	protected AGestionPositionMonstre gestionPosition;
	
	public AMonstreCarte(final int numImage, final int vieMax, final int degats, final int valeur, final double coefTempsParCase, final int score, final AGestionPositionMonstre gestionPosition, final int prix)
	{
		super(numImage, vieMax, degats, valeur, coefTempsParCase, score, prix);
		this.gestionPosition = gestionPosition;
		this.gestionPosition.initMonstre(this);
	}
	
	public AMonstreCarte(final AMonstre mob, final AGestionPositionMonstre gestionPosition){
		super(mob);
		this.gestionPosition = gestionPosition;
		this.gestionPosition.initMonstre(this);
	}

	public Direction getDirection()
	{
		return gestionPosition.getDirection();
	}


	public ACase getCase()
	{
		return gestionPosition.getCase();
	}
	
	public void setGestionPos(AGestionPositionMonstre newPos){
		gestionPosition = newPos;
	}

	public AGestionPositionMonstre getGestionPos(){
		return gestionPosition;
	}
	
	public int getTailleChemin()
	{
		return gestionPosition.getTailleChemin();
	}
	
	public void disparaitre()
	{
		gestionPosition.disparaitre();
	}
	
	public Vector2 getPositionPixel()
	{
		return gestionPosition.getPixelPosition();
	}
	
	public double getVitesse()
	{
		int coefCase = gestionPosition.getCase().getValeurPassage();
		
		return coefTempsParCase*coefCase;
	}
	
	@Override
	public float getPoucentageVie() {
		float pourcent = (float)vie/(float)vieMax;
		return pourcent;
	}
}
