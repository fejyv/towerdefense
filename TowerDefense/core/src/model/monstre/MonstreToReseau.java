package model.monstre;

@SuppressWarnings("serial")
public class MonstreToReseau extends AMonstre {
					
	public MonstreToReseau(final int numImage, final int vieMax, final int degats, final int valeur, final double coefTempsParCase, final int score, final int prix)
	{
		super(numImage, vieMax, degats, valeur, coefTempsParCase, score, prix);
	}
	
	public MonstreToReseau(AMonstre mob)
	{
		super(mob);
	}
}
