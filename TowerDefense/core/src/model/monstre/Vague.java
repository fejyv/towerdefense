package model.monstre;

import java.io.Serializable;
import java.util.ArrayList;

import typeElement.IDeplacable;

/**
 * 
 * @author Florian
 *
 */
@SuppressWarnings("serial")
public class Vague implements Serializable {
	
	private ArrayList<AMonstre> tabMonstre;
	
	public Vague(){
		tabMonstre = new ArrayList<AMonstre>();
	}
	
	public void add(AMonstre newMob){
		tabMonstre.add(newMob);
	}
	
	public void del(int num){
		if(num >= 0 && num < tabMonstre.size()){
			tabMonstre.remove(num);
		}
	}
	
	public void del(AMonstre mob){
		tabMonstre.remove(mob);
	}
	
	public void delAll(ArrayList<IDeplacable> listMob){
		tabMonstre.removeAll(listMob);
	}
	
	public ArrayList<AMonstre> get(){
		return tabMonstre;
	}
	
	public void clear(){
		tabMonstre.clear();
	}
}
