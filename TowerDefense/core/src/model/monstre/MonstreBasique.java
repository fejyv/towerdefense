package model.monstre;

import model.gestionPosition.AGestionPositionMonstre;

/**
 * 
 * @author julien.rembinski
 *
 */
@SuppressWarnings("serial")
public class MonstreBasique extends AMonstreCarte {

	public MonstreBasique(final int numImage, final int vieMax, final int degats, final int valeur, final double vitesse, final int score, final AGestionPositionMonstre gestionPosition, final int prix) {
		super(numImage, vieMax, degats, valeur, vitesse, score, gestionPosition, prix);
	}
	
	public MonstreBasique(final AMonstre mob, final AGestionPositionMonstre gestionPosition){
		super(mob, gestionPosition);
	}


	/**
	 * d�l�gue le mouvement � la gestionPositionMonstre
	 */
	public void deplacer()
	{
		if(!estMort())
		{
			gestionPosition.bouger();
		}
	}


}
