package model.joueur;

import outil.Constantes;

public class JoueurReel implements IJoueur{	
	private int pointsDeVie;
	private int score = 0;
	private int argent;
	
	public JoueurReel(final int pointsDeVie, final int argentDeDepart)
	{
		this.pointsDeVie = pointsDeVie;
		this.argent = argentDeDepart;
	}
	
	
	public int getPointsDeVie() {
		return this.pointsDeVie;
	}
	
	public void afficherArgent()
	{
		System.out.println("argent du joueur : "+argent);
	}
	
	public void afficherVie()
	{
		System.out.println("vie du joueur : "+pointsDeVie);
	}
	
	public void perdreDesPointsDeVie(final int nbPV) {
		if(this.pointsDeVie - nbPV < 0) {
			this.pointsDeVie = 0;
		} else {
			this.pointsDeVie -= nbPV; 
		}
	}

	public void gagnerDesPointsDeVie(final int nbPV) {
		if(this.pointsDeVie + nbPV >= Constantes.getInt("POINTS_DE_VIE_MAX")) {
			this.pointsDeVie = Constantes.getInt("POINTS_DE_VIE_MAX");
		} else {
			this.pointsDeVie += nbPV;
		}
	}
	

	// Sert d'interface pour arreter la partie
	public void mourir() {
				
	}
	

	public int getScore() {
		return this.score;
	}

	public void gagnerScore(final int score) {
		if(this.score + score >= Constantes.getInt("SCORE_MAX")) {
			this.score = Constantes.getInt("SCORE_MAX");
		} else {
			this.score += score;
		}
	}

	public void perdreScore(final int score) {
		if(this.score - score <= 0) {
			this.score = 0;
		} else {
			this.score -= score;
		}
	}

	

	public int getArgent() {
		return this.argent;
	}

	public void gagnerArgent(final int argent) {
		if(this.argent + argent >= Constantes.getInt("ARGENT_MAX")) {
			this.argent = Constantes.getInt("ARGENT_MAX");
		} else {
			this.argent += argent;
		}
	}

	public void perdreArgent(int argent) {
		if(this.argent - argent >= 0) {
			this.argent -= argent;
		} else {
			this.argent = 0;
		}
	}
	
	public boolean estMort()
	{
		return this.pointsDeVie == 0;
	}


	@Override
	public boolean verificationPossedeAssezArgentPourAcheter(final int argent) {
		if(this.argent - argent >= 0) {
			return true;
		}
		return false;
	}

}
