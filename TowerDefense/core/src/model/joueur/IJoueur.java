/**
 * 
 */
package model.joueur;

/**
 * @author victor.michaux
 *
 */
public interface IJoueur {
	
	/*
	 * Fonctions relatives aux points de vie du joueur
	 */
	public int getPointsDeVie();
	public void perdreDesPointsDeVie(int nbPV);
	public void gagnerDesPointsDeVie(int nbPV);
	public boolean estMort();
	
	/*
	 * Quand le joueur n'a plus de point de vie il meurt
	 */
	public void mourir();
	
	/*
	 * Fonctions relatives au score du joueur
	 */
	public int getScore();
	public void gagnerScore(int score);
	public void perdreScore(int score);
	
	/*
	 * Fonctions relatives � l'argent du joueur
	 */
	public int getArgent();
	public void gagnerArgent(int argent);
	public void perdreArgent(int argent);
	public boolean verificationPossedeAssezArgentPourAcheter(int argent);
	
	
}
