package model.gestionPosition;

import com.badlogic.gdx.math.Vector2;
import model.caseMod.ACase;
import model.monstre.AMonstre;

public class GestionPositionTirSuivre extends AGestionPositionTir{

	AMonstre monstreASuivre;
	protected Vector2 positionFinal;
	final Vector2 pointFixe = new Vector2();
	
	public GestionPositionTirSuivre(final ACase caseChoisi, final Vector2 posDeb, final AMonstre unMonstre, final int vitesse){
		super(caseChoisi, posDeb, vitesse);
		monstreASuivre = unMonstre;
	}


	@Override
	public void bouger() {
		
	}
	
	/*@Override
	public void bouger(Vector2 positionMonstre) {
		// TODO Auto-generated method stub
		if ( null != monstreASuivre ){
			positionFinal = monstreASuivre.getPositionPixel();
		}
		
		float tempsParSecond =  (float) (temps.getElapsedTimeSecond());
		float vitesse = 5;
		
		this.pointPourAxeTour = new Vector2(tir.getTour().getCase().getPosition().x,tir.getTour().getCase().getPosition().y+10);
		Vector2 pointTour = new Vector2(tir.getTour().getCase().getPosition().x,tir.getTour().getCase().getPosition().y);
		double m_rotationJoueur;
		double distanceA = (Math.sqrt(Math.pow(positionMonstre.x - pointPourAxeTour.x,2) + Math.pow(positionMonstre.y - pointPourAxeTour.y, 2))); //distance monstre pointHautTour
		double distanceB = (Math.sqrt(Math.pow(pointTour.x - pointPourAxeTour.x,2) + Math.pow(pointTour.y - pointPourAxeTour.y, 2))); //distance tour origine pointHautTour
		double distanceC = (Math.sqrt(Math.pow(pointTour.x - positionMonstre.x,2) + Math.pow(pointTour.y - positionMonstre.y, 2))); //distance tour origine monstre
		double AlKashiNum = -(Math.pow(distanceA,2))+(Math.pow(distanceB,2)) + (Math.pow(distanceC,2));
		double AlKashiDen = 2*distanceB*distanceC;
		if(positionMonstre.x>pointTour.x){
			m_rotationJoueur = AlKashiNum/AlKashiDen;
		}
		else
		{
			m_rotationJoueur = -(AlKashiNum/AlKashiDen);
		}*/
		/*
		 * Copie des sourisSuivre
		if ( distanceTotal > distanceAParcourir ){
			 float newX,newY;
			 if (positionPixel.x < positionFinal.x){
				 newX = positionPixel.x + vitesse * tempsParSecond;
			 }
			 else{
				 newX = positionPixel.x - vitesse * tempsParSecond;
			 }
				 
			 if ( positionPixel.y < positionFinal.y){
				 newY =  positionPixel.y + vitesse * tempsParSecond;
			 }
			 else {
				 newY =  positionPixel.y - vitesse * tempsParSecond;
			 }
			 positionPixel = new Vector2(newX,newY);
			 
		}
		
		positionPixel = new Vector2((float)Math.sin((m_rotationJoueur * M_PI) / 180) * vitesse * tempsParSecond,(float) Math.cos((m_rotationJoueur * M_PI) / 180) * -vitesse * tempsParSecond);
		
	}*/

	@Override
	public void calculVitesse(Vector2 posDest) {
		// TODO Auto-generated method stub
		
	}




}