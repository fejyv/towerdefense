
package model.gestionPosition;

import com.badlogic.gdx.math.Vector2;

import model.caseMod.ACase;


public abstract class AGestionPosition  {
	protected ACase caseCourante;
	
	public AGestionPosition(final ACase caseCourante) {
		this.caseCourante = caseCourante;
	}
	
	public ACase getCase()
	{
		return caseCourante;
	}
	public abstract Vector2 getPixelPosition();
	
	
}
