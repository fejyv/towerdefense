package model.gestionPosition;

import com.badlogic.gdx.math.Vector2;

import model.caseMod.ACase;

public class GestionPositionTirStandard extends AGestionPositionTir{
	
	public GestionPositionTirStandard(final ACase caseChoisi, final Vector2 posDeb, final int vitesse) {
		super(caseChoisi, posDeb, vitesse);
		// TODO Auto-generated constructor stub
	}
	
	public GestionPositionTirStandard(final ACase caseChoisi, final Vector2 posDeb, final int vitesse, final Vector2 posDest) {
		super(caseChoisi, posDeb, vitesse, posDest);
	}
	

	@Override
	public void calculVitesse(final Vector2 posDest) {
		
		float x = posDest.x - positionPixel.x;
		float y = posDest.y - positionPixel.y;
		
		boolean posiX = true;
		boolean posiY = true;
		
		if(x < 0){
			posiX = false;
			x = positionPixel.x - posDest.x;
		}
		if(y < 0){
			posiY = false;
			y = positionPixel.y - posDest.y;
		}
		
		float vitX = (x * vitesse) / (x + y);
		float vitY = (y * vitesse) / (x + y);
		
		if(!posiX){
			vitX = -vitX;
		}
		if(!posiY){
			vitY = -vitY;
		}
		
		vitesseDep.x = vitX;
		vitesseDep.y = vitY;
		
		calculRotation(posDest);
		
		//vitesseDepOld.x = vitX;
		//vitesseDepOld.y = vitY;
	}

	@Override
	public void bouger() {
		positionPixel.x += vitesseDep.x;
		positionPixel.y += vitesseDep.y;
	}

	/*@Override
	public void bouger(Vector2 directionTir) {
		// TODO Auto-generated method stub
		float tempsParSecond =  (float) (temps.getElapsedTimeSecond());
		float vitesse = 5;
		
		this.pointPourAxeTour = new Vector2(tir.getTour().getCase().getPosition().x,tir.getTour().getCase().getPosition().y+10);
		Vector2 pointTour = new Vector2(tir.getTour().getCase().getPosition().x,tir.getTour().getCase().getPosition().y);
		
		double distanceA = (Math.sqrt(Math.pow(directionTir.x - pointPourAxeTour.x,2) + Math.pow(directionTir.y - pointPourAxeTour.y, 2))); //distance monstre pointHautTour
		double distanceB = (Math.sqrt(Math.pow(pointTour.x - pointPourAxeTour.x,2) + Math.pow(pointTour.y - pointPourAxeTour.y, 2))); //distance tour origine pointHautTour
		double distanceC = (Math.sqrt(Math.pow(pointTour.x - directionTir.x,2) + Math.pow(pointTour.y - directionTir.y, 2))); //distance tour origine monstre
		double AlKashiNum = -(Math.pow(distanceA,2))+(Math.pow(distanceB,2)) + (Math.pow(distanceC,2));
		double AlKashiDen = 2*distanceB*distanceC;
		if(directionTir.x>pointTour.x){
			m_rotationTir = AlKashiNum/AlKashiDen;
		}
		else
		{
			m_rotationTir = -(AlKashiNum/AlKashiDen);
		}

		positionPixel = new Vector2((float)Math.sin((m_rotationTir * M_PI) / 180) * vitesse * tempsParSecond,(float) Math.cos((m_rotationTir * M_PI) / 180) * -vitesse * tempsParSecond);
		
	}*/





}
