package model.gestionPosition;


import java.awt.Rectangle;

import com.badlogic.gdx.math.Vector2;

import model.caseMod.ACase;
import outil.Constantes;


public abstract class AGestionPositionTir extends AGestionPosition{

	
	protected Vector2 positionPixel;
	protected Vector2 vitesseDep;
	protected int vitesse;
	
	protected boolean entrerDansCase;
	protected boolean sortiDeCase;

	protected final double M_PI = Math.PI;
	protected float m_rotationTir;

	public AGestionPositionTir(final ACase caseChoisi, final Vector2 posDeb, final int vitesse) {
		super(caseChoisi);
		this.positionPixel=posDeb;

		this.vitesse = vitesse;
		vitesseDep = new Vector2(0, -vitesse);
		m_rotationTir = 0;
		
		entrerDansCase = false;
		sortiDeCase = false;
	}
	
	public AGestionPositionTir(final ACase caseChoisi, final Vector2 posDeb, final int vitesse, final Vector2 posDest) {
		super(caseChoisi);

		this.positionPixel=posDeb;

		this.vitesse = vitesse;
		vitesseDep = new Vector2(0, -vitesse);
		m_rotationTir = 0;
		calculVitesse(posDest);
		
		entrerDansCase = false;
		sortiDeCase = false;
	}
	
	public void setEntrerCase(final boolean val){
		entrerDansCase = val;
	}
	
	public void setSortiCase(final boolean val){
		sortiDeCase = val;
	}
	
	public boolean getEntrerCase(){
		return entrerDansCase;
	}
	
	public boolean getSortiCase(){
		return sortiDeCase;
	}
	
	public float getRotationTir() {
		return m_rotationTir;
	}
	
	public Vector2 getPixelPosition(){
		return positionPixel;
	}
	
	public Rectangle getRect(){
		return new Rectangle((int)positionPixel.x, (int)positionPixel.y, Constantes.getInt("TAILLE_IMAGE"), Constantes.getInt("TAILLE_IMAGE"));
	}
	
	public Vector2 getVitesseDep(){
		return vitesseDep;
	}
	
	public int getVitesse(){
		return vitesse;
	}

	public void setM_rotationTir(final float m_rotationTir) {
		this.m_rotationTir = m_rotationTir;
	}
	
	public boolean disparaitre(){
		if(caseCourante.getRect().intersects(this.getRect())){
			
			if(!entrerDansCase){
				entrerDansCase = true;
			}
		}
		else{
			if(entrerDansCase){
				sortiDeCase = true;
			}
		}
		
		return sortiDeCase;
	}
	
	public abstract void calculVitesse(final Vector2 posDest);
	
	protected void calculRotation(final Vector2 posDest){
		/*Vector2 posOld = new Vector2(positionPixel.x + vitesseDep.x, positionPixel.y + vitesseDep.y);
        Vector2 posNew = new Vector2(positionPixel.x + vitesseDepOld.x, positionPixel.y + vitesseDepOld.y);

        double PO = Math.pow(positionPixel.x - posOld.x, 2) + Math.pow(positionPixel.y - posOld.y, 2);
        double PN = Math.pow(positionPixel.x - posNew.x, 2) + Math.pow(positionPixel.y - posNew.y, 2);
        double ON = Math.pow(posOld.x - posNew.x, 2) + Math.pow(posOld.y - posNew.y, 2);

        double cosOPN = (PO + PN - ON) / (2 * Math.sqrt(PO) * Math.sqrt(PN));

        m_rotationTir = (float) ((cosOPN / M_PI * 180) * 2);
        //m_rotationTir += 90;
        //System.out.println(m_rotationTir);*/
		
		//Comment �a fonctionne ?
		
        float valX = posDest.x - positionPixel.x;
        float valY = posDest.y - positionPixel.y;

        if(valX > valY)
        {
            if(valX > 0)
            {
                valY /= valX;
                valX /= valX;
            }
            else if(valX < 0)
            {
                valY /= -valX;
                valX /= -valX;
            }
        }
        else
        {
            if(valY > 0)
            {
                valX /= valY;
                valY /= valY;
            }
            else if(valY < 0)
            {
                valX /= -valY;
                valY /= -valY;
            }
        }

        m_rotationTir = (float)(Math.pow(valX, 2) + Math.pow(valY, 2));

        if(m_rotationTir != 0)
        {
        	m_rotationTir = (float)(valX / Math.sqrt(m_rotationTir));

        	m_rotationTir = (float)Math.acos(m_rotationTir);


            if(valY < 0)
            {
            	m_rotationTir = -(float)((m_rotationTir * (360 / (2*M_PI))) + 90);
            }
            else
            {
            	m_rotationTir = (float)(m_rotationTir * (360 / (2*M_PI))) - 90;
            }
        }
	}
	
	public abstract void bouger() ;
}
