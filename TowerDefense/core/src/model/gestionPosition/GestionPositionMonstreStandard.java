
package model.gestionPosition;


import java.awt.Rectangle;
import java.util.ArrayList;

import com.badlogic.gdx.math.Vector2;

import jeu.AJeu;
import model.caseMod.ACase;
import outil.Clock;
import outil.Constantes;
import outil.Dijkstra;
import outil.Direction;
import outil.Outils;

/**
 * 
 * @author julien.rembinski
 *
 */
public class GestionPositionMonstreStandard extends AGestionPositionMonstre {
	//permet d'�valuer le temps pass� sur la case courante
	private Clock temps;
	private Direction entree;
	private Direction sortie;
	private Vector2 coordPixel;
	

	public GestionPositionMonstreStandard(final ACase caseCourante) {
		super(caseCourante);
		this.temps = new Clock();
		entree = Direction.Gauche;
		coordPixel = Outils.getCoordObjMilieu(Outils.caseToCoordo(caseCourante));
	}

	public void bouger() {
		
		double tempsMaxReel = caseCourante.getTempsMax()*monstre.getVitesse();
		//On ne change de case que si le temps max sur la case courante est d�pass�
		if(temps.getElapsedTimeSecond() > tempsMaxReel && !chemin.isEmpty())
		{
			caseCourante.supprMonstre(this.monstre);
			//on change la case courante par la case suivante sur le chemin
			entree = chemin.get(0).getDirection(caseCourante);
			caseCourante = chemin.get(0);
			caseCourante.ajoutMonstre(this.monstre);
			//on enl�ve la nouvelle case courante du chemin
			chemin.remove(0);
			temps.restart();
		}
		if(!chemin.isEmpty())
		{
			sortie = caseCourante.getDirection(chemin.get(0));
		}
		coordPixel = Outils.getCoordObjMilieu(Outils.caseToCoordo(caseCourante));
		
		double percent = temps.getElapsedTimeSecond()/tempsMaxReel;
		Direction dir = getDirection();
		switch(dir)
		{
			case Bas:
				coordPixel.y -= 32*percent;
				coordPixel.x += 16;
				break;
			case Haut:
				coordPixel.y += 32*percent - 32;
				coordPixel.x += 16;
				break;
			case Gauche:
				coordPixel.x -= 32*percent - 32;
				coordPixel.y -= 16;
				break;
			case Droite:
				coordPixel.x += 32*percent;
				coordPixel.y -= 16;
				break;
		}
	}
	
	public Direction getDirection()
	{
		Direction direction = null;
		double tempsMaxReel = caseCourante.getTempsMax()*monstre.getVitesse();
		double percent = temps.getElapsedTimeSecond()/tempsMaxReel;
		if(percent < 0.5)
		{
			switch(entree)
			{
				case Bas:
					direction = Direction.Haut;
					break;
				case Haut:
					direction = Direction.Bas;
					break;
				case Gauche:
					direction = Direction.Droite;
					break;
				case Droite:
					direction = Direction.Gauche;
					break;
			}
		}
		else
		{
			direction = sortie;
		}
		return direction;
		
	}

	@Override
	public Vector2 getPixelPosition() 
	{
		return coordPixel;
	}
	
	public Rectangle getRect(){
		return new Rectangle((int)coordPixel.x, (int)coordPixel.y, Constantes.getInt("TAILLE_IMAGE"), Constantes.getInt("TAILLE_IMAGE"));
	}
	
	@Override
	public Boolean calculerChemin(AJeu game) {		
		ArrayList<ACase> chemin = Dijkstra.calculDijkstra(game, caseCourante); 
		if(chemin != null){
			this.chemin = chemin;
			return true;
		}
		else{
			return false;
		}
	}

}