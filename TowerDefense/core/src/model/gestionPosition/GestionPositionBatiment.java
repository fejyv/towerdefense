package model.gestionPosition;

import com.badlogic.gdx.math.Vector2;

import model.caseMod.ACase;
import outil.Outils;

public class GestionPositionBatiment extends AGestionPosition {

	public GestionPositionBatiment(final ACase caseCourante) {
		super(caseCourante);
	}

	@Override
	public Vector2 getPixelPosition() {
		return Outils.caseToCoordo(caseCourante);
	}

}
