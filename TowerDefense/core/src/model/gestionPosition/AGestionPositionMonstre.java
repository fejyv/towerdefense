
package model.gestionPosition;

import java.awt.Rectangle;
import java.util.ArrayList;

import com.badlogic.gdx.math.Vector2;

import jeu.AJeu;
import model.caseMod.ACase;
import model.monstre.AMonstreCarte;
import outil.Direction;
import outil.Outils;

/**
 * 
 * @author julien.rembinski
 *
 */
public abstract class AGestionPositionMonstre extends AGestionPosition{
	//le chemin entre la case courante et la case finale
	protected ArrayList<ACase> chemin;
	protected AMonstreCarte monstre;
	protected Vector2 positionPixel;
	
	public AGestionPositionMonstre(final ACase caseCourante) {
		super(caseCourante);
		positionPixel = Outils.getCoordObjMilieu(new Vector2(0, Outils.getCoordoY(0)));
				
	}
	
	public int getTailleChemin()
	{
		return(chemin.size());
	}
	
	public void disparaitre()
	{
		caseCourante.supprMonstre(monstre);
	}
	
	public void initMonstre(final AMonstreCarte aMonstre)
	{
		this.monstre = aMonstre;
		caseCourante.ajoutMonstre(this.monstre);
	}
	
	
	public abstract Direction getDirection();
	public abstract Rectangle getRect();

	/**
	 * D�place le monstre d'une case � l'autre en suivant le chemin
	 */
	public abstract void bouger();
	
	/**
	 * calcule le chemin que doit emprunter le monstre
	 */
	public abstract Boolean calculerChemin(AJeu game);

}