#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <windows.h>
#include <unistd.h>
using namespace std;

std::string int_to_string(int val)
{
    std::stringstream ss;
    ss << val;
    std::string str = ss.str();
    return str;
}

int main()
{
    int nbLargeur, nbHauteur;

    cout << "Nb largeur : ";
    cin >> nbLargeur;

    cout << "Nb hauteur : ";
    cin >> nbHauteur;

    vector<vector<string>> tabDonnees;

    for(int j = 0 ; j < nbHauteur ; j++){
        vector<string> tabDonneesTemp;

        for(int i = 0 ; i < nbLargeur ; i++){
            string temp = "";
            if(j < 10){
                temp = "0";
            }
            temp += int_to_string(j);

            if(i < 10){
                temp += "0";
            }
            temp += int_to_string(i);

            tabDonneesTemp.push_back(temp);
        }

        tabDonnees.push_back(tabDonneesTemp);
    }

    /*for(int j = 0 ; j < tabDonnees.size() ; j++){
        for(int i = 0 ; i < tabDonnees[j].size() ; i++){
            cout << tabDonnees[j][i] << " ";
        }
        cout << endl;
    }*/


    ofstream fichierSave("./mapCaseVoisine.propData", ios::out | ios::trunc);
    if(fichierSave)  // si l'ouverture a r�ussi
    {
        fichierSave << "#Chargement info mapCaseVoisine" << endl;
        fichierSave << int_to_string(tabDonnees.size()) << endl;
        fichierSave << int_to_string(tabDonnees[0].size()) << endl;

        for(unsigned int j = 0 ; j < tabDonnees.size() ; j++){
            for(unsigned int i = 0 ; i < tabDonnees[j].size() ; i++){
                fichierSave << tabDonnees[j][i] << " ";
            }
            fichierSave << endl;
        }
        fichierSave << "-9" << endl;

        fichierSave.close();  // on referme le fichier
    }
    else{  // sinon
        cout << "Erreur a l ouverture de ./mapCaseVoisine.propData" << endl;
    }


    ofstream fichierSaveTile("./tileCaseVoisine.propData", ios::out | ios::trunc);
    if(fichierSaveTile)  // si l'ouverture a r�ussi
    {
        fichierSaveTile << "#Chargement info tileCaseVoisine" << endl;
        fichierSaveTile << int_to_string(tabDonnees.size() * tabDonnees[0].size()) << endl;
        fichierSaveTile << "5" << endl;

        for(unsigned int j = 0 ; j < tabDonnees.size() ; j++){
            for(unsigned int i = 0 ; i < tabDonnees[j].size() ; i++){
                fichierSaveTile << tabDonnees[j][i] << " ";

                if(i > 0){
                    fichierSaveTile << tabDonnees[j][i - 1] << " ";
                }
                else{
                    fichierSaveTile << "-1   ";
                }

                if(i < tabDonnees[j].size() - 1){
                    fichierSaveTile << tabDonnees[j][i + 1] << " ";
                }
                else{
                    fichierSaveTile << "-1   ";
                }

                if(j > 0){
                    fichierSaveTile << tabDonnees[j - 1][i] << " ";
                }
                else{
                    fichierSaveTile << "-1   ";
                }

                if(j < tabDonnees[j].size() - 1){
                    fichierSaveTile << tabDonnees[j + 1][i] << " ";
                }
                else{
                    fichierSaveTile << "-1   ";
                }
                fichierSaveTile << endl;
            }
        }
        fichierSaveTile << "-9" << endl;

        fichierSaveTile.close();  // on referme le fichier
    }
    else{  // sinon
        cout << "Erreur a l ouverture de ./tileCaseVoisine.propData" << endl;
    }

    return 0;
}
















